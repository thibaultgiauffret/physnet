#!/bin/bash
mkdir -p artillery_results
artillery run --output ./artillery_results/test_simple_report.json stress_test_simple.yml
artillery report ./artillery_results/test_simple_report.json
