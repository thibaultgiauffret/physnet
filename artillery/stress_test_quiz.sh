#!/bin/bash
# Make a copy of the database.db file in the parent directory
cp ../db/modules/quiz/database.db .
# Run the random_names.py script to generate random names
python3 random_names.py
# Create artillery_results directory if it doesn't exist
mkdir -p artillery_results
artillery run --output ./artillery_results/test_quiz_report.json stress_test_quiz.yml
artillery report ./artillery_results/test_quiz_report.json
# Copy the new database.db file from the parent directory
cp ../db/modules/quiz/database.db ./database_results.db
# Copy the database.db file back to the parent directory
cp database.db ../db/modules/quiz/
# Remove the database.db file from the current directory
rm database.db