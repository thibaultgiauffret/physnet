@echo off
echo Building PhysNet for Windows...
REM Remove the existing dist directory
rmdir /s /q .\dist
REM Create a new dist directory
mkdir .\dist
mkdir .\dist\physnet-win
REM Build the excutables
start /B pkg . --targets node18-win-x64 --output .\dist\physnet-win\physnet-win.exe
timeout /T 20 /nobreak
REM xcopy the public, protected, db, and data directories to the dist directory
Robocopy ".\public" ".\dist\physnet-win\public" /mir
Robocopy ".\protected" ".\dist\physnet-win\protected" /mir
Robocopy ".\db" ".\dist\physnet-win\db" /mir
Robocopy ".\data" ".\dist\physnet-win\data" /mir
Robocopy ".\functions" ".\dist\physnet-win\functions" /mir
copy ".\cred.json" ".\dist\physnet-win\cred.json"
REM Remove unnecessary files
for /r .\dist\physnet-win %%f in (database.db-shm) do del /q "%%f"
for /r .\dist\physnet-win %%f in (database.db-wal) do del /q "%%f"
for /r .\dist\physnet-win %%f in (database.db.backup) do del /q "%%f"
REM Copy the database file
copy ".\database.db" ".\dist\physnet-win\database.db"
REM Compress the dist directory
cd .\dist
powershell Compress-Archive -Path .\physnet-win -DestinationPath .\physnet-win.zip
rmdir /s /q .\physnet-win
echo Done!