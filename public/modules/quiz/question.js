$(document).ready(function () {



  // Instanciate time modal
  var timeModal = new bootstrap.Modal(document.getElementById("timeModal"), {});

  var title = getCookie('title');
  var question_max = getCookie('question_max');
  var text = "";
  var time = "";
  var answers = "";


  // Get the quiz ID from the cookie
  var quiz = getCookie('quiz');

  socket.on("connect", () => {
    console.log(socket.id);
    socket.emit("join_quiz", quiz);
  });


  // Check if the quiz is finished
  var results = getCookie('results');
  if (results == "1") {
    window.location.replace("/resultats");
  }

  // Get elapsed time
  var counter = 0;
  counter = parseFloat(getCookie('time'));

  var s = 0; // Answer not sent
  var question = 0;

  // Get the question number and username from the cookie
  var username = getCookie('username');
  var question = parseFloat(getCookie('question'));
  var question_id = JSON.parse(getCookie('list_questions'));
  if (username == "" || username == null || question == NaN || question == null) {
    window.location.replace("/index?message=danger&content=Une erreur est survenue lors du chargement de la question.");
  } else {
    socket.emit('question', { username: username, question: question_id[0].id, quiz: quiz });
    document.getElementById("username").innerHTML = username;
    document.getElementById("question").innerHTML = question;
  }


  // Get the answer and send it to the server
  var answer;
  $("#submit").click(function () {
    send(true);
  });

  $("#submit1").click(function () {
    send(true);
  });

  // Send function
  function send(next_question) {
    var val = [];
    var answer_letter = []
    $.each($("input[name='answer']:checked"), function () {
      value = parseInt($(this).val());
      id = $(this).attr('id');
      val.push(value);
      answer_letter.push(id);
    });
    socket.emit('user_answer', { username: username, quiz: quiz, question: question_id[0].id, answer: val, answer_letter: answer_letter, next_question: next_question });
    s = 1;
  };

  // Next question function
  function next() {
    if (question >= question_max) {
      setCookie('results', 1, 1);
      window.location.replace("/resultats");
      s = 0;
    }
    else {
      if (s == 1) {
        // Get list of questions from the list_questions cookie
        var list_questions = JSON.parse(getCookie('list_questions'));
        // Pop first element
        list_questions.shift();
        // Set the cookie again
        setCookie('list_questions', JSON.stringify(list_questions), 1);
        setCookie('timeout', 0, 1);
        setCookie('question', question += 1, 1);
        setCookie('time', "0", 1);

        window.location.replace("/question");
        s = 0;
      }
    }
  };

  // Get the question from the server
  var time;

  socket.on('question', function (data) {
    text = data.text;
    time = data.time;
    type = data.type;
    answers = data.answers;

    document.getElementById("title").innerHTML = title;
    document.getElementById("progression").style.width = question / question_max * 100 + "%";
    document.getElementById("progression").innerHTML = question + "/" + question_max;
    document.getElementById("text").innerHTML = text;

    if (type == 1) {
      document.getElementById("multiple").innerHTML = '<i class="fa-solid fa-triangle-exclamation"></i>&nbsp;Une ou plusieurs réponses possibles !';
    }

    time = parseFloat(time);
    if (time != 0) {
      var Minutes = (60 * time) - counter;

      display = document.querySelector('#time');
      startTimer(Minutes, display, ss);
    } else {
      document.getElementById("time").innerHTML = "illimité";
    }

    $("#list").html("")

    if (type == 0) {
      for (i = 0; i < answers.length; i++) {
        var objto = document.getElementById("list");
        var li = document.createElement("div");
        var rep_id = answers[i].id;
        var rep_letter = answers[i].letter;
        var display_rep_letter = String.fromCharCode(65 + i);
        var rep_text = answers[i].text;
        li.innerHTML = '<input type="radio" class="btn-check"  name="answer" value="' + rep_id + '" id="' + rep_letter + '" autocomplete="off"><label class="btn btn-success mr-2 large btn_answer" for="' + rep_letter + '">' + display_rep_letter + '</label>' + rep_text;
        objto.appendChild(li);
      }
    } else {
      for (i = 0; i < answers.length; i++) {
        var objto = document.getElementById("list");
        var li = document.createElement("div");
        var rep_id = answers[i].id;
        var rep_letter = answers[i].letter;
        var rep_text = answers[i].text;
        li.innerHTML = '<input type="checkbox" class="btn-check"  name="answer" value="' + rep_id + '" id="' + rep_letter + '" autocomplete="off"><label class="btn btn-success mr-2 large btn_answer" for="' + rep_letter + '">' + rep_letter + '</label>' + rep_text;
        objto.appendChild(li);
      };
    }
    MathJax.typeset();
  });

  // Go to the next question
  socket.on('next_question', function (data) {
    next();
  })


  // Timer function
  function startTimer(duration, display, ss) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
      counter += 1;
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);
      setCookie('time', counter, 1);


      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      if (ss == 1) {
        display.textContent = minutes + ":" + seconds;

        document.getElementById("progress").style.width = timer / duration * 100 + "%";

        if (timer / duration < 0.1) {
          document.getElementById("progress").classList.add('bg-danger');
        }

        if (timer / duration < 0.5) {
          document.getElementById("progress").classList.add('bg-warning');
        }
      }

      if (--timer < 0 && ss == 1) {
        document.getElementById("time_container").innerHTML = "<i class=\"fas fa-stopwatch\"></i>&nbsp;Temps écoulé !";
        var check = $('.btn-check');
        check.prop('disabled', !check.prop('disabled'));
        ss = 0;
        timeModal.toggle();
        setCookie('timeout', 1, 1);
        // Send after timeout
        send(false);
      }
    }, 1000);
  }

  $('.btn_answer').on('click', function () {
    $(input, this).removeAttr('checked');
    $(this).removeClass('active');
  });
});

// Check if timeout
var ss = 1;
if (getCookie("timeout") == 1) {
  document.getElementById("time_container").innerHTML = "<i class=\"fas fa-stopwatch\"></i>&nbsp;time écoulé !";
  var check = $('.checkbox-answer');
  check.prop('disabled', !check.prop('disabled'));
  ss = 0;
  timeModal.toggle();
}

