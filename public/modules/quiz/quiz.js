$(document).ready(function(){
	  
	var url_string = window.location.href ;
	var url = new URL(url_string);
	var quiz = url.searchParams.get("id");
	if (quiz == null){
		quiz = getCookie("classroom");
	}

	socket.emit('index_quiz', quiz);
	console.log("Le quiz " + quiz + " a été demandé.");




	$(document).ready(function(){

		socket.on("connect", () => {
			console.log(socket.id);
			socket.emit("join_quiz", quiz);
		  });
	

		eraseAllCookies();


		// On vérifie s'il existe un cookie créé. Si c'est le cas, c'est que l'utilisateur existe donc on redirige vers le quiz.
		var username = getCookie('username');
		if (username != null){
				window.location.replace("/quiz"+"&id="+quiz);
		}

	// On récupère les données du quiz
	var title = "";
	var question_max=0;
	var time=0;
	var points=0;

	socket.on('index', function(data) {
		if (data.title != null){
			title = data.title;
			question_max = data.questions_max;
			time = data.time;
			points = data.points;

			time = time*60;
			minutes = parseInt(time / 60, 10);
			seconds = parseInt(time % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			document.getElementById("title").innerHTML = title;
			document.getElementById("questions").innerHTML = question_max;
			document.getElementById("points").innerHTML = points;
			document.getElementById("time").innerHTML = minutes + ":" + seconds;
		}else{
			window.location.replace("/index?message=danger&content=Une erreur est survenue lors du chargement du quiz.");
		}

	});


	// On envoie les données de connexion
	var username;
	$("#register").click(function(){


		username=$("#username").val();

		// On vérifie que l'utilisateur n'est pas déjà inscrit
		if(username == "" || /^[a-zA-Z0-9]+$/.test(username)==false){
			window.location.replace("/quiz?message=danger&content=Veuillez entrer un nom d'utilisateur valide (contenant seulement des lettres ou des chiffres).&id="+quiz);
		} else {
			
			socket.emit('register_quiz', {username: username, quiz: quiz});
		
		}
	});

	socket.on('redirect_quiz', function(data) {
		destination = data.destination;
		list_questions = data.list_questions;
		eraseCookie('username');
		eraseCookie('question');
		eraseCookie('results');
		setCookie('username',username,1);
		setCookie('question_max',question_max,1);
		setCookie('list_questions',JSON.stringify(list_questions),1);
		setCookie('question',1,1);
		setCookie('results',0,1);
		setCookie('timeout',0,1);
		setCookie('time',"0",1);
		setCookie('quiz',quiz,1);
		setCookie('title',title,1);
		console.log("L'utilisateur va être créé !");
		window.location.href = destination;
	});

	});
});

