
$(document).ready(function () {



    var results = getCookie('results');
    if (results != "1") {
        window.location.replace("/index?message=denied");
    }

    var username = getCookie('username');
    var quiz = getCookie('quiz');

    socket.on("connect", () => {
        console.log(socket.id);
        socket.emit("join_quiz", quiz);
    });


    socket.emit('end_quiz', { username: username, quiz: quiz });
    document.getElementById("username").innerHTML = username + ", voici vos résultats !";

    socket.on('results', function (data) {

        if (data.allQuestions == undefined) {

            console.log(data);

            allAnswers = data.allAnswers
            userResults = data.userResults

            let totalPoints = 0;
            let note = 0;
            let note_max = 0;

            for (i = 0; i < userResults.questions.length; i++) {

                // Get correct answers and points
                let sum = 0;
                let sumPoints = 0;
                for (j = 0; j < allAnswers.length; j++) {
                    if (userResults.questions[i] == allAnswers[j].question_id) {
                        sum += allAnswers[j].answer_points;
                        console.log(sum)
                        // Get user points
                        for (k = 0; k < userResults.answers[i].length; k++) {
                            if (userResults.answers[i][k] == allAnswers[j].answer_id) {
                                sumPoints += allAnswers[j].answer_points;
                            }
                        }
                    }
                }

                totalPoints += sum;
                note += sumPoints;
                note_max = parseFloat(totalPoints);
            }

            var objTo = document.getElementById("note");
            var divNote = document.createElement("div");
            divNote.innerHTML = '<center><h2><span class="badge badge-secondary">Note : ' + note + '/' + note_max + '</span></h2></center></div>';
            objTo.appendChild(divNote);
            console.log("Votre note est de : " + note + "/" + note_max);
        

        } else {
            allQuestions = data.allQuestions
            allAnswers = data.allAnswers
            userResults = data.userResults

            let questionTitles = [];
            let questionPoints = [];
            let userPoints = [];
            let correctAnswersTexts = [];
            let correctAnswersLetters = [];
            let totalPoints = 0;

            for (i = 0; i < userResults.questions.length; i++) {

                // Store question titles
                for (j = 0; j < allQuestions.length; j++) {
                    if (userResults.questions[i] == allQuestions[j].question_id) {
                        questionTitles.push(allQuestions[j].question_text);
                    }
                }

                // Get correct answers and points
                let sum = 0;
                let correctAnswersTextsTemp = [];
                let correctAnswersLettersTemp = [];
                let sumPoints = 0;
                for (j = 0; j < allAnswers.length; j++) {
                    if (userResults.questions[i] == allAnswers[j].question_id) {
                        sum += allAnswers[j].answer_points;
                        if (allAnswers[j].answer_points > 0) {
                            correctAnswersTextsTemp.push(allAnswers[j].answer_text);
                            correctAnswersLettersTemp.push(allAnswers[j].answer_letter);
                        }
                        // Get user points
                        for (k = 0; k < userResults.answers[i].length; k++) {
                            if (userResults.answers[i][k] == allAnswers[j].answer_id) {
                                sumPoints += allAnswers[j].answer_points;
                            }
                        }
                    }
                }

                totalPoints += sum;
                questionPoints.push(sum);
                userPoints.push(sumPoints);
                correctAnswersTexts.push(correctAnswersTextsTemp);
                correctAnswersLetters.push(correctAnswersLettersTemp);
            }

            let note = 0;
            let note_max = 0;

            // Display results
            for (k = 0; k < userResults.questions.length; k++) {

                question_title = questionTitles[k];
                points = questionPoints[k];
                question_id = userResults.questions[k];
                correct_answer_id = correctAnswersLetters[k].toString().split(',').join(', ');
                correct_answer_text = correctAnswersTexts[k].toString().split(',').join(', ');
                answer_id = userResults.answers[k];
                answer_text = [];
                answer_letter = [];

                for (i = 0; i < allAnswers.length; i++) {
                    for (j = 0; j < answer_id.length; j++) {
                        if (answer_id[j] == allAnswers[i].answer_id) {
                            answer_text.push(allAnswers[i].answer_text);
                            answer_letter.push(allAnswers[i].answer_letter);
                        }
                    }
                }
                answer_text = answer_text.toString().split(',').join(', ');
                answer_letter = answer_letter.toString().split(',').join(', ');

                points_attrib = userPoints[k];

                note += points_attrib;
                note_max = parseFloat(totalPoints);

                var counter = parseInt(k)+1;

                var objTo = document.getElementById("list");
                var divQuestion = document.createElement("li");
                divQuestion.classList.add('list-group-item');
                if (k!=0){
                    divQuestion.innerHTML += "<hr>"
                }
                divQuestion.innerHTML += '<span class="badge badge-secondary large mb-2 mr-2">Question ' + counter + '</span><span class="badge badge-warning large">' + points_attrib + ' points attribué(s)</span><br>' + question_title + '<br><b>Votre réponse : </b><br><center>' + answer_text + '</center><br><b>Réponse(s) attendue(s) : </b> <br><center>' + correct_answer_text + '</center>';
                // divQuestion.innerHTML = '<h5 class="mb-2 large">Question ' + question_id + ' - ' + question_title + '</h5><br>Votre réponse : <br><center><span class="badge badge-secondary large ml-3 mr-3">' + answer_letter + '</span>' + answer_text + '</center><br> Réponse(s) attendue(s) : <br><center><span class="badge badge-secondary large ml-3 mr-3">' + correct_answer_id + '</span>' + correct_answer_text + '</center><br><span class="badge badge-warning large">' + points_attrib + ' points attribué(s)</span>';
                objTo.appendChild(divQuestion);
            }

            var objTo = document.getElementById("note");
            var divNote = document.createElement("div");
            divNote.innerHTML = '<center><h2><span class="badge badge-secondary">Note : ' + note + '/' + note_max + '</span></h2></center></div>';
            objTo.appendChild(divNote);
            console.log("Votre note est de : " + note + "/" + note_max);
        }
    });

});
