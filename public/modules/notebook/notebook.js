var notebook;
var notebook_name = "notebook";
var notebook_url;
var toast;

$(document).ready(function () {

    // Hide the side by side and show info buttons
    $("#sideBySideButton").hide();
    $("#showInfosButton").hide();

    toast = new bootstrap.Toast(document.getElementById('toast'), {
        animation: true,
        autohide: true,
        delay: 5000
    })

    notebook = document.getElementById('notebook');
    notebook.contentWindow.postMessage({ message: 'remove_header' }, '*')

    // Init the tooltips
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

    // Handle the split view
    const resizer = document.getElementById("dragMe");
    const leftSide = resizer.previousElementSibling;
    const rightSide = resizer.nextElementSibling;

    let x = 0;
    let y = 0;

    let leftWidth = 0;

    const mouseDownHandler = function (e) {

        x = e.clientX;
        y = e.clientY;

        leftWidth = leftSide.getBoundingClientRect().width;

        document.addEventListener("mousemove", mouseMoveHandler);
        document.addEventListener("mouseup", mouseUpHandler);
    };

    const mouseMoveHandler = function (e) {

        const dx = e.clientX - x;
        const dy = e.clientY - y;

        document.body.style.cursor = "col-resize";

        leftSide.style.userSelect = "none";
        leftSide.style.pointerEvents = "none";

        rightSide.style.userSelect = "none";
        rightSide.style.pointerEvents = "none";

        //
        const newLeftWidth =
            ((leftWidth + dx) * 100) / resizer.parentNode.getBoundingClientRect().width;

        // If the new width is less than 20%, stop
        if (newLeftWidth < 20 || newLeftWidth > 80) {
            return;
        }
        leftSide.style.width = `${newLeftWidth}%`;
    };

    const mouseUpHandler = function () {
        resizer.style.removeProperty("cursor");
        document.body.style.removeProperty("cursor");

        leftSide.style.removeProperty("user-select");
        leftSide.style.removeProperty("pointer-events");

        rightSide.style.removeProperty("user-select");
        rightSide.style.removeProperty("pointer-events");

        document.removeEventListener("mousemove", mouseMoveHandler);
        document.removeEventListener("mouseup", mouseUpHandler);
    };

    resizer.addEventListener("mousedown", mouseDownHandler);

    // On mobile, use the touch events
    const touchStartHandler = function (e) {

        x = e.touches[0].clientX;
        y = e.touches[0].clientY;

        leftWidth = leftSide.getBoundingClientRect().width;

        document.addEventListener("touchmove", touchMoveHandler);
        document.addEventListener("touchend", touchEndHandler);
    };

    const touchMoveHandler = function (e) {

        const dx = e.touches[0].clientX - x;
        const dy = e.touches[0].clientY - y;

        document.body.style.cursor = "col-resize";

        leftSide.style.userSelect = "none";
        leftSide.style.pointerEvents = "none";

        rightSide.style.userSelect = "none";
        rightSide.style.pointerEvents = "none";

        //
        const newLeftWidth =
            ((leftWidth + dx) * 100) / resizer.parentNode.getBoundingClientRect().width;
        

        // If the new width is less than 20%, stop
        if (newLeftWidth < 20 || newLeftWidth > 80) {
            return;
        }

        leftSide.style.width = `${newLeftWidth}%`;
    };

    const touchEndHandler = function () {
        resizer.style.removeProperty("cursor");
        document.body.style.removeProperty("cursor");

        leftSide.style.removeProperty("user-select");
        leftSide.style.removeProperty("pointer-events");

        rightSide.style.removeProperty("user-select");
        rightSide.style.removeProperty("pointer-events");

        document.removeEventListener("touchmove", touchMoveHandler);
        document.removeEventListener("touchend", touchEndHandler);
    };

    resizer.addEventListener("touchstart", touchStartHandler);

    // Detect window rotation
    window.addEventListener("orientationchange", function () {
        console.log("The orientation of the screen is: " + window.screen.orientation.type);
        // Get the new orientation
        var orientation = window.screen.orientation.type;
        if (orientation == "portrait-primary" || orientation == "portrait-secondary") {
            // Portrait mode
            $("#split_left").css("width", "100%")
            $("#split_right").css("width", "100%")
            // Disable the resizer touch event
            resizer.removeEventListener("touchstart", touchStartHandler);
            document.removeEventListener("touchmove", touchMoveHandler);
            document.removeEventListener("touchend", touchEndHandler);
        } else {
            // Landscape mode
            $("#split_left").css("width", "50%")
            $("#split_right").css("width", "50%")
            // Enable the resizer touch event
            resizer.addEventListener("touchstart", touchStartHandler);
        }
    });

    // Instantiate the modal
    passwordModal = new bootstrap.Modal(document.getElementById('passwordModal'), {});

    // get parameters from URL
    var url_string = window.location.href;
    var url = new URL(url_string);
    notebook_id = url.searchParams.get("id");
    console.log("Notebook activity is : " + notebook_id);

    socket.on("connect", () => {
        console.log(socket.id);
        socket.emit("join_notebook", notebook_id);
    });

    socket.on("index_notebook", (data) => {
        title = data.title;
        notebook_name = data.title;
        instructions = data.instructions;
        notebook_url = data.url;
        notebook_aux = data.aux;
        doc = data.split;
        blocalgo = data.blocalgo;

        if (doc == "" || doc == null) {
            $("#split_left").hide();
            $("#dragMe").hide();
            $("#fullscreenButton").hide();
            $("#info").html(`
            <div class="card shadow bg-white m-3" style="flex: 0 1;" id="infos_card">
            <div class="card-body">
                <!-- Button to hide this card -->
                <button class="btn btn-secondary btn-sm float-end" type="button" id="hideButton"
                    onclick="hideCard()" data-bs-toggle="tooltip" data-bs-placement="bottom"
                    title="Masquer les consignes"><i class="fa-solid fa-eye-slash"></i></button>

                <p class="large mb-0">
                    <strong><span id="title" class="placeholder-glow"><span
                                class="placeholder col-4"></span></span></strong><br>
                    Consignes :
                </p>
                <p id="instructions" class="placeholder-glow mb-0" style="max-height:75px;overflow-y: scroll;"><span
                        class="placeholder bg-secondary col-5"></span><br><span
                        class="placeholder bg-secondary col-3"></span></p>
            </div>
        </div>
            `);
        } else {
            $("#split_info").html(`
            <div class="card shadow bg-white m-3" style="flex: 0 1;" id="infos_card">
            <div class="card-body">
                <!-- Button to hide this card -->
                <button class="btn btn-secondary btn-sm float-end" type="button" id="hideButton"
                    onclick="hideCard()" data-bs-toggle="tooltip" data-bs-placement="bottom"
                    title="Masquer les consignes"><i class="fa-solid fa-eye-slash"></i></button>

                <p class="large mb-0">
                    <strong><span id="title" class="placeholder-glow"><span
                                class="placeholder col-4"></span></span></strong><br>
                    Consignes :
                </p>
                <p id="instructions" class="placeholder-glow mb-0"  style="max-height:120px;overflow-y: scroll;"><span
                        class="placeholder bg-secondary col-5"></span><br><span
                        class="placeholder bg-secondary col-3"></span></p>
            </div>
        </div>
            `)
        }

        $("#title").html(title);
        $("#instructions").html(instructions);

        // Add aux to iframe url
        if (notebook_aux != "" && notebook_aux != null) {
            // Get current src
            var src = $("#notebook").attr("src");
            // Get the base domain of the current page
            var base_url = window.location.origin;
            // Add aux to src
            $("#notebook").attr("src", src + "?aux=" + base_url + "/" + notebook_aux);
            // reload the iframe
            notebook.src = notebook.src;
        }

        if (blocalgo == 1) {
            // Change the url of the #notebook iframe
            $("#notebook").attr("src", "https://www.ensciences.fr/addons/blocalgo_notebook/notebook_physnet.html");
        }

        // Check if local storage contains a notebook
        if (localStorage.getItem("notebook_" + notebook_id)) {
            console.log("localstorage set, not reloading the notebook");
            // Send the content of the ipynb file to the iframe
            notebook.onload = function () {
                notebook.contentWindow.postMessage({ message: 'load', content: JSON.parse(localStorage.getItem("notebook_" + notebook_id)) }, '*')
                $("#message").html("<span class=\"text-info\"><i class=\"fa-solid fa-circle-info\"></i>&nbsp;Le notebook a été chargé à partir du stockage votre navigateur&nbsp;!</span>")
                toast.show();
            }
        } else {
            // Wait for the iframe to load
            notebook.onload = function () {
                $.getJSON(notebook_url, function (json) {
                    // Send the content of the ipynb file to the iframe
                    notebook.contentWindow.postMessage({ message: 'load', content: json }, '*')
                });
            }
        }

        // If the notebook is split, show the split view
        if (doc != "") {
            var doc_iframe = document.getElementById("doc");
            doc_iframe.src = doc
        }


    });
})

window.onmessage = function (e) {
    if (e.data.message == 'saveLocal') {
        console.log("Saving notebook to local storage");
        localStorage.setItem("notebook_" + notebook_id, JSON.stringify(e.data.content));
        $("#message").html(`<span class=\"text-success\"><i class=\"fa-solid fa-circle-check\"></i>&nbsp;Le notebook a été sauvegardé dans votre navigateur&nbsp;!</span>`)
        toast.show();
    } else if (e.data.message == 'upload') {
        file = e.data.content;
        // Show the modal
        passwordModal.show();
        // Get the content of the notebook

        // Wait for the passwordSubmit event
        $("#passwordSubmit").click(function () {
            // Get the password
            var password = $("#password").val();

            // Hide the modal
            passwordModal.hide();

            var name = $("#username").val();
            // Replace spaces by underscores
            name = name.replace(/ /g, "_");

            if (name == "") {
                $("#message").html("<span class=\"text-success\"><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Merci de renseigner votre nom avant de déposer votre travail&nbsp;!</span>")
                $("#username").addClass("is-invalid");
                toast.show();
                return;
            } else {
                // if (confirm("Êtes-vous sûr de vouloir déposer cet enregistrement ? L'ensemble des autres enregistrements effectués seront supprimés. Si vous souhaitez les conserver, merci de les télécharger avant de déposer votre travail.")) {
                $("#username").removeClass("is-invalid");

                // Get the file type
                var date = new Date();
                // Convert the date to a string in the format YYYY-MM-DD_HH-MM
                date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "_" + date.getHours() + "-" + date.getMinutes();
                var filename = "notebook_" + notebook_id + "-name_" + name + "-date_" + date + ".ipynb";

                socket.emit('uploadNotebook', { file: file, filename: filename, pass: password, notebook_id: notebook_id }, function (response) {
                    clearForm()
                    if (response == "success") {
                        $("#message").html("<span class=\"text-success\"><i class=\"fa-solid fa-circle-check\"></i>&nbsp;Le fichier a bien été déposé&nbsp;!</span>")
                        $("#file").val("");
                        toast.show();
                    } else if (response == "exists") {
                        $('#message').html('<span class=\"text-danger\"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Une erreur s\'est produite lors de l\'envoi du fichier. Merci de modifier votre nom&nbsp;!</span>')
                        toast.show();
                    } else if (response == "wrong_pass") {
                        $('#message').html('<span class=\"text-warning\"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Le code de dépôt est incorrect&nbsp;!</span>')
                        toast.show();
                    } else {
                        $('#message').html('<span class=\"text-danger\"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Une erreur s\'est produite lors de l\'envoi du fichier&nbsp;!</span>')
                        toast.show();
                    }

                })
                // }
            }

        });
    }
}

// Handle the split view
function sideBySide() {
    $("#split_left").show();
    $("#dragMe").show();
    $("#split_right").show();
    $("#sideBySideButton").hide();
    $("#fullscreenButton").show();
    if ($("#infos_card").is(":hidden")) {
        $("#showInfosButton").show();
    }
}

// Handle the fullscreen view
function fullscreen() {
    $("#split_left").hide();
    $("#dragMe").hide();
    $("#fullscreenButton").hide();
    $("#sideBySideButton").show();
    $("#showInfosButton").hide();
}

function saveNotebook() {
    console.log("save notebook");
    notebook.contentWindow.postMessage({ message: 'saveLocal' }, '*')
}

function downloadNotebook() {
    console.log("download notebook");
    notebook.contentWindow.postMessage({ message: 'save', name: notebook_name + ".ipynb" }, '*')
}

function uploadFile() {
    notebook.contentWindow.postMessage({ message: 'upload' }, '*')
}

function hideCard() {
    $("#infos_card").hide();
    $("#showInfosButton").show();
}

function showCard() {
    $("#infos_card").show();
    $("#showInfosButton").hide();
}

function reloadNotebook() {
    // Get the content of the ipynb file found at url. Open it as a json file
    if (confirm("Êtes-vous sûr de vouloir recharger le notebook ? L'ensemble des modifications seront perdues.")) {
        $.getJSON(notebook_url, function (json) {
            // Send the content of the ipynb file to the iframe
            notebook.contentWindow.postMessage({ message: 'load', content: json }, '*')
        });
    }
}

function clearForm() {
    $("#file").removeClass("is-invalid");
    $("#size").removeClass("text-danger");
    $("#extensions").removeClass("text-danger");
}

// Save the notebook every 1 minute
setInterval(function () {
    // Save to the local storage
    notebook.contentWindow.postMessage({ message: 'saveLocal' }, '*')
}, 60000);

