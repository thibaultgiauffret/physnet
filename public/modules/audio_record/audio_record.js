// DISCLAIMER : Some of the code handling the recording comes from this project : https://github.com/ralzohairi/js-audio-recording

var passwordModal;
var audio_id;
var maxTime = 1;

// IndexedDB
window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.OIndexedDB || window.msIndexedDB,
    IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.OIDBTransaction || window.msIDBTransaction,
    dbVersion = 1;

var record_attempt = 0;
var elapsedTime = 0;
const recordedHTML = (record_attempt) => {
    return `
        <th>
            <span class="badge bg-primary" style="margin-top:14px;">`+ record_attempt + `</span>
        </th>
        <td>
            <audio class="audio-element my-1" id="audio_element_${record_attempt}" controls>
            </audio>
        </td>
        <td>
        <button type="button" class="btn btn-secondary my-1" onclick="downloadAudio(`+ record_attempt + `)"><i
        class="fa-solid fa-download"></i></button>
        <button type="button" class="btn btn-danger my-1" onclick="deleteAudioFile(`+ record_attempt + `)"><i
        class="fa-solid fa-trash"></i></button>

        <button type="button" class="btn btn-success my-1" onclick="sendAudio(`+ record_attempt + `)"><i
        class="fa-solid fa-file-arrow-up"></i>&nbsp;Déposer ce travail</button>
        </td>
    `
}

// Create/open database
var request = window.indexedDB.open("audioFiles", dbVersion),
    db,
    createObjectStore = function (dataBase) {
        // Create an objectStore
        console.log("Creating objectStore")
        dataBase.createObjectStore("audio");

    },
    putAudio = function (blob) {
        console.log("Putting audio in IndexedDB ");
        // Open a transaction to the database
        var transaction = db.transaction(["audio"], "readwrite");

        // Put the blob into the dabase
        var put = transaction.objectStore("audio").put(blob, "audio_" + record_attempt);

        // Retrieve the file that was just stored
        transaction.objectStore("audio").get("audio_" + record_attempt).onsuccess = function (event) {
            var blob = event.target.result;
            console.log(blob);
        }
    },
    getAudio = function () {
        // Test if the "audio" objectStore exists
        if (db.objectStoreNames.contains("audio")) {
            console.log("Getting audio from IndexedDB ");
            // Open a transaction to the database
            var transaction = db.transaction(["audio"], "readonly");

            // For each record in the store
            transaction.objectStore("audio").openCursor().onsuccess = function (event) {
                var cursor = event.target.result;
                console.log(cursor);
                // get the blob
                if (cursor) {
                    var blob = cursor.value;
                    // Split the key to get the record_attempt
                    record_attempt = cursor.key.split("_")[1];
                    // Add the audio to the #records list
                    var record = document.createElement("tr");
                    // Add class record_attempt to the record
                    record.classList.add("record_attempt");
                    // Set the id of the record
                    record.id = "record_attempt_" + record_attempt;
                    record.innerHTML = recordedHTML(record_attempt)
                    $("#records").append(record);
                    // Get the audio element
                    var audio = document.getElementById("audio_element_" + record_attempt);
                    // Add the audio to the record
                    var source = document.createElement("source");
                    source.src = URL.createObjectURL(blob);
                    source.type = blob.type;
                    // Append the source to the audio element
                    audio.appendChild(source);
                    // Move to the next record
                    cursor.continue();
                }
            }
        }

    },
    deleteAudio = function (id) {
        // Open a transaction to the database
        var transaction = db.transaction(["audio"], "readwrite");

        // Delete the audio from the database
        var request = transaction.objectStore("audio").delete("audio_" + id);
    },
    resetDatabase = function () {
        // Delete the objectStore
        var transaction = db.transaction(["audio"], "readwrite");
        var request = transaction.objectStore("audio").clear();
    };


request.onerror = function (event) {
    console.log("Error creating/accessing IndexedDB database");
};

request.onsuccess = function (event) {
    console.log("Success creating/accessing IndexedDB database");
    db = request.result;

    db.onerror = function (event) {
        console.log("Error creating/accessing IndexedDB database");
    };

    if (db.setVersion) {
        if (db.version != dbVersion) {
            var setVersion = db.setVersion(dbVersion);
            setVersion.onsuccess = function () {
                createObjectStore(db);
                getAudio();
            };
        } else {

            getAudio();
        }
    } else {
        getAudio();
    }

}

request.onupgradeneeded = function (event) {
    createObjectStore(event.target.result);
};


$(document).ready(function () {

    // Instantiate the modal
    passwordModal = new bootstrap.Modal(document.getElementById('passwordModal'), {});

    // get parameters from URL
    var url_string = window.location.href;
    var url = new URL(url_string);
    audio_id = url.searchParams.get("id");
    console.log("Audio activity is : " + audio_id);

    socket.on("connect", () => {
        console.log(socket.id);
        socket.emit("join_audio", audio_id);
    });

    socket.on("index_audio", (data) => {
        title = data.title;
        time = data.time;
        if (time == 0) {
            time = 99999999999;
        }
        instructions = data.instructions;
        // Format the time to minutes:seconds
        minutes = parseInt(time / 60, 10);
        seconds = parseInt(time % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        maxTime = time; // in seconds

        document.getElementById("title").innerHTML = title;
        if (maxTime == 99999999999) {
            document.getElementById("time").innerHTML = "illimité";
        } else {
            document.getElementById("time").innerHTML = minutes + ":" + seconds;
        }
        document.getElementById("instructions").innerHTML = instructions;
    });


    var microphoneButton = document.getElementById("record");
    var pauseRecordingButton = document.getElementById("pause");
    var stopRecordingButton = document.getElementById("stop");
    var elapsedTimeTag = document.getElementById("timer");
    var audioElement = $(".audio-element")[0];
    var audioElementSource = document.getElementsByClassName("audio-element")[0].getElementsByTagName("source")[0];

    //Listeners
    microphoneButton.onclick = startAudioRecording;
    pauseRecordingButton.onclick = pauseAudioRecording;
    stopRecordingButton.onclick = stopAudioRecording;

    // Displays recording control buttons
    function showRecordingButton() {
        microphoneButton.style.display = "none";
        pauseRecordingButton.style.display = "inline-block";
        stopRecordingButton.removeAttribute("disabled");
        handleRecordingTime();
    }

    function hideRecordingButton() {
        microphoneButton.style.display = "inline-block";
        pauseRecordingButton.style.display = "none";
        stopRecordingButton.setAttribute("disabled", true);
        clearInterval(elapsedTimeTimer);
    }

    //Controller

    var elapsedTimeTimer;

    // Pauses the currently started audio recording
    function pauseAudioRecording() {

        // Test if the audio is paused
        let recorderAudioIsPaused = audioRecorder.mediaRecorder.state === "paused";
        console.log("paused?", recorderAudioIsPaused);
        if (recorderAudioIsPaused) {
            // If the audio is paused, resume it
            resumeAudioRecording();
        } else {
            console.log("Pausing Audio Recording...");
            audioRecorder.pause();
            pauseRecordingButton.innerHTML = '<i class="fas fa-play"></i>&nbsp;Reprendre l\'enregistrement';
            clearInterval(elapsedTimeTimer);
        }

    }

    // Resumes the currently paused audio recording
    function resumeAudioRecording() {
        console.log("Resuming Audio Recording...");
        audioRecorder.mediaRecorder.resume();
        pauseRecordingButton.innerHTML = '<i class="fas fa-pause"></i>&nbsp;Pause';
        handleRecordingTime();
    }

    // Starts the audio recording
    function startAudioRecording() {

        console.log("Recording Audio...");

        // If a previous audio recording is playing, pause it
        let recorderAudioIsPlaying = !audioElement.paused;
        console.log("paused?", !recorderAudioIsPlaying);
        if (recorderAudioIsPlaying) {
            audioElement.pause();
        }

        // Start recording using the audio recording API
        audioRecorder.start()
            .then(() => {
                audioRecordStartTime = new Date();
                showRecordingButton();
            })
            .catch(error => {

                if (error.message.includes("mediaDevices API or getUserMedia method is not supported in this browser.")) {
                    console.log("To record audio, use browsers like Chrome and Firefox.");
                    displayBrowserNotSupportedOverlay();
                }

                switch (error.name) {
                    case 'AbortError':
                        console.log("An AbortError has occured.");
                        break;
                    case 'NotAllowedError':
                        console.log("A NotAllowedError has occured. User might have denied permission.");
                        break;
                    case 'NotFoundError':
                        console.log("A NotFoundError has occured.");
                        break;
                    case 'NotReadableError':
                        console.log("A NotReadableError has occured.");
                        break;
                    case 'SecurityError':
                        console.log("A SecurityError has occured.");
                        break;
                    case 'TypeError':
                        console.log("A TypeError has occured.");
                        break;
                    case 'InvalidStateError':
                        console.log("An InvalidStateError has occured.");
                        break;
                    case 'UnknownError':
                        console.log("An UnknownError has occured.");
                        break;
                    default:
                        console.log("An error occured with the error name " + error.name);
                };
            });
    }

    // Stop the currently started audio recording & sends it
    function stopAudioRecording() {

        console.log("Stopping Audio Recording...");
        audioRecorder.stop()
            .then(audioAsblob => {

                record_attempt++;
                console.log("Audio Recorded Successfully.");
                elapsedTime = 0;
                elapsedTimeTag.innerHTML = "00:00";
                hideRecordingButton();
                clearInterval(elapsedTimeTimer);

                microphoneButton.innerHTML = '<i class="fas fa-microphone"></i>&nbsp;Commencer un nouvel enregistrement';

                // Add the audio to the #records list
                var record = document.createElement("tr");
                record.classList.add("record_attempt");
                record.id = "record_attempt_" + record_attempt;
                record.innerHTML = recordedHTML(record_attempt)
                $("#records").append(record);

                var audio = document.createElement("source");
                audio.src = URL.createObjectURL(audioAsblob);
                audio.type = audioAsblob.type;
                document.getElementById("audio_element_" + record_attempt).appendChild(audio);

                putAudio(audioAsblob);
            })
            .catch(error => {
                //Error handling structure
                switch (error.name) {
                    case 'InvalidStateError':
                        console.log("An InvalidStateError has occured.");
                        break;
                    default:
                        console.log("An error occured with the error name " + error);
                };
            });
    }

    // Computes the elapsed recording time since the moment the function is called in the format h:m:s
    function handleRecordingTime() {
        elapsedTimeTimer = setInterval(() => {
            elapsedTime += 1000
            displayRecordingTime(elapsedTime);
        }, 1000); // Every second
    }

    // Display elapsed time during audio recording
    function displayRecordingTime(elapsedTime) {

        elapsedTimeDisp = computeElapsedTime(elapsedTime);
        elapsedTimeTag.innerHTML = elapsedTimeDisp;

        if (checkMaxTime(elapsedTime)) {
            stopAudioRecording();
        }
    }

    // Check the maxTime and give 10% of additional time
    function checkMaxTime(elapsedTime) {
        console.log(elapsedTime)
        console.log(maxTime * 1000)

        if (elapsedTime > (maxTime * 1000)) {
            remainingTime = (((maxTime * (1.1)) * 1000) - elapsedTime) / 1000;
            $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Vous avez atteint le temps maximum d'enregistrement. Ce dernier va bientôt s'arrêter (" + remainingTime + " secondes restantes)&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
            $("#timer").addClass("text-danger");
        }
        if (elapsedTime > ((maxTime * (1.1)) * 1000)) {
            $("#message").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;L'enregistrement a été arrêté car vous avez dépassé la durée imposée&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
            $("#timer").removeClass("text-danger");
            return true;
        }
        return false;
    }

    // Computes the elapsedTime since the moment the function is called in the format mm:ss or hh:mm:ss
    function computeElapsedTime(elapsedTime) {

        let timeDiff = elapsedTime
        timeDiff = timeDiff / 1000;
        let seconds = Math.floor(timeDiff % 60);
        seconds = seconds < 10 ? "0" + seconds : seconds;
        timeDiff = Math.floor(timeDiff / 60);
        let minutes = timeDiff % 60;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        timeDiff = Math.floor(timeDiff / 60);
        let hours = timeDiff % 24;
        timeDiff = Math.floor(timeDiff / 24);
        let days = timeDiff;
        let totalHours = hours + (days * 24);
        totalHours = totalHours < 10 ? "0" + totalHours : totalHours;

        if (totalHours === "00") {
            return minutes + ":" + seconds;
        } else {
            return totalHours + ":" + minutes + ":" + seconds;
        }
    }


    // API to handle audio recording 
    var audioRecorder = {
        /** Stores the recorded audio as Blob objects of audio data as the recording continues*/
        audioBlobs: [],/*of type Blob[]*/
        /** Stores the reference of the MediaRecorder instance that handles the MediaStream when recording starts*/
        mediaRecorder: null, /*of type MediaRecorder*/
        /** Stores the reference to the stream currently capturing the audio*/
        streamBeingCaptured: null, /*of type MediaStream*/
        /** Start recording the audio 
         * @returns {Promise} - returns a promise that resolves if audio recording successfully started
         */
        start: function () {
            //Feature Detection
            if (!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
                //Feature is not supported in browser
                //return a custom error
                return Promise.reject(new Error('mediaDevices API or getUserMedia method is not supported in this browser.'));
            }

            else {
                //Feature is supported in browser

                //create an audio stream
                return navigator.mediaDevices.getUserMedia({ audio: true }/*of type MediaStreamConstraints*/)
                    //returns a promise that resolves to the audio stream
                    .then(stream /*of type MediaStream*/ => {

                        //save the reference of the stream to be able to stop it when necessary
                        audioRecorder.streamBeingCaptured = stream;

                        //create a media recorder instance by passing that stream into the MediaRecorder constructor
                        audioRecorder.mediaRecorder = new MediaRecorder(stream); /*the MediaRecorder interface of the MediaStream Recording
                    API provides functionality to easily record media*/

                        //clear previously saved audio Blobs, if any
                        audioRecorder.audioBlobs = [];

                        //add a dataavailable event listener in order to store the audio data Blobs when recording
                        audioRecorder.mediaRecorder.addEventListener("dataavailable", event => {
                            //store audio Blob object
                            audioRecorder.audioBlobs.push(event.data);
                        });

                        //start the recording by calling the start method on the media recorder
                        audioRecorder.mediaRecorder.start();
                    });

                /* errors are not handled in the API because if its handled and the promise is chained, the .then after the catch will be executed*/
            }
        },
        pause: function () {
            //pause the recording using the audio recording API
            audioRecorder.mediaRecorder.pause();
        },
        /** Stop the started audio recording
         * @returns {Promise} - returns a promise that resolves to the audio as a blob file
         */
        stop: function () {
            //return a promise that would return the blob or URL of the recording
            return new Promise(resolve => {
                //save audio type to pass to set the Blob type
                let mimeType = audioRecorder.mediaRecorder.mimeType;

                //listen to the stop event in order to create & return a single Blob object
                audioRecorder.mediaRecorder.addEventListener("stop", () => {
                    //create a single blob object, as we might have gathered a few Blob objects that needs to be joined as one
                    let audioBlob = new Blob(audioRecorder.audioBlobs, { type: mimeType });

                    //resolve promise with the single audio blob representing the recorded audio
                    resolve(audioBlob);
                });
                audioRecorder.cancel();
            });
        },
        /** Cancel audio recording*/
        cancel: function () {
            //stop the recording feature
            audioRecorder.mediaRecorder.stop();

            //stop all the tracks on the active stream in order to stop the stream
            audioRecorder.stopStream();

            //reset API properties for next recording
            audioRecorder.resetRecordingProperties();
        },
        /** Stop all the tracks on the active stream in order to stop the stream and remove
         * the red flashing dot showing in the tab
         */
        stopStream: function () {
            //stopping the capturing request by stopping all the tracks on the active stream
            audioRecorder.streamBeingCaptured.getTracks() //get all tracks from the stream
                .forEach(track /*of type MediaStreamTrack*/ => track.stop()); //stop each one
        },
        /** Reset all the recording properties including the media recorder and stream being captured*/
        resetRecordingProperties: function () {
            audioRecorder.mediaRecorder = null;
            audioRecorder.streamBeingCaptured = null;

            /*No need to remove event listeners attached to mediaRecorder as
            If a DOM element which is removed is reference-free (no references pointing to it), the element itself is picked
            up by the garbage collector as well as any event handlers/listeners associated with it.
            getEventListeners(audioRecorder.mediaRecorder) will return an empty array of events.*/
        }
    }
})

function deleteAudioFile(id) {
    if (confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?")) {
        $("#record_attempt_" + id).remove();
        deleteAudio(id);
    }
}

function downloadAudio(id) {
    var audio = document.getElementById("audio_element_" + id);
    var blob = audio.getElementsByTagName("source")[0].src;
    var type = audio.getElementsByTagName("source")[0].type;
    var filename = "enregistrement_" + id + "." + type.split("/")[1];
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    a.href = blob;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(blob);
}

function sendAudio(id) {
    passwordModal.show();
    $("#passwordSubmit").click(function () {
        var password = $("#password").val();
        passwordModal.hide();
        var name = $("#username").val();
        name = name.replace(/ /g, "_");

        if (name == "") {
            $("#message").html("<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-exclamation\"></i>&nbsp;Merci de renseigner votre nom avant de déposer votre travail&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")
            $("#username").addClass("is-invalid");
            return;
        } else {
            $("#username").removeClass("is-invalid");
            var audio = document.getElementById("audio_element_" + id);
            var blob = audio.getElementsByTagName("source")[0].src;
            var type = audio.getElementsByTagName("source")[0].type;
            var date = new Date();
            date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "_" + date.getHours() + "-" + date.getMinutes();
            var filename = "audio_" + audio_id + "-name_" + name + "-date_" + date + "." + type.split("/")[1].split(";")[0];

            var xhr = new XMLHttpRequest();
            xhr.open('GET', blob, true);
            xhr.responseType = 'blob';
            xhr.onload = e => {
                data = xhr.response;
                var reader = new FileReader();
                reader.readAsDataURL(data);
                reader.onloadend = function () {
                    base64 = reader.result;
                    base64 = base64.split(',')[1]
                    socket.emit("audio", { id: audio_id, file: base64, filename: filename, pass: password }, (data) => {
                        if (data == "success") {
                            $('#message').html("<div class='alert alert-success alert-dismissible fade show' role='alert'><i class=\"fa-solid fa-circle-check\"></i>&nbsp;Votre travail a bien été déposé&nbsp;!<button type='button' class='close' data-bs-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>")

                            $("#records").empty();
                            record_attempt = 0;
                            elapsedTime = 0;
                            resetDatabase();
                        } else if (data == "exists") {
                            $('#message').html('<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Une erreur s\'est produite lors de l\'envoi du fichier audio. Merci de modifier votre nom&nbsp;!<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                        } else if (data == "wrong_pass") {
                            $('#message').html('<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Le code de dépôt est incorrect&nbsp;!<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                        } else {
                            $('#message').html('<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fa-solid fa-circle-exclamation"></i>&nbsp;Une erreur s\'est produite lors de l\'envoi du fichier audio&nbsp;!<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                        }
                    });
                }

            }
            xhr.send();
        }
    })


}