var url_string = window.location.href; //window.location.href
var url = new URL(url_string);
var classroom = url.searchParams.get("classe");
setCookie('classroom', classroom, 1);

$(document).ready(function () {

  // Instanciate password modal
  var passwordModal = new bootstrap.Modal(document.getElementById("passwordModal"), {});

  // On press enter in password input
  $("#password").keyup(function (event) {
    if (event.keyCode === 13) {
      $("#passwordSubmit").click();
    }
  });

  // Instanciate link modal
  var linkModal = new bootstrap.Modal(document.getElementById("linkModal"), {});

  // Instanciate toast
  var toastEl = document.getElementById('notif')
  notif = new bootstrap.Toast(toastEl, { animation: true, autohide: true, delay: 10000 })

  // Create QRCode for the classroom
  var qrcode = new QRCode("qrcode", {
    text: window.location.href,
    width: 720,
    height: 720,
  });

  // Join the classroom and load the content
  socket.on("connect", () => {
    console.log(socket.id);
    socket.emit('load_classroom', classroom);
  });

  // If classroom is refreshed
  socket.on('refresh_classroom', () => {
    console.log('refresh_classroom');
    socket.emit('load_classroom', classroom);
  });

  // Receive the classroom content
  socket.on('load_classroom', (data) => {
    console.log('load_classroom');
    passwordModal.hide();
    eraseCookie('classroom');
    // Ask the previous messages
    socket.emit('load_notif', classroom);

    $('#title').html(data.title);
    // Change data.time to HHhMM
    if (data.time != null && data.time != '') {
      data.time = data.time.split(':');
      data.time = "Durée : " + data.time[0] + 'h' + data.time[1];
      $('#time').html(data.time);
    } else {
      $('#time').html('');
    }

    if (data.instructions != null && data.instructions != '') {

      instructions = `
      <div class="alert alert-primary" role="alert">
        <div class="row" class="mb-1"><span class="mb-2"><i class='fa-solid fa-bullseye'></i>&nbsp;<b>Consignes et objectifs</b>&nbsp;:</span></div>
        <div class="row mb-0 mx-1">
        ` + data.instructions + `
      </div>
        </div>
      `
      $('#instructions').html(instructions);
    } else {

    }

    var objto = document.getElementById("documents_list");
    objto.innerHTML = '';
    if (data.documents.length == 0) {
      var li = document.createElement("div");
      li.classList.add('col-sm-12');
      li.classList.add('mb-3');
      li.innerHTML = '<div class="card h-100 "><div class="card-body"><h5>Aucun document disponible</h5><p class="date">Veuillez contacter votre professeur pour plus d\'informations.</p></div></div>';
      objto.appendChild(li);
    } else {
      for (var i = 0; i < data.documents.length; i++) {
        console.log(data.documents[i]);
        if (data.documents[i].type == 'section') {
          var li = document.createElement("div");
          li.classList.add('row');
          li.classList.add('my-2');
          li.innerHTML = '<h4><i class="fa-solid fa-chevron-right"></i>&nbsp;&nbsp;' + data.documents[i].title + '</h4>';
          objto.appendChild(li);
        } else if (data.documents[i].type == 'note') {
          var li = document.createElement("div");
          li.classList.add('row');
          li.classList.add('my-2');
          li.innerHTML = '<div>' + data.documents[i].text + '</div>';
          objto.appendChild(li);
        } else {
          var li = document.createElement("div");
          li.classList.add('col-sm-6');
          li.classList.add('mb-3');
          if (data.documents[i].icon == null) {
            data.documents[i].icon = '';
          } else {
            data.documents[i].icon = '<i class="' + data.documents[i].icon + '"></i>&nbsp;';
          }
          target = '';
          if (data.documents[i].external == 1) {
            target = 'target="_blank"';
          }
          li.innerHTML = '<div class="card h-100 "><div class="card-body"><h5>' + data.documents[i].icon + "" + data.documents[i].title + '</h5><p class="date">' + data.documents[i].text + '</p></div><div class="card-footer"><a href="' + data.documents[i].url + '" ' + target + ' class="btn btn-primary btn-sm"><i class="fas fa-eye"></i>&nbsp;Consulter</a></div></div>';
          objto.appendChild(li);
        }
      };
    }
  });

  // io on load_classroom_pass
  socket.on('load_classroom_pass', () => {
    // show password modal
    passwordModal.show();
  });


  // io on wrong_password
  $("#passwordError").hide();
  socket.on("error_classroom_password", (data) => {
    $("#passwordError").show();
    if (data == "classroom_id_not_number") {
      $("#passwordError").html("Le numéro de la classe n'est pas un nombre");
    }
    if (data == "wrong_password") {
      $("#passwordError").html("Le mot de passe est incorrect");
    }
  });

  // Receive the previous messages
  socket.on('load_notif2', function (data) {
    // Clear the notif list
    $('#notif_list').html('');
    $('#notif_counter').html('0');

    data = data.data;
    if (data.length == 0) {
      $('#notif_list').append('<li class="list-group-item" id="no_notif">Aucune notification</li>');
    } else {
      for (var i = 0; i < data.length; i++) {
        addNotif(data[i]);
      }
    }
  });

  // io on com_link
  socket.on('com_link', (data) => {
    url = data.url;
    mode = data.mode;
    if (mode == 'notif') {
      $('#notif_icon').html('<i class="fa-solid fa-link"></i>');
      $('#notif_title').html('Nouveau lien envoyé par votre professeur');
      $('#notif_content').html('<a href="' + url + '" target="_blank">' + url + '</a>');
      addNotif({ type: 'link', content: url });
      notif.show();
    }
    if (mode == 'modal') {
      linkModal.show();
      $('#linkModalTitle').html('Nouvelle ressource envoyée par votre professeur');
      $('#linkModalContent').html('<iframe src="' + url + '" width="100%" height="100%"></iframe>');
    }
    if (mode == 'both') {
      $('#notif_icon').html('<i class="fa-solid fa-link"></i>');
      $('#notif_title').html('Nouveau lien envoyé par votre professeur');
      $('#notif_content').html('<a href="' + url + '" target="_blank">' + url + '</a>');
      addNotif({ type: 'link', content: url });
      notif.show();

      $('#linkModalTitle').html('Nouvelle ressource envoyée par votre professeur');
      $('#linkModalContent').html('<iframe src="' + url + '" width="100%" height="100%"></iframe>');
      linkModal.show();
    }
    if (mode == 'window') {
      window.open(url, '_blank');
    }
  });

  // io on com_message
  socket.on('com_message', (message) => {
    $('#notif_icon').html('<i class="fa-solid fa-comment"></i>');
    $('#notif_title').html('Nouveau message');
    $('#notif_content').html(message);
    notif.show();
    addNotif({ type: 'message', content: message });
  });
});

// io on com_file
socket.on('com_file', (data) => {
  $('#notif_icon').html('<i class="fa-solid fa-file"></i>');
  $('#notif_title').html('Nouveau fichier');
  $('#notif_content').html('<a href="' + data.url + '" target="_blank">' + data.url + '</a>');
  notif.show();
  addNotif({ type: 'file', content: data.url });
});

function submitPassword() {
  socket.emit('unlock_classroom', { pass: $('#password').val(), classroom: classroom });
};

function addNotif(data) {
  // Remove the no_notif li
  if ($('#no_notif').length) {
    $('#no_notif').remove();
  }
  // Get the date HH:mm
  if (data.date == null) {
    data.date = Date.now();
  }
  // Convert data.date to int
  data.date = parseInt(data.date);
  var date = new Date(data.date);
  minutes = date.getMinutes();
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  hours = date.getHours();
  if (hours < 10) {
    hours = "0" + hours;
  }
  day = date.getDate();
  if (day < 10) {
    day = "0" + day;
  }
  month = date.getMonth() + 1;
  if (month < 10) {
    month = "0" + month;
  }
  date = day + "/" + month + " " + hours + ":" + minutes;

  if (data.type == "link") {
    var li = document.createElement("span");
    li.classList.add('dropdown-item');
    li.classList.add('d-flex');
    li.classList.add('align-items-center');
    li.innerHTML = `<div class="mr-3"><div class="icon-circle bg-primary"> <i class="fas fa-link text-white"></i></div></div><div>
    <div class="small text-gray-500">`+ date + `</div>
    <a href="`+ data.content + `" target="_blank">` + data.content + `</a>
    </div>
    `;
    var objto = document.getElementById("notif_list");
    objto.insertBefore(li, objto.firstChild);
  } else if (data.type == "message") {
    var li = document.createElement("span");
    li.classList.add('dropdown-item');
    li.classList.add('d-flex');
    li.classList.add('align-items-center');
    li.innerHTML = `<div class="mr-3"><div class="icon-circle bg-success"> <i class="fas fa-comment text-white"></i></div></div><div>
    <div class="small text-gray-500">`+ date + `</div>
    <span class="font-weight-bold">`+ data.content + `</span>
    </div>
    `;
    var objto = document.getElementById("notif_list");
    objto.insertBefore(li, objto.firstChild);
  } else if (data.type == "file") {
    var li = document.createElement("span");
    li.classList.add('dropdown-item');
    li.classList.add('d-flex');
    li.classList.add('align-items-center');
    li.innerHTML = `<div class="mr-3"><div class="icon-circle bg-warning"> <i class="fas fa-file text-white"></i></div></div><div>
    <div class="small text-gray-500">`+ date + `</div>
    <a href="`+ data.content + `" target="_blank">` + data.content + `</a>
    </div>
    `;
    var objto = document.getElementById("notif_list");
    objto.insertBefore(li, objto.firstChild);
  }
  // Get the counter value
  var counter = document.getElementById("notif_counter").innerHTML;
  // Increment the counter value
  counter++;
  // Set the counter value
  document.getElementById("notif_counter").innerHTML = counter;
}

function leaveClassroom() {
  eraseCookie('classroom');
  socket.emit('leave_classroom', classroom);
  window.location.href = '/index';
};