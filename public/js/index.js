$(document).ready(function () {

  let col_classroom_width = 400;

  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    // show all .col-classroom
    $(".col-classroom").show();
    // show all .card
    $(".card").show();
    $(".card").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });

    // hide .col-classroom if no card is visible
    $(".col-classroom").filter(function () {
      $(this).toggle($(this).find('.card:visible').length > 0);
    });
  });

  socket.on("connect", () => {
    console.log(socket.id);
    //  io emit load_classrooms
    socket.emit('join_main');
    socket.emit('load_levels');
  });

  // io on load_levels
  socket.on('load_levels', (data) => {
    if (data.length <= 1) {
      $("#scroll-right").hide();
    }

    var objto = document.getElementById("classroom_list");
    objto.innerHTML = '';
    //  create a column for each level
    for (var i = 0; i < data.length; i++) {
      var li = document.createElement("div");
      li.classList.add('col-12');
      li.classList.add('col-classroom');
      li.id = data[i];
      li.innerHTML = '<h4 style="text-align:center;"><span class="badge badge-warning mx-auto">' + data[i] + '</span></h4>';
      objto.appendChild(li);
    };

    socket.emit('load_classrooms');
  });

  // io on load_classrooms
  socket.on('load_classrooms', (data) => {
    eraseCookie('classroom');
    for (var i = data.length - 1; i >= 0; i--) {
      // convert date to french format "lundi 1 janvier"
      if (data[i].date != null && data[i].date != '') {
        var options = { weekday: 'long', month: 'long', day: 'numeric' };
        var thedate = new Date(data[i].date);
        data[i].date = thedate.toLocaleDateString("fr-FR", options);
      }


      // create a card for each classroom
      var li = document.createElement("div");
      li.classList.add('card');
      li.classList.add('mb-3');
      // li.classList.add('h-100');
      if (data[i].active == 1) {
        li.innerHTML = '<div class="card-body"><strong><h5 class="mb-0">' + data[i].title + '</h5></strong><p class="date">' + data[i].date + '</p></div><div class="card-footer"><a href="/classroom?classe=' + data[i].id + '" class="btn btn-primary btn-sm"><i class="fas fa-door-open"></i>&nbsp;Entrer</a></div>';
      } else {
        li.innerHTML = '<div class="card-body"><strong><h5 class="mb-0">' + data[i].title + '</h5></strong><p class="date">' + data[i].date + '</p></div><div class="card-footer"><a href="#" class="btn btn-secondary btn-sm disabled"><i class="fas fa-door-open"></i>&nbsp;Entrer</a></div>';
      }

      // insert card in div with id = data[i].level
      document.getElementById(data[i].level).appendChild(li);

    };


    col_classroom_width = $("#classroom_list").children().first().outerWidth(true);
  });

  // scroll classroom list to the right
  $("#scroll-right").click(function () {
    col_classroom_width = $("#classroom_list").children().first().outerWidth(true);
    $("#classroom_list").animate({ scrollLeft: "+=" + col_classroom_width });
  });

  // scroll classroom list to the left
  $("#scroll-left").click(function () {
    col_classroom_width = $("#classroom_list").children().first().outerWidth(true);
    $("#classroom_list").animate({ scrollLeft: "-=" + col_classroom_width });
  });

  $("#classroom_list").scroll(function () {
    scroll_visible();
  })
  $("#scroll-left").hide();
});

// scroll visible on window resize
$(window).resize(function () {
  scroll_visible();
});


function scroll_visible() {
  // hide #scroll-right if #classroom_list is not scrollable
  if ($("#classroom_list").scrollLeft() + $("#classroom_list").innerWidth() >= $("#classroom_list")[0].scrollWidth) {

    $("#scroll-right").hide();
  } else {
    $("#scroll-right").show();
  }

  // hide #scroll-left if #classroom_list is not scrollable
  if ($("#classroom_list").scrollLeft() <= 0) {
    $("#scroll-left").hide();
  } else {
    $("#scroll-left").show();
  }
}