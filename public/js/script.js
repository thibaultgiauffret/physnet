var socket = io();

$(function () {
  var includes = $("[data-include]");
  jQuery.each(includes, function () {
    var file = "views/" + $(this).data("include") + ".html";
    $(this).load(file);
  });
});

$(function () {
  var includes = $("[data-include-modules]");
  jQuery.each(includes, function () {
    var file = $(this).data("include-modules") + ".html";
    $(this).load(file);
  });
});

function loadJS(FILE_URL, async = true) {
  let scriptEle = document.createElement("script");

  scriptEle.setAttribute("src", FILE_URL);
  scriptEle.setAttribute("type", "text/javascript");
  scriptEle.setAttribute("async", async);

  document.body.appendChild(scriptEle);

  // success event 
  scriptEle.addEventListener("load", () => {
    console.log("JS file loaded successfully")
  });
  // error event
  scriptEle.addEventListener("error", (ev) => {
    console.log("Error on loading JS file", ev);
  });
}

function openTab(evt, tabName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");

  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function setCookie(name, value, time) {
  var expires = "";
  if (time) {
    var date = new Date();
    date.setTime(date.getTime() + time * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = name + "=; Max-Age=-99999999;";
}

function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "classroom.json", true); // Replace 'my_data' with the path to your file
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function getObjects(obj, key, val) {
  var objects = [];
  for (var i in obj) {
    if (!obj.hasOwnProperty(i)) continue;
    if (typeof obj[i] == "object") {
      objects = objects.concat(getObjects(obj[i], key, val));
    }
    //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
    else if ((i == key && obj[i] == val) || (i == key && val == "")) {
      //
      objects.push(obj);
    } else if (obj[i] == val && key == "") {
      //only add if the object is not already in the array
      if (objects.lastIndexOf(obj) == -1) {
        objects.push(obj);
      }
    }
  }
  return objects;
}

function UrlExists(url) {
  var http = new XMLHttpRequest();
  http.open("HEAD", url, false);
  http.send();
  return http.status != 404;
}

function eraseAllCookies() {
  document.cookie = "username=; Max-Age=-99999999;";
  document.cookie = "question=; Max-Age=-99999999;";
  document.cookie = "results=; Max-Age=-99999999;";
  document.cookie = "timeout=; Max-Age=-99999999;";
  document.cookie = "quiz=; Max-Age=-99999999;";
  document.cookie = "time=; Max-Age=-99999999;";
}

// Redirection handler
socket.on('redirect', (url) => {
  window.location.replace(url);
});

// Display messages
$(document).ready(function () {
  var params = new URLSearchParams(window.location.search)
  var message = params.get('message');
      var content = params.get('content');

  if (message == "denied") {
    document.getElementById("message").innerHTML =
      '<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fas fa-lock"></i>&nbsp;Accès refusé !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
  }else if (message == "success") {
    if (content != null) {
        document.getElementById("message").innerHTML =
          '<div class="alert alert-success alert-dismissible fade show" role="alert"><i class="fas fa-circle-check"></i>&nbsp;' + content + '<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    } else {
      document.getElementById("message").innerHTML =
        '<div class="alert alert-success alert-dismissible fade show" role="alert"><i class="fas fa-circle-check"></i>&nbsp;Opération effectuée avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
  } else if (message == "danger") {
      if (content != null) {
        document.getElementById("message").innerHTML =
          '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fas fa-circle-xmark"></i>&nbsp;' + content + '<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      } else {
        document.getElementById("message").innerHTML =
          '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fas fa-circle-xmark"></i>&nbsp;Une erreur est survenue !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      }
    } else if (message == "warning"){
      if (content != null) {
        document.getElementById("message").innerHTML =
          '<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fas fa-exclamation-triangle"></i>&nbsp;' + content + '<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      } else {
        document.getElementById("message").innerHTML =
          '<div class="alert alert-warning alert-dismissible fade show" role="alert"><i class="fas fa-exclamation-triangle"></i>&nbsp;Une erreur est survenue !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      }
    }

})