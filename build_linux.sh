#!/bin/bash
echo "Building PhysNet for Linux..."
# Remove the existing dist directory
rm -rf ./dist
# Create a new dist directory
mkdir ./dist
mkdir ./dist/physnet-linux
# Build the excutables
pkg . --targets node18-linux-x64 --output ./dist/physnet-linux/physnet-linux
# Copy the public, protected, db, and data directories to the dist directory
cp ./public ./dist/physnet-linux/public -r
cp ./protected ./dist/physnet-linux/protected -r
cp ./db ./dist/physnet-linux/db -r
cp ./data ./dist/physnet-linux/data -r
cp ./functions ./dist/physnet-linux/functions -r
cp ./cred.json ./dist/physnet-linux/cred.json -r
# Create a start_physnet.sh file in the dist directory
echo "#!/bin/bash" > ./dist/physnet-linux/start_physnet.sh
echo "./physnet-linux" >> ./dist/physnet-linux/start_physnet.sh
chmod +x ./dist/physnet-linux/start_physnet.sh
# Copy the database.db file to the dist directory
cp database.db ./dist/physnet-linux/database.db
# Find all database.db.backup files in the dist directory and delete them
find ./dist/physnet-linux -name "database.db.backup" -exec rm -rf {} \;
# Compress the dist directory without the .exe file and node_sqlite3.node
cd ./dist
zip -r ./physnet-linux.zip ./physnet-linux
rm -rf ./physnet-linux
echo "Done!"