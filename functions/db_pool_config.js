var sqlite3 = require('sqlite3').verbose();
genericPool = require("generic-pool");

const opts = {
    max: 10, // maximum size of the pool
    min: 1 // minimum size of the pool
};

module.exports = {
    getPool: function (db_loc = "./database.db") {
        return genericPool.createPool(
            {
                create: function () {
                    db = new sqlite3.Database(db_loc);
                    // db.run("PRAGMA journal_mode = WAL");
                    return db;
                },
                destroy: function (db) {
                    db.close();
                }
            }
            , opts);
    }
}