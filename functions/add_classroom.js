// Module to handle classrooms creation
module.exports = function (io, socket, path, log) {

    const pool = require("./db_pool_config").getPool();

    socket.on("add_classroom", function (data) {

        // Check if documents list is empty
        if (data.documents.length == 0) {
            log("Documents list cannot be empty")
            io.to("admin").emit("add_classroom", "Il faut au moins un document !");
            return;
        }

        // Promise to add documents
        addDocuments(data.documents).then(function (docId) {

            docId = JSON.stringify(docId);

            // Add classroom to database
            addClassroom(data, docId).then(function (classroomId) {
                log("Classroom successfully added : " + classroomId)
                io.to("admin").emit("add_classroom", "success");
            }).catch(function (err) {
                log("Error adding classroom")
                log(err)
                io.to("admin").emit("add_classroom", "error");
            })

        }).catch(function (err) {
            log("Error adding documents")
            log(err)
            io.to("admin").emit("add_classroom", "error");
        });

    });

    function addClassroom(data, docId) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                var stmt = db.prepare("INSERT INTO classrooms_base (title, level, date, instructions, documents, time, active, visible, pass) VALUES (?,?,?,?,?,?,?,?,?)").run([data.title, data.level, data.date, data.instructions, docId, data.time, data.active, data.visible, data.password], function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("err");
                    } else {
                        log("Inserted classroom row : " + this.lastID);
                        resolve(this.lastID);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });

    }

    function addDocument(data) {
        return new Promise(function (resolve, reject) {

            if (data.type == "doc") {
                pool.acquire().then((db) => {

                    var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?,?,?,?,?,?)").run([data.type, data.icon, data.title, data.text, data.url, data.external], function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        if (this.changes == 0) {
                            log("Document adding failed");
                            reject("Document adding failed");
                        } else {
                            log("Inserted document row : " + this.lastID);
                            resolve(this.lastID);
                        }
                    });
                    stmt.finalize();
                    pool.release(db);
                });
            } else if (data.type == "section") {
                pool.acquire().then((db) => {
                    var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?,?,?,?,?,?)").run([data.type, "", data.title, "", "", 0], function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        if (this.changes == 0) {
                            log("Section addition to classroom_documents failed");
                            reject("Section addition to classroom_documents failed");
                        } else {
                            log("Inserted section row : " + this.lastID);
                            resolve(this.lastID);
                        }
                    });
                    stmt.finalize();
                    pool.release(db);
                });
            } else if (data.type == "note") {
                pool.acquire().then((db) => {
                    var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?,?,?,?,?,?)").run([data.type, "", "", data.text, "", 0], function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        if (this.changes == 0) {
                            log("Note addition to classroom_documents failed");
                            reject("Note addition to classroom_documents failed");
                        } else {
                            log("Inserted note row : " + this.lastID);
                            resolve(this.lastID);
                        }
                    });
                    stmt.finalize();
                    pool.release(db);
                });
            } else {
                // Get the module addToDB function from modules
                const module = require("./modules/" + data.type + "/addToDB.js");
                module.addToDB(path, data, log).then(function (id) {
                    resolve(id);
                }).catch(function (err) {
                    reject(err);
                })
            }
        });

    }

    function addDocuments(data) {

        return new Promise(function (resolve, reject) {
            let docId = [];

            // Add each document to database
            data.forEach(function (doc) {
                addDocument(doc).then(function (id) {
                    docId.push(id);
                    if (docId.length == data.length) {
                        resolve(docId);
                    }
                }).catch(function (err) {
                    reject(err);
                });
            })
        });

    }
}
