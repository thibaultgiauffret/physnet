const { get } = require('http');

// Module to handle file
module.exports = function (io, socket, root, db_path, fs, log) {

    const path = require('path');
    const pool = require(root + '/functions/db_pool_config.js').getPool(db_path);

    // Send index of notebook
    socket.on('join_notebook', (id) => {
        // Check if id is a number
        if (isNaN(id)) {
            // Redirect to home page
            socket.emit("redirect", "/index?message=danger&content=Le cahier Python demandé n'existe pas.");
            return;
        }
        getFileIndex(id).then((data) => {
            if (data == "notebook_not_found") {
                // Redirect to home page
                socket.emit("redirect", "/index?message=danger&content=Le cahier Python demandé n'existe pas.");
                return;
            }
            // remove pass from data
            delete data.pass;
            socket.emit('index_notebook', data);
        }).catch((err) => {
            log(err);
            socket.emit("redirect", "/index?message=danger&content=Une erreur est survenue lors du chargement du module de cahier Python. Veuillez contacter votre professeur.");
            return;
        })
    });

    function getFileIndex(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM notebook_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        resolve("notebook_not_found");
                    } else {
                        resolve(res);
                        // Remove pass from res
                        delete res.pass;
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    // Receive upload file 
    socket.on('uploadNotebook', (data, callback) => {
        filename = data.filename;
        user_pass = data.pass;

        checkNotebookPassword(data.notebook_id).then((pass) => {
            if (pass == user_pass) {
                var file_content = data.file;
                var file = JSON.stringify(file_content);
                // Save file to file
                fs.writeFile(root + '/data/assignments/notebook/' + filename, file, function (err) {
                    if (err) {
                        log(err);
                        callback("error");
                    } else {
                        callback("success");
                    }
                }
                );
            } else {
                callback("wrong_pass");
            }
        });
    });

    function checkNotebookPassword(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT pass FROM notebook_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject(err);
                    } else {
                        resolve(res.pass);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("create_notebook", (data, callback) => {
        // Upload notebook file
        var notebook_file = data.notebook_file;
        getLastNotebookId().then((id) => {
            id++;
            var notebook_file_name = "notebook_" + id + ".ipynb";
            var notebook_file_relpath = 'notebook_files/' + notebook_file_name;
            var notebook_file_path = root + '/data/shared_files/' + notebook_file_relpath;
            var buff = new Buffer.from(notebook_file, 'base64');
            uploadFileNotebook(notebook_file_path, buff).then(() => {

                if (data.notebook_aux == "") {
                    var notebook_aux_relpath = "";
                } else {
                    var notebook_aux_relpath = 'notebook_files/' + data.notebook_aux_name;
                    var notebook_aux_path = root + '/data/shared_files/' + notebook_aux_relpath;
                    var buff = new Buffer.from(data.notebook_aux, 'base64');
                    uploadFileNotebook(notebook_aux_path, buff).then(() => {
                        log("Notebook aux file uploaded");
                    }).catch((err) => {
                        log(err);
                        callback("error");
                    })
                }

                var doc_file = data.doc_file;
                if (doc_file != "") {
                    var doc_file_name = "doc_notebook_" + id + "." + data.doc_file_ext;
                    var doc_file_relpath = doc_file_name;
                    var doc_file_path = root + '/data/shared_files/' + doc_file_relpath;
                    var buff = new Buffer.from(doc_file, 'base64');
                    uploadFileNotebook(doc_file_path, buff).then(() => {
                        log("Doc file uploaded");
                    }).catch((err) => {
                        log(err);
                        callback("error");
                    })
                } else {
                    doc_file_path = data.doc_url;
                }
                pool.acquire().then((db) => {
                    var stmt = db.prepare("INSERT INTO notebook_base (title, instructions, url, aux, split, blocalgo, pass) VALUES (?, ?, ?, ?, ?, ?, ?)").run([data.title, data.instructions, notebook_file_relpath, notebook_aux_relpath, doc_file_relpath, data.blocalgo, data.pass], function (err) {
                        if (err) {
                            log(err);
                            callback("error");
                        }
                        if (this.changes == 0) {
                            log("Notebook adding failed");
                            callback("error");
                        } else {
                            callback("success");
                        }
                    });
                    stmt.finalize();
                    pool.release(db);
                });

            }).catch((err) => {
                log(err);
                callback("error");
            })
        }).catch((err) => {
            log(err);
            callback("error");
        })

    });

    function getLastNotebookId() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare(`SELECT seq FROM sqlite_sequence WHERE name = ? LIMIT 1`).get("notebook_base", function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        resolve(0);
                    } else {
                        resolve(res.seq);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }


    function uploadFileNotebook(file_path, buff) {
        return new Promise((resolve, reject) => {
            fs.writeFile(file_path, buff, function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    socket.on("load_notebook_list", () => {
        loadNotebookList().then((rows) => {
            io.to("admin").emit("load_notebook_list", rows);
        }).catch((err) => {
            log(err);
        })
    })

    function loadNotebookList() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT id, title FROM notebook_base ORDER BY id DESC").all(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No notebook found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    socket.on("select_notebook", (id, callback) => {
        selectNotebook(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function selectNotebook(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM notebook_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No notebook found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    socket.on("modify_notebook", (data, callback) => {
        modifyNotebook(data).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function modifyNotebook(data) {
        return new Promise((resolve, reject) => {
            // Test if file is uploaded
            if (data.notebook_file == "") {
                var notebook_file_relpath = data.old_notebook_file;
            } else {
                var notebook_file = data.notebook_file;
                var notebook_file_name = "notebook_" + data.id + ".ipynb";
                var notebook_file_relpath = 'notebook_files/' + notebook_file_name;
                var notebook_file_path = root + '/data/shared_files/' + notebook_file_relpath;
                var buff = new Buffer.from(notebook_file, 'base64');
                uploadFileNotebook(notebook_file_path, buff).then(() => {
                    log("Notebook file uploaded");
                }).catch((err) => {
                    log(err);
                })
            }
            if (data.notebook_aux == "") {
                var notebook_aux_relpath = data.old_aux_file;
                if (notebook_aux_relpath == "") {
                    // Delete file beginning with "notebook_aux_" + id
                    try {
                        var files = fs.readdirSync(root + '/data/shared_files/notebook_files/');
                        files = files.filter(file => file.startsWith("notebook_aux_" + data.id));
                        for (var i = 0; i < files.length; i++) {
                            // Delete file
                            fs.unlinkSync(root + '/data/shared_files/notebook_files/' + files[i]);
                        }
                    } catch (err) {
                        log(err);
                    }
                }
            } else {
                var notebook_aux_relpath = 'notebook_files/' + data.notebook_aux_name;
                var notebook_aux_path = root + '/data/shared_files/' + notebook_aux_relpath;
                var buff = new Buffer.from(data.notebook_aux, 'base64');
                uploadFileNotebook(notebook_aux_path, buff).then(() => {
                    log("Notebook aux file uploaded");
                }).catch((err) => {
                    log(err);
                })
            }
            if (data.doc_file == "") {
                if (data.doc_url == "") {
                    var doc_file_relpath = data.old_doc_file;
                    if (doc_file_relpath == "") {
                        // Delete file beginning with "doc_notebook_" + id
                        try {
                            var files = fs.readdirSync(root + '/data/shared_files/notebook_files/');
                            files = files.filter(file => file.startsWith("doc_notebook_" + data.id));
                            for (var i = 0; i < files.length; i++) {
                                // Delete file
                                fs.unlinkSync(root + '/data/shared_files/notebook_files/' + files[i]);
                            }
                        } catch (err) {
                            log(err);
                        }
                    }
                } else {
                    var doc_file_relpath = data.doc_url;
                }
            } else {
                var doc_file = data.doc_file;
                var doc_file_name = "doc_notebook_" + data.id + "." + data.doc_file_ext;
                var doc_file_relpath = "notebook_files/" + doc_file_name;
                var doc_file_path = root + '/data/shared_files/' + doc_file_relpath;
                var buff = new Buffer.from(doc_file, 'base64');
                uploadFileNotebook(doc_file_path, buff).then(() => {
                    log("Doc file uploaded");
                }).catch((err) => {
                    log(err);

                })
            }

            pool.acquire().then((db) => {
                var stmt = db.prepare("UPDATE notebook_base SET title = ?, instructions = ?, url = ?, aux = ?, split = ?, blocalgo = ?, pass = ? WHERE id = ?").run([data.title, data.instructions, notebook_file_relpath, notebook_aux_relpath, doc_file_relpath, data.blocalgo, data.pass, data.id], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook update failed");
                    } else {
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })

    }

    socket.on("delete_notebook", (id, callback) => {
        deleteNotebook(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
            callback("error");
        })
    })

    function deleteNotebook(id) {
        return new Promise((resolve, reject) => {
            // Find the file
            try {
                // Delete file beginning with "notebook_" + id
                var files = fs.readdirSync(root + '/data/shared_files/');
                files = files.filter(file => file.startsWith("notebook_" + id));
                for (var i = 0; i < files.length; i++) {
                    // Delete file
                    fs.unlinkSync(root + '/data/shared_files/' + files[i]);
                }

            } catch (err) {
                log(err);
            }

            try {
                // Find file beginning with "doc_notebook_" + id
                var files = fs.readdirSync(root + '/data/shared_files/');
                files = files.filter(file => file.startsWith("doc_notebook_" + id));
                for (var i = 0; i < files.length; i++) {
                    // Delete file
                    fs.unlinkSync(root + '/data/shared_files/' + files[i]);
                }
            } catch (err) {
                log(err);
            }

            pool.acquire().then((db) => {

                var stmt = db.prepare("DELETE FROM notebook_base WHERE id = ?").run(id, function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook deleting failed");
                    } else {
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        });
    }

    socket.on("delete_notebook_aux", (id, callback) => {
        deleteNotebookAux(id).then((data) => {
            callback(data);
        }
        ).catch((err) => {
            log(err);
            callback("error");
        })
    })

    function deleteNotebookAux(id) {
        return new Promise((resolve, reject) => {
            // Find the notebook in database
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT aux FROM notebook_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No notebook found, cannot delete aux file");
                    } else {
                        let aux = res.aux;
                        // Remove notebook_files/ from aux
                        aux = aux.split("notebook_files/")[1];
                        console.log("Aux file to delete : " + aux);
                        let counter = 0;
                        // Delete file beginning with "notebook_aux_" + id
                        try {
                            var files = fs.readdirSync(root + '/data/shared_files/notebook_files/');
                            files = files.filter(file => file.startsWith(aux));
                            for (var i = 0; i < files.length; i++) {
                                // Delete file
                                fs.unlinkSync(root + '/data/shared_files/notebook_files/' + files[i]);
                                // Empty aux in database
                                deleteNotebookAuxInDb(id).then((data) => {
                                    log(data);
                                }).catch((err) => {
                                    log(err);
                                })
                                counter++;
                            }
                        } catch (err) {
                            log(err);
                        }
                        if (counter == 0) {
                            reject("No aux file deleted");
                        }
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })
    }

    function deleteNotebookAuxInDb(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("UPDATE notebook_base SET aux = ? WHERE id = ?").run("", id, function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook aux deleting failed");
                    } else {
                        resolve("Notebook aux removed from database");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })
    }

    socket.on("delete_notebook_doc", (id, callback) => {
        deleteNotebookDoc(id).then((data) => {
            callback(data);
        }
        ).catch((err) => {
            log(err);
            callback("error");
        })
    })

    function deleteNotebookDoc(id) {
        return new Promise((resolve, reject) => {
            // Find the notebook in database
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT split FROM notebook_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No notebook found, cannot delete doc file");
                    } else {
                        // Check if doc is a url
                        if (res.split.startsWith("http")) {
                            resolve("success");
                            return;
                        }
                        let doc = res.split;
                        // Remove notebook_files/ from doc
                        doc = doc.split("notebook_files/")[1];
                        console.log("Doc file to delete : " + doc);
                        let counter = 0;
                        // Delete file beginning with "doc_notebook_" + id
                        try {
                            var files = fs.readdirSync(root + '/data/shared_files/notebook_files/');
                            files = files.filter(file => file.startsWith(doc));
                            for (var i = 0; i < files.length; i++) {
                                // Delete file
                                fs.unlinkSync(root + '/data/shared_files/notebook_files/' + files[i]);
                                // Empty aux in database
                                deleteNotebookDocInDb(id).then((data) => {
                                    log(data);
                                }).catch((err) => {
                                    log(err);
                                })
                                counter++;
                            }
                        } catch (err) {
                            log(err);
                        }
                        if (counter == 0) {
                            reject("No doc file deleted");
                        }
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })
    }

    function deleteNotebookDocInDb(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("UPDATE notebook_base SET split = ? WHERE id = ?").run("", id, function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Notebook doc deleting failed");
                    } else {
                        resolve("Notebook doc removed from database");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })
    }

    socket.on("get_notebook_files", (id) => {
        // With fs, go to /data/assignments/notebook and list all files
        getNotebookFiles(id).then((files) => {
            io.to("admin").emit("notebook_files_admin", files);
        })
    })

    function getNotebookFiles(id) {
        return new Promise((resolve, reject) => {
            fs.readdir(root + '/data/assignments/notebook', function (err, files) {
                if (err) {
                    log(err);
                    reject(err);
                } else {
                    files_list = [];
                    // Filter files witch start with "file_"+id
                    files = files.filter(file => file.startsWith("notebook_" + id));
                    for (var i = 0; i < files.length; i++) {
                        file = {}
                        // Get name between "name_" and next "-"
                        file.name = files[i].split("name_")[1].split("-")[0];
                        // Get date between "date_" and next "."
                        file.date = files[i].split("date_")[1].split(".")[0];
                        // Get path
                        file.path = files[i];
                        files_list.push(file);
                    }
                    resolve(files_list);
                }
            })
        });
    }

    socket.on("delete_notebook_file", (file_name, callback) => {
        log("Deleting file : " + file_name);
        file_path = '/data/assignments/notebook/' + file_name;
        deleteNotebookFile(file_path).then((data) => {
            callback(data);
        })
    })

    function deleteNotebookFile(file_path) {
        return new Promise((resolve, reject) => {
            // check if file exists
            fs.exists(root + file_path, function (exists) {
                if (exists) {
                    // Delete file
                    fs.unlink(root + file_path, function (err) {
                        if (err) {
                            log("Error deleting file : " + file_path);
                            log(err);
                            reject(err);
                        } else {
                            resolve("success");
                        }
                    })
                } else {
                    resolve("not_exists");
                }
            })
        });
    }

    socket.on("delete_notebook_files", (files, callback) => {
        errors = [];
        for (var i = 0; i < files.length; i++) {
            deleteNotebookFile("/data/assignments/notebook/" + files[i]).then((data) => {
                if (data != "success") {
                    errors.push(data);
                }
            }
            )
        }
        if (errors.length == 0) {
            log("All file files deleted : " + JSON.stringify(files));
            callback("success");
        }
    })

}