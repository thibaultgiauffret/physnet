exports.load = function (io, socket, root, log) {
    var db_path = root + '/db/modules/notebook/database.db';
    fs = require('fs-extra');
    require('./notebook.js')(io, socket, root, db_path, fs, log);
};

// Export unauthorised variable
exports.unauthorized = [
    'create_notebook',
    'load_notebook_list',
    'select_notebook',
    'modify_notebook',
    'delete_notebook',
    'get_notebook_files',
    'delete_notebook_file',
    "delete_notebook_files",
    "delete_notebook_aux",
    "delete_notebook_doc"
]

// Module name
exports.name = 'Cahier Python';

// Module id
exports.id = "notebook";