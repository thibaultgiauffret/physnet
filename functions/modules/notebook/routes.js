module.exports = function (app,express,path) {
    app.use(express.static(path + "/public/modules/notebook/"));
    app.get('/notebook', (req, res) => {
        res.sendFile(path + '/public/modules/notebook/notebook.html')
    })
}