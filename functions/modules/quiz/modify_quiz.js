// Module to handle quiz modification
module.exports = function (io, socket, path, db_path, log) {

    const pool = require(path + '/functions/db_pool_config.js').getPool(db_path);

    socket.on("load_quiz_list", function () {
        console.log("Loading quiz list");
        // Get quiz list
        loadQuizList().then((rows) => {
            socket.emit("load_quiz_list", rows);
        }).catch((err) => {
            log(err);
        }
        );
    });

    function loadQuizList() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                // Get quiz list by DESC order
                var stmt = db.prepare(`SELECT id, title FROM quiz_base ORDER BY id DESC`).all(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No quiz found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("select_quiz", function (id) {
        let quiz = {};
        // Get quiz
        selectQuiz(id).then((data_quiz) => {
            quiz.id = data_quiz.id;
            quiz.title = data_quiz.title;
            quiz.display_results = data_quiz.display_results;
            quiz.question_order = data_quiz.question_order;
            // Get questions associated to quiz
            selectQuestions(id).then((data_questions) => {
                let questions = [];
                for (let i = 0; i < data_questions.length; i++) {
                    // Get answers associated to question
                    selectAnswers(data_questions[i].id).then((data_answers) => {
                        questions.push({
                            id: data_questions[i].id,
                            quiz_id: data_questions[i].quiz_id,
                            text: data_questions[i].text,
                            time: data_questions[i].time,
                            type: data_questions[i].type,
                            answers: data_answers
                        });
                        if (questions.length == data_questions.length) {
                            quiz.questions = questions;
                            io.to("admin").emit("select_quiz", quiz);
                        }
                    }).catch((err) => {
                        log(err);
                    })
                }
            }
            ).catch((err) => {
                log(err);
            })
        }).catch((err) => {
            log(err);
        })

    });


    function selectQuiz(quiz_id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare(`SELECT * FROM quiz_base WHERE id = ?`).get(quiz_id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No quiz found with id : " + quiz_id);
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    function selectQuestions(quiz_id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare(`SELECT * FROM quiz_questions WHERE quiz_id = ? ORDER BY id`).all(quiz_id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No question found with quiz_id : " + quiz_id);
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    function selectAnswers(question_id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare(`SELECT * FROM quiz_answers WHERE question_id = ? ORDER BY id`).all(question_id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No answer found with question_id : " + question_id);
                    } else {
                        resolve(res)
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("modify_quiz", function (quiz) {
        log("Modifying quiz " + quiz.id)

        // Check if question list is empty
        if (quiz.questions.length == 0) {
            log("Question list cannot be empty");
            io.to("admin").emit("modify_quiz", "Il faut au moins une question !");
            return;
        }
        // For each question, check if answers list is empty
        for (let i = 0; i < quiz.questions.length; i++) {
            if (quiz.questions[i].answers.length == 0) {
                log("Answers list cannot be empty");
                io.to("admin").emit("modify_quiz", "Il faut au moins une réponse par question !");
                return;
            }
        }

        let errors = 0;

        pool.acquire().then((db) => {

            // Update quiz title

            var stmt = db.prepare(`UPDATE quiz_base SET title = ?, display_results = ?, question_order = ? WHERE id = ?`).run([quiz.title, quiz.display_results, quiz.question_order, quiz.id], function (err) {
                if (err) {
                    errors++;
                    log("Error while updating quiz title : " + quiz.id);
                    log(err);
                }
                if (this.changes == 0) {
                    errors++;
                    log("Error while updating quiz title : " + quiz.id);
                    log(err);
                }
            });
            stmt.finalize();

            // For each question
            for (let i = 0; i < quiz.questions.length; i++) {
                // Check if question is new
                if (quiz.questions[i].update == true) {
                    // Update question
                    var stmt = db.prepare('UPDATE quiz_questions SET quiz_id = ?, text = ?, time = ?, type = ? WHERE id = ?').run([quiz.id, quiz.questions[i].text, quiz.questions[i].time, quiz.questions[i].type, quiz.questions[i].id], function (err) {
                        if (err) {
                            errors++;
                            log("Error while updating question : " + quiz.questions[i].id);
                            log(err);
                        }
                        if (this.changes == 0) {
                            errors++;
                            log("Error while updating question : " + quiz.questions[i].id);
                            log(err);
                        }
                    });
                    stmt.finalize();

                    // For each answer
                    for (let j = 0; j < quiz.questions[i].answers.length; j++) {
                        // Check if answer is new
                        if (quiz.questions[i].answers[j].update == true) {
                            // Update answer
                            var stmt = db.prepare('UPDATE quiz_answers SET question_id = ?, text = ?, points = ?, letter = ? WHERE id = ?').run([quiz.questions[i].answers[j].question_id, quiz.questions[i].answers[j].text, quiz.questions[i].answers[j].points, quiz.questions[i].answers[j].letter, quiz.questions[i].answers[j].id], function (err) {
                                if (err) {
                                    errors++;
                                    log("Error while updating answer : " + quiz.questions[i].answers[j].id);
                                    log(err);
                                }
                                if (this.changes == 0) {
                                    errors++;
                                    log("Error while updating answer : " + quiz.questions[i].answers[j].id);
                                    log(err);
                                }
                            });
                            stmt.finalize();
                        } else {
                            log("Inserting new answer : " + quiz.questions[i].answers[j].text)
                            // Insert new answer
                            var stmt = db.prepare('INSERT INTO quiz_answers (question_id, text, points, letter) VALUES (?, ?, ?, ?)').run([quiz.questions[i].answers[j].question_id, quiz.questions[i].answers[j].text, quiz.questions[i].answers[j].points, quiz.questions[i].answers[j].letter], function (err) {
                                if (err) {
                                    errors++;
                                    log("Error while inserting answer : " + quiz.questions[i].answers[j].text);
                                    log(err);
                                }
                                if (this.changes == 0) {
                                    errors++;
                                    log("Error while inserting answer : " + quiz.questions[i].answers[j].text);
                                    log(err);
                                }
                            });
                            stmt.finalize();
                        }
                    }
                } else {
                    let last_id;

                    modifyAddQuestion(quiz, i).then(function (inserted_question) {


                        last_id = inserted_question;

                        // For each answer in new question
                        for (let j = 0; j < quiz.questions[i].answers.length; j++) {
                            log("Inserting new answer : " + last_id + " " + quiz.questions[i].answers[j].text)
                            if (last_id != undefined) {
                                var stmt = db.prepare('INSERT INTO quiz_answers (question_id, text, points, letter) VALUES (?, ?, ?, ?)').run([last_id, quiz.questions[i].answers[j].text, quiz.questions[i].answers[j].points, quiz.questions[i].answers[j].letter], function (err) {
                                    if (err) {
                                        errors++;
                                        log("Error while inserting answer : " + quiz.questions[i].answers[j].text);
                                        log(err);
                                    }
                                    if (this.changes == 0) {
                                        errors++;
                                        log("Error while inserting answer : " + quiz.questions[i].answers[j].text);
                                        log(err);
                                    }
                                });
                                stmt.finalize();
                            } else {
                                log("Error while inserting new answer : no new question id")
                            }
                        }
                    }).catch(function (err) {
                        log("Error adding new question")
                        log(err)
                    });

                }
            }

            let removed_questions = JSON.stringify(JSON.stringify(quiz.removed_questions));
            removed_questions = removed_questions.replace('"[', "");
            removed_questions = removed_questions.replace(']"', "");
            removed_questions = "(" + removed_questions + ")";

            // Delete removed questions
            var stmt = db.prepare("DELETE FROM quiz_questions WHERE id IN " + removed_questions).run(function (err) {
                if (err) {
                    log("Error deleting questions");
                    log(err);
                }
                if (this.changes == 0) {
                    log("No question deleted");
                } else {
                    log("Deleted question row : " + removed_questions);
                }
            });
            stmt.finalize();


            // Delete removed answers associated to removed questions
            var stmt = db.prepare("DELETE FROM quiz_answers WHERE question_id IN " + removed_questions).run(function (err) {
                if (err) {
                    log("Error deleting answers");
                    log(err);
                }
                if (this.changes == 0) {
                    log("No answer deleted");
                } else {
                    log("Deleted answer row associated to questions : " + removed_questions);
                }
            });
            stmt.finalize();

            let removed_answers = JSON.stringify(JSON.stringify(quiz.removed_answers));
            removed_answers = removed_answers.replace('"[', "");
            removed_answers = removed_answers.replace(']"', "");
            removed_answers = "(" + removed_answers + ")";

            // Delete removed answers
            var stmt = db.prepare("DELETE FROM quiz_answers WHERE id IN " + removed_answers).run(function (err) {
                if (err) {
                    log("Error deleting answers");
                    log(err);
                }
                if (this.changes == 0) {
                    log("No answer deleted");
                } else {
                    log("Deleted answer row : " + removed_answers);
                }
            });
            stmt.finalize();
            pool.release(db);
        })


        if (errors == 0) {
            log("Quiz modified successfully")
            io.to("admin").emit("modify_quiz", "success");
        } else {
            log("Error while modifying quiz")
            io.to("admin").emit("modify_quiz", "fail");
        }
    });

    function modifyAddQuestion(quiz, i) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Insert new question
                var stmt = db.prepare('INSERT INTO quiz_questions (quiz_id, text, time, type) VALUES (?, ?, ?, ?)').run([quiz.id, quiz.questions[i].text, quiz.questions[i].time, quiz.questions[i].type], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        errors++;
                        reject("Error while inserting question : " + quiz.questions[i].text);
                    } else {
                        resolve(this.lastID);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });

        })
    }

    socket.on("delete_quiz", function (quiz_id) {
        let errors = 0;
        pool.acquire().then((db) => {
            // Delete quiz
            var stmt = db.prepare(`DELETE FROM quiz_base WHERE id = ?`).run(quiz_id, function (err) {
                if (err) {
                    errors++;
                    log("Error while deleting quiz : " + quiz_id);
                    log(err);
                }
                if (this.changes == 0) {
                    errors++;
                    log("Error while deleting quiz : " + quiz_id);
                } else {
                    log("Deleted quiz row : " + quiz_id);
                }
            });
            stmt.finalize();

            // Delete the questions and answers associated to the quiz
            selectQuestions(quiz_id).then((rows) => {

                pool.release(db);

                for (let i = 0; i < rows.length; i++) {
                    // Delete question
                    var stmt = db.prepare(`DELETE FROM quiz_questions WHERE id = ?`).run(rows[i].id, function (err) {
                        if (err) {
                            errors++;
                            log("Error while deleting question : " + rows[i].id + " from quiz : " + quiz_id);
                            log(err);
                        }
                        if (this.changes == 0) {
                            errors++;
                            log("No question deleted");
                        }
                    });
                    stmt.finalize();
                    // Delete answers
                    var stmt = db.prepare(`DELETE FROM quiz_answers WHERE question_id = ?`).run(rows[i].id, function (err) {
                        if (err) {
                            errors++;
                            log("Error while deleting answers from question : " + rows[i].id + " from quiz : " + quiz_id);
                            log(err);
                        }
                        if (this.changes == 0) {
                            errors++;
                            log("No answer deleted");
                        }
                    });
                    stmt.finalize();

                    // Delete results associated to quiz_id

                    var stmt = db.prepare("DELETE FROM quiz_results WHERE quiz_id = ?").run(quiz_id, function (err) {
                        if (err) {
                            errors++;
                            log("Error while deleting results from quiz : " + quiz_id);
                            log(err);
                        }
                        if (this.changes == 0) {
                            log("No result deleted");
                        }
                    });
                    stmt.finalize();
                }

                pool.release(db);
            }).catch((err) => {
                errors++;
                log("Error while selecting questions from quiz : " + quiz_id);
                log(err);
            });
        }).catch((err) => {
            errors++;
            log("Error while selecting questions from quiz : " + quiz_id);
            log(err);
        });

        if (errors == 0) {
            log("Quiz deleted successfully")
            io.to("admin").emit("delete_quiz", "success");
        } else {
            log("Error while deleting quiz")
            io.to("admin").emit("delete_quiz", "fail");
        }
    });


}