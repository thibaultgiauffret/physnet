module.exports.modifyInDB = function (path, data, log) {
    const pool = require(path + '/functions/db_pool_config.js').getPool(path + '/database.db');
    return new Promise((resolve, reject) => {

        pool.acquire().then((db) => {

            // Check if quiz already exists
            if (data.update == true) {
                var stmt = db.prepare("UPDATE classrooms_documents SET type = ?, icon = ?, title = ?, text = ?, url = ?, external = ? WHERE id = ?").run([data.type, "fa-regular fa-square-check", "QCM", data.text, "./quiz?id=" + data.id, 0, data.doc_id], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Quiz updating failed");
                    } else {
                        log("Updated quiz row");
                        resolve(data.doc_id);
                    }
                });
                stmt.finalize();
            } else {
                var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?, ?, ?, ?, ?, ?)").run([data.type, "fa-regular fa-square-check", "QCM", data.text, "./quiz?id=" + data.id, 0], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("Quiz adding failed");
                    } else {
                        log("Added quiz row : " + this.lastID);
                        resolve(this.lastID);
                    }
                });
                stmt.finalize();
            }
            pool.release(db);
        });
    })
}