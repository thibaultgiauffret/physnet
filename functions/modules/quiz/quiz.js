module.exports = function (socket, path, db_path, log) {

  const pool = require(path + '/functions/db_pool_config.js').getPool(db_path);

  // Join quiz room
  socket.on("join_quiz", function (quiz) {
    socket.join("quiz_" + quiz);
    log("L'utilisateur " + socket.id + " a rejoint le quiz " + quiz + ".");
  });

  // Get the quiz infos for the index page (title, number of questions, time, points)
  socket.on("index_quiz", function (quiz) {
    log("Le quiz " + quiz + " a été demandé.");

    if (isNaN(quiz)) {
      socket.emit("redirect", "/index?message=danger&content=Le quiz demandé n'existe pas !");
      return;
    }

    getInfosIndex(quiz).then(data => {
      if (data == "quiz_not_found") {
        socket.emit("redirect", "/index?message=danger&content=Le quiz demandé n'existe pas !");
        return;
      }
      let title = data.title;
      let questions_max = data.questions_max;
      let time = data.time;
      getQuestionsIndex(quiz).then(data => {
        let questions = "";
        data.forEach(function (row) {
          questions += row.id + ",";
        });
        questions = questions.slice(0, -1);
        getPointsIndex(questions).then(data => {
          let points = data[0].points;
          let row = {
            title: title,
            questions_max: questions_max,
            time: time,
            points: points,
          }
          socket.emit("index", row);
        }).catch(err => {
          log("Error getting points")
          log(err);
        });
      }).catch(err => {
        log("Error getting questions")
        log(err);
      });
    }).catch(err => {
      log("Error getting quiz infos")
      log(err);
    });
  });

  function getInfosIndex(quiz) {
    // Promise to get the quiz infos
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {

        var stmt = db.prepare("SELECT quiz_base.title, COUNT(DISTINCT quiz_questions.id) AS questions_max, SUM(quiz_questions.time) AS time FROM quiz_base INNER JOIN quiz_questions ON quiz_base.id = quiz_questions.quiz_id WHERE quiz_base.id = ?").get(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.title == null) {
            resolve("quiz_not_found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });

  }

  function getQuestionsIndex(quiz) {
    // Promise to get the questions
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT quiz_questions.id FROM quiz_questions WHERE quiz_id = ?").all(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No question found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });
  }

  function getPointsIndex(questions) {
    // Promise to get the points for each question
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT SUM(points) AS points FROM quiz_answers WHERE question_id IN (" + questions + ")").all(function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No points found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });
  }

  // Register a user to a quiz
  socket.on("register_quiz", function (data) {
    username = data.username;
    quiz = data.quiz;
    if (isNaN(quiz)) {
      socket.emit("redirect", "/index?message=danger&content=Le quiz demandé n'existe pas !");
      return;
    }
    // Check if the username contains only letters and numbers
    if (username == "" || /^[a-zA-Z0-9]+$/.test(username) == false) {
      destination = "/quiz?id=" + quiz + "&message=warning&content=Veuillez entrer un nom d'utilisateur valide (contenant seulement des lettres ou des chiffres)";
      socket.emit("redirect_quiz", { destination: destination, list_questions: [] });
      return;
    }

    datetime = Date.now();

    register(username, datetime, quiz).then(data => {
      let userExists = data;
      var destination = "/question";
      if (userExists == false) {
        getQuestionOrder(quiz).then(order => {
          getQuestionsIndex(quiz).then(data => {
            if (order == 1) {
              // Shuffle the questions
              data = shuffle(data)
              socket.emit("redirect_quiz", { destination: destination, list_questions: data });
            } else {
              socket.emit("redirect_quiz", { destination: destination, list_questions: data });
            }
            log("L'utilisateur " + username + " a été enregistré au quiz " + quiz + ".")
          }).catch(err => {
            log(err)
          })
        }).catch(err => {
          log(err)
        })
      } else {
        destination = "/quiz?id=" + quiz + "&message=danger&content=Cet utilisateur existe déjà !";
        socket.emit("redirect_quiz", { destination: destination, list_questions: [] });
      }
    }).catch(err => {
      log(err)
    })
  });

  function shuffle(array) {
    let currentIndex = array.length, randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }

  // Get the question and the answers for a quiz
  socket.on("question", function (data) {
    log("La question " + data.question + " du quiz " + data.quiz + " a été demandée par " + data.username + ".");
    username = data.username;
    question = data.question;
    quiz = data.quiz;

    let question_data;
    getQuestion(question).then(data => {
      question_data = data

      let answers_data;
      getAnswers(question).then(data => {
        answers_data = data
        // Shuffle the answers
        answers_data = shuffle(answers_data)
        question_data["answers"] = answers_data
        socket.emit("question", question_data);
      })
        .catch(err => {
          log(err)
        })
    })
      .catch(err => {
        log(err)
      })

  });

  // Save the user answer
  socket.on("user_answer", function (data) {
    username = data.username;
    quiz = data.quiz;
    question = data.question;
    answer = data.answer;
    answer_letter = data.answer_letter;
    next_question = data.next_question;

    log(
      username +
      " a répondu à la question " +
      question +
      " et à donné la réponse : " +
      answer
    );

    saveAnswers(username, quiz, question, answer, answer_letter);

    if (next_question == true) {
      socket.emit("next_question", {});
    }
  });

  // End the quiz
  socket.on("end_quiz", function (data) {
    username = data.username;
    quiz = data.quiz;
    deactivateUser(username, quiz).then(deleted => {
      getDisplayResults(quiz).then(display => {
        if (display == 1) {
          fullResults(data);
        } else {
          pointsResults(data);
        }
      }).catch(err => {
        log(err)
      })
    }).catch(err => {
      log(err)
    })
  });

  function deactivateUser(username, quiz) {
    // Promise to deactivate the user
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("UPDATE quiz_results SET active=0 WHERE username = ? AND quiz_id = ?").run([username, quiz], function (err) {
          if (err) {
            reject(err);
          }
          if (this.changes == 0) {
            reject("User not deactivated");
          } else {
            resolve(true);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });
  }


  function fullResults(data, admin = false) {
    let username = data.username;
    let quiz = data.quiz;
    let id = data.id;

    if (!admin) {
      log(username + " a terminé le quiz " + quiz);
    }

    getResultsUser(username, quiz).then(data => {
      let userResults = data[0];
      userResults.questions = JSON.parse(userResults.questions);
      userResults.answers = JSON.parse(userResults.answers);
      userResults.answers_letters = JSON.parse(userResults.answers_letters);

      getAllQuestions(quiz).then(data => {
        allQuestions = data

        getAllAnswers(quiz).then(data => {
          allAnswers = data

          if (admin) {
            socket.emit("results_admin", {
              id: id,
              username: username,
              quiz: quiz,
              allQuestions: allQuestions,
              allAnswers: allAnswers,
              userResults: userResults,
            });
          } else {
            socket.emit("results", {
              allQuestions: allQuestions,
              allAnswers: allAnswers,
              userResults: userResults,
            });
          }


        }).catch(err => {
          log(err)
        })
      }).catch(err => {
        log(err)
      })
    }).catch(err => {
      log(err)
    })

  }

  function pointsResults(data) {
    let username = data.username;
    let quiz = data.quiz;

    getResultsUser(username, quiz).then(data => {
      let userResults = data[0];
      userResults.questions = JSON.parse(userResults.questions);
      userResults.answers = JSON.parse(userResults.answers);

      getPointsAnswers(quiz).then(data => {

        socket.emit("results", {
          allAnswers: data,
          userResults: userResults,
        });
      }).catch(err => {
        log(err)
      })
    }).catch(err => {
      log(err)
    })
  }


  function getDisplayResults(quiz) {
    // Promise to get the quiz infos
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT display_results FROM quiz_base WHERE id=?").get(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            reject("No quiz found");
          } else {
            resolve(res.display_results);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });
  }

  // Get the text, the time and the type of a specific question
  function getQuestion(question) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        // get the question and the time
        var stmt = db.prepare("SELECT quiz_questions.text, quiz_questions.time, quiz_questions.type FROM quiz_questions WHERE quiz_questions.id = ?").get(question, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            reject("No question found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Get the answers for a specific question (no points)
  function getAnswers(question) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        let answers = [];
        var stmt = db.prepare("SELECT id, letter, text FROM quiz_answers WHERE quiz_answers.question_id=?").all(question, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No answer found");
          } else {
            resolve(res)
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Get the answers for a specific question (with points). Used for the results page (end of the quiz)
  function getAllAnswers(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT quiz_questions.id AS question_id, quiz_answers.id AS answer_id, quiz_answers.text AS answer_text, quiz_answers.letter AS answer_letter, quiz_answers.points AS answer_points FROM quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.question_id WHERE quiz_questions.quiz_id=?").all(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No answer found");
          } else {
            resolve(res)
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Get the answers for a specific question (with points). Used for the results page (end of the quiz)
  function getPointsAnswers(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT quiz_questions.id AS question_id, quiz_answers.id AS answer_id, quiz_answers.points AS answer_points FROM quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.question_id WHERE quiz_questions.quiz_id=?").all(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No answer found");
          } else {
            resolve(res)
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Get all the questions for a specific quiz. Used for the results page (end of the quiz)
  function getAllQuestions(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        // get the question 
        var stmt = db.prepare("SELECT quiz_questions.id AS question_id, quiz_questions.text AS question_text FROM quiz_questions WHERE quiz_questions.quiz_id=?").all(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No question found");
          } else {
            resolve(res)
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Register a user to a quiz
  function register(username, date, quiz) {
    return new Promise((resolve, reject) => {
      testUserExists(username, quiz).then(data => {
        let userExists = data;

        if (userExists == false) {
          pool.acquire().then((db) => {
            var stmt = db.prepare("INSERT INTO quiz_results (username, date, quiz_id, questions, answers, answers_letters, active) VALUES (?,?,?, ?, ?, ?, ?)").run([username, date, quiz, "[]", "[]", "[]", 1], function (err) {
              if (err) {
                reject(err);
              }
              if (this.changes == 0) {
                reject("User not registered");
              } else {
                resolve(false);
              }
            });
            stmt.finalize();
            pool.release(db);
          });
        } else {
          resolve(true);
        }

      }).catch(err => {
        log("Error testing if user exists")
        log(err)
      })

    })
  }

  // Test if a user already exists for a specific quiz
  function testUserExists(username, quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("SELECT username FROM quiz_results WHERE username = ? AND quiz_id = ?").get([username, quiz], function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            resolve(false)
          } else {
            resolve(true)
          }
        });
        stmt.finalize();
        pool.release(db);
      })
    });
  }

  function getQuestionOrder(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {

        var stmt = db.prepare("SELECT question_order FROM quiz_base WHERE id = ?").get(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res == undefined) {
            reject("No question order found");
          } else {
            resolve(res.question_order)
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    });
  }

  // Get user's previous answers and add the new
  function saveAnswers(username, quiz, question, answer, answer_letter) {

    getResultsUser(username, quiz).then(data => {

      let results = data[0];

      pool.acquire().then((db) => {


        results.questions = JSON.parse(results.questions);
        results.answers = JSON.parse(results.answers);
        results.answers_letters = JSON.parse(results.answers_letters);

        if (results.questions.includes(question)) {
          log("Question already answered, not adding it again")
          return;
        } else {
          results.questions.push(question);
          results.answers.push(answer);
          results.answers_letters.push(answer_letter);


          var stmt = db.prepare("UPDATE quiz_results SET questions = ?, answers = ?, answers_letters = ? WHERE username = ? AND quiz_id = ?").run([JSON.stringify(results.questions), JSON.stringify(results.answers), JSON.stringify(results.answers_letters), username, quiz], function (err) {
            if (err) {
              log(err)
            }
            if (this.changes == 0) {
              log("Answers not saved for user " + username + " in quiz " + quiz + " : " + data.stringify(results.answers));
            }
          });
          stmt.finalize();
          pool.release(db);
        }
      })
    })
      .catch(err => {
        log(err)
      })
  }

  // Get the results for a specific user and a specific quiz
  function getResultsUser(username, quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        // get the question and the time
        var stmt = db.prepare("SELECT date, questions, answers, answers_letters FROM quiz_results WHERE username = ? AND quiz_id = ?").all([username, quiz], function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No results found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  // Get the results list for a specific quiz
  socket.on('get_results_list', function (quiz) {
    getResultsList(quiz).then(data => {
      // For each user, get the answers
      data.forEach(user => {
        fullResults({ id: user.id, username: user.username, quiz: quiz }, true)
      })

    })
      .catch(err => {
        log(err)
      })
  })

  function getResultsList(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        // Get the question and the time
        var stmt = db.prepare("SELECT id, username FROM quiz_results WHERE quiz_id = ?").all(quiz, function (err, res) {
          if (err) {
            reject(err);
          }
          if (res.length == 0) {
            reject("No results found");
          } else {
            resolve(res);
          }
        });
        stmt.finalize();
        pool.release(db);
      });
    })
  }

  socket.on('delete_results', function (id) {
    id.forEach(quiz => {
      deleteResults(quiz).then(() => {

        log("Results deleted for quiz " + quiz)
        socket.emit('delete_results', 'success');

      }).catch(err => {
        log("Error deleting results for quiz " + quiz)
        log(err)
        socket.emit('delete_results', 'error');
      })
    })
  });

  function deleteResults(quiz) {
    return new Promise((resolve, reject) => {
      pool.acquire().then((db) => {
        var stmt = db.prepare("DELETE FROM quiz_results WHERE id = ?").run(quiz, function (err) {
          if (err) {
            reject(err);
          }
          if (this.changes == 0) {
            reject("Results not deleted");
          } else {
            resolve();
          }
        });
        stmt.finalize();
        pool.release(db);
      })
    })
  }


  socket.emit('delete_results', 'error');
}