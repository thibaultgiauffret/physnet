module.exports = function (app,express,path) {
    app.use(express.static(path + "/public/modules/quiz/"));
    app.get('/quiz', (req, res) => {
        res.sendFile(path + '/public/modules/quiz/quiz.html')
    })
    app.get('/question', (req, res) => {
        res.sendFile(path + '/public/modules/quiz/question.html')
    })
    app.get('/resultats', (req, res) => {
        res.sendFile(path + '/public/modules/quiz/results.html')
    })
}