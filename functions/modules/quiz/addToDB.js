module.exports.addToDB = function (path, data, log) {
    const pool = require(path + '/functions/db_pool_config.js').getPool(path + '/database.db');
    return new Promise((resolve, reject) => {
        pool.acquire().then((db) => {

            var stmt = db.prepare("INSERT INTO classrooms_documents VALUES (NULL,?,?,?,?,?,?)").run([data.type, "fa-regular fa-square-check", "QCM", data.text, "./quiz?id=" + data.id, 0], function (err) {
                if (err) {
                    reject(err);
                }
                if (this.changes == 0) {
                    reject("Quiz adding failed");
                } else {
                    log("Inserted quiz row : " + this.lastID);
                    resolve(this.lastID);
                }
            });
            stmt.finalize();
            pool.release(db);
        })
    })

}