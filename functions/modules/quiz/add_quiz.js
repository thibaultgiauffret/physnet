// Module to handle quiz creation
module.exports = function (io, socket, path, db_path, log) {

    const pool = require(path + '/functions/db_pool_config.js').getPool(db_path);

    socket.on('get_last_quiz_ids', () => {
        // Get last quiz id
        getLastQuestionId().then((last_question_id) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare(`SELECT seq FROM sqlite_sequence WHERE name = ? LIMIT 1`).get("quiz_base", function (err, res) {
                    if (err) {
                        log("Error getting last quiz id")
                        log(err);
                    }
                    if (res == undefined) {
                        io.to("admin").emit('last_ids', { quiz: 0, question: 0 });
                    } else {
                        io.to("admin").emit('last_ids', { quiz: res.seq, question: last_question_id.seq });
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        }).catch((err) => {
            log("Error getting last question id")
            log(err);
        })
    });


    function getLastQuestionId() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                // Get last question id
                var stmt = db.prepare(`SELECT seq FROM sqlite_sequence WHERE name = ? LIMIT 1`).get("quiz_questions", function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No question found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        })
    }

    socket.on('create_quiz', (data) => {
        log("Creating quiz : " + data.title)
        pool.acquire().then((db) => {
            // Check if question list is empty
            if (data.questions.length == 0) {
                log("Question list cannot be empty")
                socket.emit('add_quiz', "Il faut au moins une question !");
                return;
            }
            // For each question, check if answer list is empty
            data.questions.forEach(question => {
                if (question.answers.length == 0) {
                    log("Answer list cannot be empty")
                    socket.emit('add_quiz', "Il faut au moins une réponse pour chaque question !");
                    return;
                }
            });

            // Add quiz to database
            var stmt = db.prepare(`INSERT INTO quiz_base (title, display_results, question_order) VALUES (?, ?, ?)`).run([data.title, data.display_results, data.question_order], function (err) {
                if (err) {
                    log("Error adding quiz : " + data.title + ", quiz not added to database");
                    log(err);
                    socket.emit('add_quiz', "error");
                }
                if (this.changes == 0) {
                    log("Error adding quiz : " + data.title + ", quiz not added to database");
                    socket.emit('add_quiz', "error");
                } else {
                    let questions = data.questions;
                    let question_count = 0;
                    // Add each question to database
                    questions.forEach(question => {
                        var stmt = db.prepare(`INSERT INTO quiz_questions (quiz_id, text, time, type) VALUES (?, ?, ?, ?)`).run([question.quiz_id, question.text, question.time, question.type], function (err) {
                            if (err) {
                                log("Error adding question : " + question.text + ", question not added to database");
                                log(err);
                                socket.emit('add_quiz', "error");
                            }
                            if (this.changes == 0) {
                                log("Error adding question : " + question.text + ", question not added to database");
                                socket.emit('add_quiz', "error");
                            } else {
                                let answers = question.answers;
                                let question_id = this.lastID;
                                // Add each answer to database
                                answers.forEach(answer => {
                                    var stmt = db.prepare(`INSERT INTO quiz_answers (question_id,  text, points, letter) VALUES (?, ?, ?, ?)`).run([question_id, answer.text, answer.points, answer.letter], function (err) {
                                        if (err) {
                                            log("Error adding answer : " + answer.text + ", answer not added to database");
                                            log(err);
                                            socket.emit('add_quiz', "error");
                                        }
                                        if (this.changes == 0) {
                                            log("Error adding answer : " + answer.text + ", answer not added to database");
                                            socket.emit('add_quiz', "error");
                                        }
                                    });
                                    stmt.finalize();
                                });
                                question_count++;
                                // If all questions have been added, send success message
                                if (question_count == questions.length) {
                                    log("Quiz created successfully : " + data.title)
                                    socket.emit('add_quiz', "success");
                                }
                            }
                        });
                        stmt.finalize();
                    });

                }
            })
            stmt.finalize();
            pool.release(db);
        })
    });
}
