module.exports = function (req, res, io) {

    var root = process.cwd();
    var db_path = root + '/db/modules/file_deposit/database.db';
    const pool = require(root + '/functions/db_pool_config.js').getPool(db_path);

    let uploadPath;
    let filename;
    let name;
    let file_deposit_id 
    
    // Get the file deposit id
    file_deposit_id = req.body.file_deposit_id;

    // Get the name
    name = req.body.username;
    // Replace spaces by underscores
    name = name.replace(/ /g, "_");


    if (name == undefined || name == "") {
        return res.redirect('/file_deposit?id=' + file_deposit_id + '&message=name_error');
    }

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.redirect('/file_deposit?id=' + file_deposit_id + '&message=file_error');
    }

    var fileDeposit = req.files.fileDeposit;

    var user_pass = req.body.password;
    checkFilePassword(file_deposit_id).then((pass) => {

        // Check if the password is correct
        if (pass == user_pass) {
            checkFileExtensionsAndSize(file_deposit_id).then((fileRequirements) => {

                // If file deposit size is less than 10MB
                if (fileDeposit.size <= fileRequirements.max_size * 1024 * 1024) {

                    // Get the extension
                    file_extension = "." + fileDeposit.name.split('.').pop();

                    fileRequirements.extensions = fileRequirements.extensions.split(",");

                    // Check if file extension is allowed
                    if (fileRequirements.extensions.includes(file_extension)) {

                        // Get the date
                        let date = new Date();
                        // Convert the date to a string in the format YYYY-MM-DD_HH-MM
                        date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "_" + date.getHours() + "-" + date.getMinutes();
                        // Create the filename
                        filename = "fichier_" + file_deposit_id + "-name_" + name + "-date_" + date + "." + file_extension
                        uploadPath = process.cwd() + '/data/assignments/file_deposit/' + filename;

                        // Use the mv() method to place the file somewhere on your server
                        fileDeposit.mv(uploadPath, function (err) {
                            if (err)
                                return res.status(500).send(err);

                            console.log("File " + filename + " uploaded to " + uploadPath);
                            // Display message
                            res.redirect('/file_deposit?id=' + file_deposit_id + '&message=success');
                        });
                    } else {
                        res.redirect('/file_deposit?id=' + file_deposit_id + '&message=extension_error');
                    }
                } else {
                    res.redirect('/file_deposit?id=' + file_deposit_id + '&message=size_error');
                }
            }).catch((err) => {
                res.redirect('/file_deposit?id=' + file_deposit_id + '&message=error');
            });

        } else {
            res.redirect('/file_deposit?id=' + file_deposit_id + '&message=password_error');
        }

    }).catch((err) => {
        res.redirect('/file_deposit?id=' + file_deposit_id + '&message=error');
    });

    function checkFilePassword(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT pass FROM file_deposit_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject(err);
                    } else {
                        resolve(res.pass);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    function checkFileExtensionsAndSize(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT max_size, extensions FROM file_deposit_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("File deposit not found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }
}