module.exports.addToDB = function (path, data, log) {
    const pool = require(path + '/functions/db_pool_config.js').getPool(path + '/database.db');
    return new Promise((resolve, reject) => {
        pool.acquire().then((db) => {
            var stmt = db.prepare("INSERT INTO classrooms_documents VALUES (NULL,?,?,?,?,?,?)").run([data.type, "fa-solid fa-box-open", "Dépôt de fichier", data.text, "./file_deposit?id=" + data.id, 0], function (err) {
                if (err) {
                    reject(err);
                }
                if (this.changes == 0) {
                    reject("File deposit adding failed");
                } else {
                    log("Inserted file deposit row : " + this.lastID);
                    resolve(this.lastID);
                }
            });
            stmt.finalize();
            pool.release(db);
        })
    })
}