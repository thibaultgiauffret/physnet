// Module to handle file
module.exports = function (io, socket, root, db_path, fs, log) {

    const path = require('path');
    const pool = require(root + '/functions/db_pool_config.js').getPool(db_path);

    // Send index of file_deposit
    socket.on('join_file_deposit', (id) => {
        // Check if id is a number
        if (isNaN(id)) {
            // Redirect to home page
            socket.emit("redirect", "/index?message=danger&content=La boîte de dépôt demandée n'existe pas.");
            return;
        }
        getFileIndex(id).then((data) => {
            if (data == "file_deposit_not_found") {
                // Redirect to home page
                socket.emit("redirect", "/index?message=danger&content=La boîte de dépôt demandée n'existe pas.");
                return;
            }
            socket.emit('index_file_deposit', data);
        }).catch((err) => {
            log(err);
            socket.emit("redirect", "/index?message=danger&content=Une erreur est survenue lors du chargement du module de boîte de dépôt. Veuillez contacter votre professeur.");
            return;
        })
    });

    function getFileIndex(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM file_deposit_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        resolve("file_deposit_not_found");
                    } else {
                        resolve(res);
                        // Remove pass from res
                        delete res.pass;
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        })
    }

    socket.on("create_file_deposit", (data, callback) => {

        pool.acquire().then((db) => {
            var stmt = db.prepare("INSERT INTO file_deposit_base (title, instructions, max_size, extensions, pass) VALUES (?, ?, ?, ?, ?)").run([data.title, data.instructions, data.max_size, data.extensions, data.pass], function (err) {
                if (err) {
                    log(err);
                    callback("error");
                }
                if (this.changes == 0) {
                    log("File deposit adding failed");
                    callback("error");
                } else {
                    callback("success");
                }
            });
            stmt.finalize();
            pool.release(db);
        });
    });

    socket.on("load_file_deposit_list", () => {
        loadFileList().then((rows) => {
            io.to("admin").emit("load_file_deposit_list", rows);
        }).catch((err) => {
            log(err);
        })
    })

    function loadFileList() {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT id, title FROM file_deposit_base ORDER BY id DESC").all(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No file deposit found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    socket.on("select_file_deposit", (id, callback) => {
        selectFile(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function selectFile(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("SELECT * FROM file_deposit_base WHERE id = ?").get(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res == undefined) {
                        reject("No file deposit found");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    socket.on("modify_file_deposit", (data, callback) => {
        modifyFile(data).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
        })
    })

    function modifyFile(data) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {
                var stmt = db.prepare("UPDATE file_deposit_base SET title = ?, instructions = ?, max_size = ?, extensions = ?, pass = ? WHERE id = ?").run([data.title, data.instructions, data.max_size, data.extensions, data.pass, data.id], function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("File deposit updating failed");
                    } else {
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        });
    }

    socket.on("delete_file_deposit", (id, callback) => {
        deleteFile(id).then((data) => {
            callback(data);
        }).catch((err) => {
            log(err);
            callback("error");
        })
    })

    function deleteFile(id) {
        return new Promise((resolve, reject) => {
            pool.acquire().then((db) => {

                var stmt = db.prepare("DELETE FROM file_deposit_base WHERE id = ?").run(id, function (err) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("File deposit deleting failed");
                    } else {
                        resolve("success");
                    }
                });
                stmt.finalize();
                pool.release(db);
            })
        });
    }

    socket.on("get_file_deposit_files", (id) => {
        // With fs, go to /data/assignments/file_deposit and list all files
        getFileDepositFiles(id).then((files) => {
            io.to("admin").emit("file_deposit_files_admin", files);
        })
    })

    function getFileDepositFiles(id) {
        return new Promise((resolve, reject) => {
            fs.readdir(root + '/data/assignments/file_deposit', function (err, files) {
                if (err) {
                    log(err);
                    reject(err);
                } else {
                    files_list = [];
                    // Filter files witch start with "file_"+id
                    files = files.filter(file => file.startsWith("fichier_" + id));
                    for (var i = 0; i < files.length; i++) {
                        file = {}
                        // Get name between "name_" and next "-"
                        file.name = files[i].split("name_")[1].split("-")[0];
                        // Get date between "date_" and next "."
                        file.date = files[i].split("date_")[1].split(".")[0];
                        // Get size
                        file.size = fs.statSync(root + '/data/assignments/file_deposit/' + files[i]).size;
                        // Convert size to Mo
                        file.size = Math.round(file.size / 1000000 * 100) / 100;
                        // Get path
                        file.path = files[i];
                        files_list.push(file);
                    }
                    resolve(files_list);
                }
            })
        });
    }

    socket.on("delete_file_deposit_file", (file_name, callback) => {
        console.log("Deleting file : " + file_name);
        file_path = '/data/assignments/file_deposit/' + file_name;
        deleteFileDepositFile(file_path).then((data) => {
            callback(data);
        })
    })

    function deleteFileDepositFile(file_path) {
        return new Promise((resolve, reject) => {
            // check if file exists
            fs.exists(root + file_path, function (exists) {
                if (exists) {
                    // Delete file
                    fs.unlink(root + file_path, function (err) {
                        if (err) {
                            log("Error deleting file : " + file_path);
                            log(err);
                            reject(err);
                        } else {
                            resolve("success");
                        }
                    })
                } else {
                    resolve("not_exists");
                }
            })
        });
    }

    socket.on("delete_file_deposit_files", (files, callback) => {
        errors = [];
        for (var i = 0; i < files.length; i++) {
            deleteFileDepositFile("/data/assignments/file_deposit/" + files[i]).then((data) => {
                if (data != "success") {
                    errors.push(data);
                }
            }
            )
        }
        if (errors.length == 0) {
            log("All file files deleted : " + JSON.stringify(files));
            callback("success");
        }
    })

}