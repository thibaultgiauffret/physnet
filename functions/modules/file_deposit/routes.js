module.exports = function (app, express, path) {
    app.use(express.static(path + "/public/modules/file_deposit/"));
    app.get('/file_deposit', (req, res) => {
        res.sendFile(path + '/public/modules/file_deposit/file_deposit.html')
    })
    
    app.post('/upload_file_deposit', (req, res) => {
        require('./upload')(req, res);
    })

}

