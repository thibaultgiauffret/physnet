exports.load = function (io, socket, root, log) {
    var db_path = root + '/db/modules/file_deposit/database.db';
    fs = require('fs-extra');
    require('./file_deposit.js')(io, socket, root, db_path, fs, log);
};

// Export unauthorised variable
exports.unauthorized = [
    'create_file_deposit',
    'load_file_deposit_list',
    'select_file_deposit',
    'modify_file_deposit',
    'delete_file_deposit',
    'get_files_deposit',
    'delete_files_deposit']

// Module name
exports.name = 'boîte de dépôt';

// Module id
exports.id = "file_deposit";