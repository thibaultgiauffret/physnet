exports.load = function (io, socket, path, log) {
    var db_path = path + '/db/modules/audio_record/database.db';
    fs = require('fs-extra');
    require('./audio_record.js')(io, socket, path, db_path, fs, log);
};

// Export unauthorised variable
exports.unauthorized = [
    'create_audio',
    'load_audio_list',
    'select_audio',
    'modify_audio',
    'delete_audio',
    'get_audio_files',
    'delete_audio_file']

// Module name
exports.name = 'oral';

// Module id
exports.id = "audio_record";