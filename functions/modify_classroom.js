// Module to handle classrooms modification
module.exports = function (io, socket, path, log) {

    const pool = require("./db_pool_config").getPool();

    socket.on("select_classroom", function (data) {
        // Get the classroom
        selectClassroom(data).then(function (rows) {
            documents_list = JSON.parse(rows[0].documents);
            documents = documents_list.join(",");
            // Get the documents
            selectDocuments(documents).then(function (doc) {
                rows[0].documents = doc;
                io.to("admin").emit("select_classroom", rows[0]);
            }).catch(function (err) {
                log("Error selecting documents");
                log(err);
            });
        }).catch(function (err) {
            log("Error selecting classroom");
            log(err);
        });
    });


    socket.on("modify_classroom", function (data) {
        // Check if documents is empty
        if (data.documents.length == 0) {
            log("Document list cannot be empty");
            io.to("admin").emit("modify_classroom", "Il faut au moins un document !");
            return;
        }

        // Modify the documents associated to the classroom
        modifyDocuments(data.documents).then(function (docId) {
            docId = JSON.stringify(docId);

            updateClassroom(data, docId).then(function () {
                log("Classroom successfully modified : " + data.id);
                io.to("admin").emit("modify_classroom", "success");

                removeClassroomDocuments(data).then(function () {
                    log("Classroom documents successfully removed : " + data.id);
                }).catch(function (err) {
                    log("Error removing classroom documents");
                    log(err);
                });

            }).catch(function (err) {
                log("Classroom modification failed");
                log(err);
                io.to("admin").emit("modify_classroom", "fail");
            });

        }).catch(function (err) {
            log("Error modifying documents");
            log(err)
        });

    });

    function updateClassroom(data, docId) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Modify classroom in database
                var stmt = db.prepare("UPDATE classrooms_base SET title = ?, level = ?, date = ?, instructions = ?, documents = ?, time = ?, active = ?, visible = ?, pass = ? WHERE id = ?").run([data.title, data.level, data.date, data.instructions, docId, data.time, data.active, data.visible, data.password, data.id], function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("No changes in classroom_base");
                    } else {
                        resolve();
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    function removeClassroomDocuments(data) {
        return new Promise(function (resolve, reject) {

            let removed_documents = data.removed_documents.join(",");
            if (removed_documents == "") {
                reject("No document removed in classroom_documents");
            } else {

                pool.acquire().then((db) => {
                    // Delete removed documents
                    var stmt = db.prepare("DELETE FROM classrooms_documents WHERE id IN " + removed_documents).run(function (err, res) {
                        if (err) {
                            reject(err);
                        }
                        if (this.changes == 0) {
                            reject("No document removed in classroom_documents");
                        } else {
                            resolve();
                        }
                    });
                    stmt.finalize();

                    pool.release(db);
                });
            }
        });
    }


    function selectClassroom(data) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Get classroom data
                var stmt = db.prepare("SELECT * FROM classrooms_base WHERE id = ? LIMIT 1").all(data, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        reject("No results in classroom_base");
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    function selectDocuments(data) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Get documents data
                var stmt = db.prepare(`SELECT id, type, icon, title, text, url, external FROM classrooms_documents WHERE id IN (${data})`).all(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        log("No documents found in classroom_documents");
                        resolve([]);
                    } else {
                        data = data.split(",");
                        // Convert data to int
                        data = data.map(function (x) {
                            return parseInt(x);
                        });
                        // Order the documents by data
                        res.sort(function (a, b) {
                            return data.indexOf(a.id) - data.indexOf(b.id);
                        });
                        resolve(res)
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    function modifyDocument(data) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Add documents to database
                // Check the type of document
                if (data.type == "doc") {
                    // Check if document already exists
                    if (data.update == true) {
                        var stmt = db.prepare("UPDATE classrooms_documents SET type = ?, icon = ?, title = ?, text = ?, url = ?, external = ? WHERE id = ?").run([data.type, data.icon, data.title, data.text, data.url, data.external, data.id], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Modified document row : " + data.id);
                                resolve(data.id);
                            }
                        });
                        stmt.finalize();
                    } else {
                        var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?, ?, ?, ?, ?, ?)").run([data.type, data.icon, data.title, data.text, data.url, data.external], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Added document row");
                                resolve(this.lastID);
                            }
                        });
                        stmt.finalize();
                    }
                } else if (data.type == "section") {
                    // Check if document already exists
                    if (data.update == true) {
                        var stmt = db.prepare("UPDATE classrooms_documents SET type = ?, title = ? WHERE id = ?").run([data.type, data.title, data.id], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Modified section row : " + data.id);
                                resolve(data.id);
                            }
                        });
                        stmt.finalize();
                    } else {
                        var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?, ?, ?, ?, ?, ?)").run([data.type, "", data.title, "", "", 0], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Added section row");
                                resolve(this.lastID);
                            }
                        });
                        stmt.finalize();
                    }
                } else if (data.type == "note") {
                    // Check if document already exists
                    if (data.update == true) {
                        var stmt = db.prepare("UPDATE classrooms_documents SET type = ?, text = ? WHERE id = ?").run([data.type, data.text, data.id], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Modified note row : " + data.id);
                                resolve(data.id);
                            }
                        });
                        stmt.finalize();
                    } else {
                        var stmt = db.prepare("INSERT INTO classrooms_documents (type, icon, title, text, url, external) VALUES (?, ?, ?, ?, ?, ?)").run([data.type, "", "", data.text, "", 0], function (err, res) {
                            if (err) {
                                reject(err);
                            }
                            if (this.changes == 0) {
                                reject("No changes in classroom_documents");
                            } else {
                                log("Added note row");
                                resolve(this.lastID);
                            }
                        });
                        stmt.finalize();
                    }
                } else {
                    // Get the module addToDB function from modules
                    const module = require("./modules/" + data.type + "/modifyInDB.js");
                    module.modifyInDB(path, data, log).then(function (id) {
                        resolve(id);
                    }).catch(function (err) {
                        reject(err);
                    })
                }
                pool.release(db);
            });
        });
    }

    async function modifyDocuments(data) {
        const docId = [];
        for (const doc of data) {
            try {
                let id;
                if (doc.id == null) {
                    id = await addDocument(doc);
                } else {
                    id = await modifyDocument(doc);
                }
                docId.push(id);
            } catch (error) {
                log(error);
            }
        }
        return docId;
    }

    function addDocument(data) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Add the document to database
                var stmt = db.prepare("INSERT INTO classrooms_documents VALUES (NULL,?,?,?,?,?,?)").run([data.type, data.icon, data.title, data.text, data.url, data.external], function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("No changes in classroom_documents");
                    } else {
                        log("Inserted document row : " + this.lastID);
                        resolve(this.lastID);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    socket.on("delete_classroom", function (id) {
        // Get all documents to delete
        getDocumentsForDeletion(id).then(function (data) {
            var documents = [];
            if (data.length == 0) {
                log("No documents to delete");
                deleteClassroom(id).then(function () {
                    log("Classroom successfully deleted : " + id);
                    io.to("admin").emit("delete_classroom", "success");
                }).catch(function (err) {
                    log("Error while deleting classroom")
                    log(err);
                });
            } else {
                documents = data[0].documents;
                // Delete them
                deleteDocuments(documents).then(function () {
                    deleteClassroom(id).then(function () {
                        log("Classroom successfully deleted : " + id);
                        io.to("admin").emit("delete_classroom", "success");
                    }).catch(function (err) {
                        log("Error while deleting classroom")
                        log(err);
                    });
                }).catch(function (err) {
                    log("Error while deleting documents")
                    log(err);
                });
            }
        }).catch(function (err) {
            log("Error while getting documents to delete")
            log(err);
        });
    });

    function deleteClassroom(id) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Delete the classroom
                var stmt = db.prepare("DELETE FROM classrooms_base WHERE id = ?").run(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (this.changes == 0) {
                        reject("No changes in classroom_base");
                    } else {
                        resolve();
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }
    function getDocumentsForDeletion(id) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Get all documents to delete
                var stmt = db.prepare("SELECT documents FROM classrooms_base WHERE id = ?").all(id, function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res.length == 0) {
                        log("No documents to delete")
                        resolve([]);
                    } else {
                        resolve(res);
                    }
                });
                stmt.finalize();
                pool.release(db);
            });
        });
    }

    function deleteDocuments(documents) {
        return new Promise(function (resolve, reject) {
            pool.acquire().then((db) => {
                // Convert documents to string for the query
                if (documents.length == 0) {
                    resolve();
                } else {
                    documents = JSON.parse(documents).join(",");

                    // Delete documents from database
                    var stmt = db.prepare(`DELETE FROM classrooms_documents WHERE id IN (${documents})`).run(function (err) {
                        if (err) {
                            reject(err);
                        }
                        if (this.changes == 0) {
                            log("No changes in classroom_documents");
                            resolve();
                        } else {
                            log("Deleted document row : " + documents);
                            resolve();
                        }
                    });
                    stmt.finalize();
                    pool.release(db);
                }
            });
        });
    }

}
