# PhysNet Modules Structure

DISCLAIMER 1 : The PhysNet module system is quite primitive yet. Many improvements are to be expected.

DISCLAIMER 2 : This document describes quickly the structure of the PhysNet modules. It is intended for people who want to create their own modules. It is not complete yet, take a look at the existing modules for more information....

The PhysNet modules are organized as follows:

## functions/modules/<module_id>
This folder contains the module functions called by the server. At least four files are required:
- modules.js contains :
    - the required submodules for the module, 
    - the name of the activity,
    - the id (same as <module_id>),
    - the list of unauthorized socketio events (admin only) associated with the module,
- routes.js contains the routes for the module (used for student interface),
- addToDB.js contains the function to add the activity to the database, called by add_classroom.js module
- modifyInDB.js contains the function to modify the activity to the database, called by modify_classroom.js module

## protected/modules/<module_id>
This folder contains the module files for the admin panel interface. At least three files are required:
- admin.html contains the html code for the admin panel (buttons, modals to manage the activity)
- js/module_defs.json contains :
    - the addActivityButton function name, called when building the activities button list in add/modify classroom modals
    - the addToClassroom function name, called when sending the the classroom infos to the server in add/modify classroom modal
    - the loadActivity function name, called when loading an activity in the classroom modify modal
- js/main.js contains the functions defined in module_defs.json

## public/modules/<module_id>
This folder contains the module files for the student interface. The required files are the ones called by the server in the routes.js file.

## db/modules/<module_id>/database.db
This file is the database for the module. It can be called by submodules and the addToDB/modifyInDB files.