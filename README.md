# PhysNet

PhysNet is a modular virtual classroom app based on NodeJS to serve resources (files, quiz...) and communicate in real time with students. It has been designed to be used during class as an aggregator for all the resources and tools needed by the students. It can also be used as a distance learning tool but it's goal is not to replace the teacher/student interaction.

A blog post in French is available [here](https://www.ensciences.fr/read.php?article=1129).

## Disclaimer

- This project is still in development and is not ready for production. Many features are missing and bugs are to be expected.
- My code is far from perfect and I'm still learning. You may find some aberrations (about performance and security), feel free to report them.
- The app interface is in French and is not translated yet.

## Installation

### Requirements

- NodeJS
- NPM

### Steps

1. Download the sources or clone the repository

```git clone https://framagit.org/ThibGiauffret/physnet.git```

2. Install dependencies with `npm install`
3. Run the server with `npm start`

The network address of the server is displayed in the console. You can access the app from the host machine at the following address : `http://localhost:8000`.

## Usage

### Student : join a classroom and access resources

When on the main page (/index), the classroom are sorted by level in columns. The students can click on the classroom they want to join and enter the password if needed.

There are many types of resources that can be accessed by the students (see below) from the simple link to a file to the quiz module.

### Teacher : create a new course

All the classroom data is stored in the database.db file. To create a new course, you can either modify the database file (advanced users...) or use the admin panel. On the host machine, you can access the admin panel at the following address : `http://localhost:8000/admin`. The default username is `admin` and the default password is `supersecret` (*to change before use in production environment !*).

From there, you can add/modify the classroom and its documents.

Options for the classroom are :
- **Title** : the name of the classroom
- **Date** : the date of the course
- **Duration** : the duration of the course (in hours and minutes)
- **Level** : the course level (used to sort the classrooms on the main page)
- **Password** : the password to join the classroom (leave empty for no password)
- **Active** and **Visible** : if the classroom is not active, the students can see it but not join it. If it is not visible, the students can't see it.
- **Documents** or **Quizzes** : the documents and quizzes to serve to the students (see below)

Options for the documents are :
- **Title** : the name of the document
- **Icon** : the icon to display for the document (see [Font Awesome](https://fontawesome.com/icons?d=gallery))
- **Description** : the description of the document
- **URL** : the URL of the document (can be a link to a file or a website)

Modules can be added to PhysNet. For instance, the quiz module allows you to create quizzes and serve them to the students with the following options :
- **Title** : the name of the quiz
- **Questions** : the questions of the quiz (see below)
    - **Question** : the question content, can be HTML with images and MathJax equations (between `$`)
    - **Type** : the type of the question, simple choice or multiple choice
    - **Duration** : the duration of the question (in minutes and seconds), with a switch to enable/disable the timer
    - **Answers** : the answers of the question (see below)
        - **Letter** : the letter of the answer (A, B, C, D...)
        - **Answer** : the answer content
        - **Points** : the number of points for this answer (can be negative)

Modules are independent from the core files of PhysNet. They are stored in the `modules` folder and can be added or removed easily : more information [here](https://framagit.org/ThibGiauffret/physnet/-/blob/master/modules_structure.md).


### Teacher : send messages and ressources

From the admin panel, you can also **send messages and ressources** to the students. They will see a popup (or a modal, depending your choice) and the message will be stored in their notification center. All the modification done on the classrooms or the messages are sent in real time to the students. That way, they don't have to refresh the page to see the changes.

## Roadmap

- [x] Basic classroom/document interface
- [x] Basic admin panel to manage classrooms and display server ressources
- [x] Messages and notification system (real time)
- [x] Add modules system
- [x] Add quiz module
- [x] Add file deposit module
- [x] Add audio recording module
- [ ] Real time quiz
- [ ] H5P integration
- [ ] Improve existing features and fix bugs (!!!)
- [ ] ...

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE](LICENSE) file for details.

