if (typeof (PhusionPassenger) !== 'undefined') {
  PhusionPassenger.configure({ autoInstall: false });
}

// Loading modules
var express = require("express");
const jwt = require('jsonwebtoken')
var cookieParser = require('cookie-parser')
var http = require("http");
var app = express();
const fileUpload = require('express-fileupload');
app.use(fileUpload());
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
  maxHttpBufferSize: 1e8 // 100 MB
})
var fs = require("fs-extra");
var bodyParser = require("body-parser");
var os = require("os");
var ifaces = os.networkInterfaces();
var favicon = require("serve-favicon");

var pool = require("./functions/db_pool_config.js").getPool();

const bcrypt = require('bcrypt')
const path = require('path');

// Cors configuration
const cors = require('cors');
app.use(cors())

// Express configuration
app.use(favicon(process.cwd() + "/public/images/favicon.png"));
app.use(express.static(process.cwd() + "/public/"));
app.use(express.static(process.cwd() + "/data/shared_files/"));

app.get('/index', (req, res) => {
  res.sendFile(process.cwd() + '/public/index.html')
})
app.get('/classroom', (req, res) => {
  res.sendFile(process.cwd() + '/public/classroom.html')
})

// Admin authentication
app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Get the list of users from cred.json
var users = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'cred.json')), "utf8");

// Server configuration (port)
var port = 8000;
if (typeof (PhusionPassenger) !== 'undefined') {
  server.listen('passenger');
} else {
  server.listen(port);
}

// Get the IP address of the server
Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;
  var myip;
  ifaces[ifname].forEach(function (iface) {
    if ("IPv4" !== iface.family || iface.internal !== false) {
      // Skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // This single interface has multiple ipv4 addresses
      myip = iface.address;
      console.log(
        "Le serveur est démarré à l'adresse suivante : http://" +
        iface.address +
        ":" +
        port
      );
      console.log(
        "La page d'administration est disponible ici : http://" +
        iface.address +
        ":" +
        port +
        "/admin"
      );
    } else {
      // This interface has only one ipv4 adress
      myip = iface.address;
      console.log(
        "Le serveur est démarré à l'adresse suivante : http://" +
        iface.address +
        ":" +
        port
      );
      console.log(
        "La page d'administration est disponible ici : http://" +
        iface.address +
        ":" +
        port +
        "/admin"
      );
    }
    ++alias;
  });
});

// Every 24 hours, generate a new secret for the JWT
const SECRET = require('crypto').randomBytes(64).toString('hex')
setInterval(() => {
  SECRET = require('crypto').randomBytes(64).toString('hex')
}, 1000 * 60 * 60 * 24)

// Authentification form
app.post('/login', (req, res) => {
  if (req.body.username == "" || req.body.password == "") {
    return res.redirect('/auth?message=bad_credentials')
  }

  // Checking
  const user = users.find(u => u.username === req.body.username)

  // If the user doesn't exist
  if (!user) {
    return res.redirect('/auth?message=bad_credentials')
  } else {
    // If the password is wrong
    checkPassword(req.body.password, user.password).then((result) => {
      if (result) {
        const token = jwt.sign({
          id: user.id,
          type: user.type,
          username: user.username
        }, SECRET, { expiresIn: '2 hours' })

        return res.json({ access_token: token })
      } else {
        return res.redirect('/auth?message=bad_credentials')
      }
    })
  }


})

// Modify the username of an user
app.post('/modify_username', (req, res) => {
  if (req.body.old == "" || req.body.new == "") {
    return res.json({ message: "empty_field" })
  }

  // Get the token from the cookies
  const token = req.cookies.token

  // If the token doesn't exist
  if (!token || token === undefined) {
    // Remove the token from cookies
    res.clearCookie('token')
    return res.redirect('/auth?message=need_token')
  } else {
    // Check if the token is valid
    jwt.verify(token, SECRET, (err, decoded) => {
      if (err) {
        res.redirect('/auth?message=bad_token')
      } else {
        // Get the username
        const username = decoded.username
        // Get the user from the cred.json file
        const user = users.find(u => u.username === username)

        // If the old username is wrong
        if (user.username !== req.body.old) {
          return res.json({ message: "user_not_found" })
        } else {
          // If the new username already exists
          if (users.find(u => u.username === req.body.new)) {
            return res.json({ message: "error" })
          } else {
            // Modify the username
            user.username = req.body.new
            // Save the new cred.json file
            fs.writeFileSync(process.cwd() + "/cred.json", JSON.stringify(users))
            return res.json({ message: "success" })
          }
        }
      }
    })
  }
})

// Modify the password of an user
app.post('/modify_password', (req, res) => {
  if (req.body.old == "" || req.body.new == "") {
    return res.json({ message: "empty_field" })
  }

  // Get the token from the cookies
  const token = req.cookies.token

  // If the token doesn't exist
  if (!token || token === undefined) {
    // Remove the token from cookies
    res.clearCookie('token')
    return res.redirect('/auth?message=need_token')
  } else {
    // Check if the token is valid
    jwt.verify(token, SECRET, (err, decoded) => {
      if (err) {
        res.redirect('/auth?message=bad_token')
      } else {
        // Get the username
        const username = decoded.username
        // Get the user from the cred.json file
        const user = users.find(u => u.username === username)
        // If the user doesn't exist
        if (!user) {
          return res.json({ message: "user_not_found" })
        }
        // If the old password is wrong
        checkPassword(req.body.old, user.password).then((result) => {
          if (!result) {
            return res.json({ message: "wrong_password" })
          } else {
            // Modify the password
            hashPassword(req.body.new).then((result) => {
              user.password = result
              // Save the changes
              fs.writeFileSync(process.cwd() + "/cred.json", JSON.stringify(users, null, 2))
              return res.json({ message: "success" })
            })

          }
        })
      }
    })
  }
})

// Hash the password
async function hashPassword(password) {
  const salt = await bcrypt.genSalt(10)
  const hash = await bcrypt.hash(password, salt)
  return hash
}

// Default password : supersecret. Uncomment the following line to generate the hash for this password
// hashPassword("supersecret").then((result) => {
//   console.log("Hash for default password is : "+result)
// })

// Check if the password is correct
async function checkPassword(password, hash) {
  isSame = await bcrypt.compare(password, hash)
  console.log("checkPass : " + isSame)
  return isSame
}

// List all unauthorized socket requests
let unauthorized = [
  "load_classroom_list",
  'get_connected_users',
  'emit_com_link',
  'emit_com_file',
  "emit_com_message",
  'add_classroom',
  'load_classroom_list',
  'modify_classroom',
  'delete_classroom',
  "select_classroom",
  'get_resources',
  'get_data_folder_size',
  'get_database_size',
  'select_classroom_messages',
  'delete_classroom_message',
  'delete_classroom_messages',
  'refresh_classroom',
  'get_modules'
];

/* Load the additionnal modules
   Each module has its own folder in the functions/modules folder with two required files : module.js and routes.js. In module.js, an "unauthorized" array must be defined. This array contains the list of unauthorized requests for this module.
*/

// Additionnal modules name
let modules = []

fs.readdirSync(path.join(process.cwd(), 'functions/modules')).forEach(file => {

  module = require('./functions/modules/' + file + '/module.js')

  // Get the list of unauthorized requests
  unauthorized = unauthorized.concat(module.unauthorized)

  modules.push({ name: module.name, id: module.id })

  // Load the routes.js file
  require('./functions/modules/' + file + '/routes.js')(app, express, process.cwd());
})

// Check token middleware
const checkTokenMiddleware = (req, res, next) => {
  // Get the token from the cookies
  const token = req.cookies.token

  // If the token doesn't exist
  if (!token || token === undefined) {
    // Remove the token from cookies
    res.clearCookie('token')
    return res.redirect('/auth?message=need_token')
  }

  // Check if the token is valid
  jwt.verify(token, SECRET, (err, decodedToken) => {
    if (err) {
      res.redirect('/auth?message=bad_token')
    } else {
      return next()
    }
  })
}

app.get('/auth', (req, res) => {
  res.clearCookie('token')
  res.sendFile(process.cwd() + '/public/login.html')
})

// Protected routes
app.use(checkTokenMiddleware, express.static(process.cwd() + "/protected/"));
app.use(checkTokenMiddleware, express.static(process.cwd() + "/data/"));
// Test if data folder exists
if (!fs.existsSync(process.cwd() + "/data/")) {
  fs.mkdirSync(process.cwd() + "/data/");
}
// Test if data/shared_files folder exists
if (!fs.existsSync(process.cwd() + "/data/shared_files/")) {
  fs.mkdirSync(process.cwd() + "/data/shared_files/");
}
// Test if data/assignments folder exists
if (!fs.existsSync(process.cwd() + "/data/assignments/")) {
  fs.mkdirSync(process.cwd() + "/data/assignments/");
}
// For each module, create a folder in data/assignments
modules.forEach(module => {
  if (!fs.existsSync(process.cwd() + "/data/assignments/" + module.id)) {
    fs.mkdirSync(process.cwd() + "/data/assignments/" + module.id);
  }
})

app.get('/admin', checkTokenMiddleware, (req, res) => {
  // Get the token from the cookies
  const token = req.cookies.token
  const decoded = jwt.decode(token, { complete: false })

  if (decoded.type === 'admin' && decoded.exp > Date.now() / 1000) {
    // If the user is admin and the token is valid
    res.sendFile(process.cwd() + '/protected/admin.html')
  }

})

let reboot = true

/* Core of the socket.io server */
io.sockets.on("connection", function (socket) {

  // Probably not the best way to do it, but it works... Changes are required I guess...

  /* Load the core socket modules */
  require('./functions/load_classrooms.js')(io, socket, log);
  require('./functions/communication.js')(io, socket, process.cwd(), fs, log);
  require('./functions/add_classroom.js')(io, socket, process.cwd(), log);
  require('./functions/modify_classroom.js')(io, socket, process.cwd(), log);
  const monitor = require('./functions/monitor').connect(io, socket, os, fs, process.cwd(), log, modules);

  /* Load the additionnal socket modules */
  fs.readdirSync(path.join(process.cwd(), 'functions/modules')).forEach(file => {
    // Load the module.js file
    require('./functions/modules/' + file + '/module.js').load(io, socket, process.cwd(), log);
  })

  socket.on("get_modules", () => {
    socket.emit("get_modules", modules)
  })

  socket.on('join_main', function () {
    socket.join("main");
  })

  socket.on("join_admin", function (data, callback) {
    // Récupération du token dans le cookie "token"
    const token = data

    const decoded = jwt.decode(token, { complete: false })

    if (decoded.type === 'admin' && decoded.exp > Date.now() / 1000) {
      console.log("Admin logged in");
      socket.join("admin");
      callback(true);
    } else {
      callback(false);
    }

    if (reboot) {
      reboot = false
      socket.emit("reboot")
    }
  });

  socket.use((packet, next) => {
    if (isValid(packet[0])) {
      return next();
    }
    return next(new Error("authentication error"));
  });

  function isValid(event) {
    // Check if the user is in admin room
    if (socket.rooms.has("admin")) {
      return true;
    } else {
      // Check the event name
      if (unauthorized.includes(event)) {
        return false;
      }
      return true;
    }
  }

  log("Client connected with id : " + socket.id);

  // Log the message and send it to the console
  function log(message) {
    console.log(message);
    time = new Date();
    io.to("admin").emit("server_message", "[" + time.getDay() + "/" + time.getMonth() + "/" + time.getFullYear() + " " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds() + "] " + message);
  }
})
