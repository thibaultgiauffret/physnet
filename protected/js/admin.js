var messageModal;
var linkModal;
var fileModal;

var modules = [];

$(document).ready(function () {

    // Rejoin if there's a disconnect
    socket.on("connect", () => {
        console.log(socket.id);
        // Get token from cookie
        const token = getCookie("token");
        if (token != null) {
            socket.emit("join_admin", token, (data) => {
                if (data) {
                    console.log("Admin logged in");
                    init()
                } else {
                    console.log("Admin not logged in");
                }
            });
        } else {
            // Redirect to login page
            window.location.replace("/auth");
        }
    });

    // When the server is disconnected, redirect to login page
    socket.on("reboot", () => {
        // Reload the page
        window.location.reload();
    });

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function init() {
        socket.emit('load_classroom_list');
        socket.emit('get_connected_users');
        socket.emit('get_resources');
        socket.emit('get_data_folder_size');
        socket.emit('get_database_size');
        socket.emit("get_modules");
    };

    socket.on("get_modules", (data) => {
        modules = data;
        addModules();
        addActivitiesButtons("addDocument", "addClassroomActivities");
    });

    socket.on('load_classroom_list', (data) => {
        // Find all select elements with the class classroom_list
        var classroom_list = document.getElementsByClassName("classroom_list");
        // for each select element
        for (var i = 0; i < classroom_list.length; i++) {
            // Clear all select elements
            classroom_list[i].innerHTML = "<option disabled selected value> Choisir une salle de classe </option>";
            $("#modify_classroom").hide();
            // for each classroom in data
            for (var j = 0; j < data.length; j++) {
                // create option element
                var option = document.createElement("option");
                // set value to classroom id
                option.value = data[j].id;
                // set text to classroom title
                option.text = "Classe " + data[j].id + ": " + data[j].title;
                if (data[j].active == 0) {
                    option.text += " (Inactive)";
                }
                if (data[j].visible == 0) {
                    option.text += " (Invisible)";
                }
                // append option to select element
                classroom_list[i].appendChild(option);
            }
        }
    });

    socket.on('server_message', (data) => {
        $("#console").val(function () {
            return this.value + data + "\r\n";
        });
        document.getElementById("console").scrollTop = document.getElementById("console").scrollHeight
    });

    // Get the number of connected users
    socket.on('connected_users', (data) => {
        $("#connected_users").html(data);
    })

    // Ask for the number of connected users every 5 seconds
    setInterval(function () {
        socket.emit('get_connected_users');
        socket.emit('get_resources');
    }, 10000);

    socket.on("resources", (data) => {
        $("#cpu").html(data.cpu + " %");
        $("#memory").html(data.nodeMemory + " / " + data.memory + " Go");
        $("#database").html(data.databaseSize + ' Mo / ' + data.dataFolderSize + ' Mo');
    });

    socket.on("classroom_messages", (data) => {
        console.log(data);
        objTo = $("#message_list_body")
        for (var i = 0; i < data.messages.length; i++) {
            // Convert data.messages[i].date to number
            data.messages[i].date = parseInt(data.messages[i].date);
            // Convert timestamp to date DD/MM HH:MM
            var date = new Date(data.messages[i].date);
            console.log(data.messages[i].date);
            var dateStr = date.getDate() + "/" + (date.getMonth() + 1) + " " + date.getHours() + ":" + date.getMinutes();
            objTo.append('<tr id="message_' + data.messages[i].id + '"><td>' + data.messages[i].id + '</td><td>' + dateStr + '</td><td>' + data.messages[i].type + '</td><td>' + data.messages[i].content + '</td><td><button class="btn btn-sm btn-danger" onclick="deleteMessage(' + data.messages[i].classroom + ',' + data.messages[i].id + ', \'' + data.messages[i].type + '\')"><i class="fas fa-trash"></i></button></td></tr>');
        }


    });

});

function fixSummernote() {
    // Fix for summernote dropdowns
    let buttons = $('.note-editor button[data-toggle="dropdown"]');

    buttons.each((key, value) => {
        //   add attribute data-bs-toggle
        $(value).attr("data-bs-toggle", "dropdown");
        //   remove attribute data-toggle
        $(value).removeAttr("data-toggle");
        // add autoclose to dropdown
        $(value).attr("data-bs-auto-close", "true");
    })
}

function sendLink() {
    var classroom = $("#classroom_list_link").val()
    var url = $("#url").val()
    if (classroom == null || url == "") {
        $("#classroom_list_link").addClass("is-invalid")
        $("#message_send_link").html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Un ou plusieurs champs sont vides !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
        return;
    } else {
        var mode = $("#mode").val()
        socket.emit('emit_com_link', { classroom: classroom, url: url, mode: mode });
        linkModal.hide();
        $("#message").html('<div class="alert alert-success alert-dismissible fade show" role="alert">Lien envoyé !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
    }
}

function sendFile() {
    var classroom = $("#classroom_list_file").val()
    if (classroom == null) {
        $("#classroom_list_file").addClass("is-invalid")
        $("#message_send_file").html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Veuillez sélectionner une classe !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
        return;
    } else {
        var files = $("#file").prop('files')
        socket.emit('emit_com_file', { classroom: classroom, name: files[0].name, overwrite: false }, files[0], (status) => {
            if (status.message == "success") {
                socket.emit('get_data_folder_size');
                fileModal.hide();
                $("#message").html('<div class="alert alert-success alert-dismissible fade show" role="alert">Fichier envoyé !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
            } else if (status.message == "exists") {
                if (confirm("Le fichier existe déjà, voulez-vous l'écraser ?")) {
                    socket.emit('emit_com_file', { classroom: classroom, name: files[0].name, overwrite: true }, files[0], (status) => {
                        if (status.message == "success") {
                            socket.emit('get_data_folder_size');
                            fileModal.hide();
                            $("#message").html('<div class="alert alert-success alert-dismissible fade show" role="alert">Fichier remplacé et envoyé !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                        } else {
                            $("#message_send_file").html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de l\'envoi du fichier !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
                        }
                    });
                }

            } else {
                $("#message_send_file").html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de l\'envoi du fichier !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
            }
        });
    }
}

function sendMessage() {
    var classroom = $("#classroom_list_message").val()
    if (classroom == null) {
        $("#classroom_list_message").addClass("is-invalid")
        $("#message_send_message").html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Veuillez sélectionner une classe !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
        return;
    } else {
        var message = $.trim($("#messageContent").val())
        socket.emit('emit_com_message', { classroom: classroom, message: message });
        messageModal.hide();
        $("#message").html('<div class="alert alert-success alert-dismissible fade show" role="alert">Message envoyé !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
    }

}

function selectClassroomMessage(id) {
    // If option zero is selected, do nothing
    if (id == "") {
        return;
    }
    // Clear the modifyDocument div and show the modify_classroom div
    $("#message_list_body").html("");
    $("#message_list").show();

    // Enable the buttons
    $("#deleteMessages").prop("disabled", false);

    // Send the request to the server
    socket.emit("select_classroom_messages", id);
}

function deleteMessage(classroom, index, type) {
    console.log("deleteMessage(" + classroom + "," + index + ")");
    socket.emit("delete_classroom_message", { classroom: classroom, index: index, type: type }, (status) => {
        console.log("deleteMessage : " + status);
        if (status.message == "success") {
            $("#message_list_body").html("");
            socket.emit("select_classroom_messages", classroom);
        }
    });
}

function deleteAllMessages() {
    // Get the selected classroom
    if (confirm("Voulez-vous vraiment supprimer tous les messages de cette salle de classe ?")) {
        classroom = $("#classroom_list_clean").val();
        socket.emit("delete_classroom_messages", classroom, (status) => {
            console.log("deleteAllMessages : " + status);
            if (status.message == "success") {
                $("#message_clean").html("<div class='alert alert-success alert-dismissible fade show' role='alert'>Messages supprimés !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                $("#message_list_body").html("");
                $("#classroom_list_clean").val("");
                socket.emit("select_classroom_messages", classroom);
            } else {
                $("#message_clean").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Erreur lors de la suppression des messages !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
            }
        });
    }
}

function logout() {
    // Delete the token cookie
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    // Redirect to login page
    window.location.href = "/auth?message=logout";
}

function modifyPassword() {
    var old = $("#oldPassword").val();
    var new1 = $("#newPassword1").val();
    var new2 = $("#newPassword2").val();

    if (new1 != new2) {
        $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Les mots de passe ne correspondent pas !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
        $("#newPassword1").addClass("is-invalid");
        $("#newPassword2").addClass("is-invalid");
        return;
    } else {
        $.ajax({
            url: "/modify_password",
            type: "POST",
            data: { old: old, new: new1 },
            success: function (data) {
                if (data.message == "success") {
                    // Remove token cookie
                    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    // Redirect to the admin page
                    window.location.href = "/auth?message=modify_user";
                } else if (data.message == "wrong_password") {
                    $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Le mot de passe actuel est incorrect !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                    $("#oldPassword").addClass("is-invalid");
                } else if (data.message == "user_not_found") {
                    $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Utilisateur introuvable !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                } else if (data.message == "empty_field") {
                    $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Veuillez remplir tous les champs !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                } else {
                    $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Erreur lors de la modification du mot de passe !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                }
            },
            error: function (data) {
                console.log(data);
                $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Erreur lors de la modification du mot de passe !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
            }
        });
    }
}

function modifyUsername() {
    var newUsername = $("#newUsername").val();
    var oldUsername = $("#oldUsername").val();

    $.ajax({
        url: "/modify_username",
        type: "POST",
        data: { old: oldUsername, new: newUsername },
        success: function (data) {
            if (data.message == "success") {
                // Remove token cookie
                document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                // Redirect to the admin page
                window.location.href = "/auth?message=modify_user";
            } else if (data.message == "user_not_found") {
                $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Utilisateur introuvable !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
                $('#oldUsername').addClass("is-invalid");
            } else if (data.message == "empty_field") {
                $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Veuillez remplir tous les champs !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
            } else {
                $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Erreur lors de la modification du nom d'utilisateur !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
            }
        },
        error: function (data) {
            console.log(data);
            $("#message_password").html("<div class='alert alert-danger alert-dismissible fade show' role='alert'>Erreur lors de la modification du nom d'utilisateur !<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button></div>");
        }
    });
}

// Modules functions

function addActivitiesButtons(target, loc) {
    let buttons = `
    <button type="button" class="btn btn-warning mr-1 mb-1" onclick="addSection(\'`+ target + `\');">
    Titre de section
    </button>
    <button type="button" class="btn btn-warning mr-1 mb-1" onclick="addNote(\'`+ target + `\');">
    Note
    </button>
    <button type="button" class="btn btn-warning mr-1 mb-1" onclick="addDocument(\'`+ target + `\');">
    Document
    </button>`

    // For each module

    for (var i = 0; i < modules.length; i++) {
        // Change first letter to uppercase
        modules[i].name = modules[i].name.charAt(0).toUpperCase() + modules[i].name.slice(1);
        // Create a button
        buttons += `
            <button type="button" class="btn btn-warning mr-1 mb-1" onclick="`+ modules[i].defs.addActivityButton + `(\'` + target + `\');">
            `+ modules[i].name + `
            </button>`
    }

    // Add the buttons to the page
    $("#" + loc).html(buttons);

}

function addModules() {
    var div = $("#modules");
    // Include content of HTML files with JQuery and add to div
    for (var i = 0; i < modules.length; i++) {
        console.log("Module loaded : " + modules[i])
        if (modules[i] == undefined) {
            console.log("Error: module not found");
        } else {
            div.append('<div id="admin_' + modules[i].id + '"></div>');
            $("#admin_" + modules[i].id).load("/modules/" + modules[i].id + "/admin.html");

            // get the module info from the modules/xxx/module_defs?json
            var moduleDefs = $.ajax({
                dataType: "json",
                async: false,
                url: "/modules/" + modules[i].id + "/js/module_defs.json",
                success: function (data) {
                    // add the content of data to the modules[i]
                    modules[i].defs = data;
                },
                error: function (data) {
                    console.log("Error: module_defs.json not found, using default empty values")
                    modules[i].defs = {
                        "addActivityButton": "",
                        "addToClassroom": "",
                        "loadActivity": ""
                    }
                }
            });
        }
    }
}

function addActivitiesToClassroom(location) {
    let activities = []
    let errors = []

    for (var i = 0; i < modules.length; i++) {
        // Get the function name from addToClassroom key
        var functionName = modules[i].defs.addToClassroom;
        // Call the function
        if (!functionName == "") {
            data = window[functionName](location);
            activities = activities.concat(data.activity);
            errors = errors.concat(data.errors);
        }
    }
    return { activities: activities, errors: errors };
}


function addArrows(location) {
    let order;
    if (location == "addDocument") {
        addClassroomOrder++;
        order = addClassroomOrder;
    }
    if (location == "modifyDocument") {
        modClassroomOrder++;
        order = modClassroomOrder;
    }
    $("#" + location + " .accordion-collapse").last().each(function () {
        // Add a button to move up
        $(this).append(`
        <div class="d-flex justify-content-center" style="transform: translate(0px,-15px)">
        <input type="hidden" class="doc_order" value="`+ order + `">
        <div class="btn-group" role="group">
        <button type="button" class="btn btn-sm btn-secondary" onclick="moveUp(this)"><i class="fa-solid fa-arrow-up"></i></button>
        <button type="button" class="btn btn-sm btn-secondary" onclick="moveDown(this)"><i class="fa-solid fa-arrow-down"></i></button>
        </div>

        <button type="button" class="btn btn-sm btn-danger ms-2" onclick="if (confirm('Supprimer le document ?')){deleteEl(this)}"><i class="fa-solid fa-trash"></i></button>
        </div>
        `);

    });
}

function moveUp(el) {
    var previous_accordion = el.parentElement.parentElement.parentElement.parentElement.previousElementSibling;
    var accordion = el.parentElement.parentElement.parentElement.parentElement;
    if (previous_accordion != null) {
        // In each accordion, get the doc_order input and swap the values
        let order = accordion.querySelector(".doc_order").value;
        accordion.querySelector(".doc_order").value = previous_accordion.querySelector(".doc_order").value;
        previous_accordion.querySelector(".doc_order").value = order;
        // Move the accordion
        previous_accordion.before(accordion);
    }
}

function moveDown(el) {
    var next_accordion = el.parentElement.parentElement.parentElement.parentElement.nextElementSibling;
    var accordion = el.parentElement.parentElement.parentElement.parentElement;
    if (next_accordion != null) {
        // In each accordion, get the doc_order input and swap the values
        let order = accordion.querySelector(".doc_order").value;
        accordion.querySelector(".doc_order").value = next_accordion.querySelector(".doc_order").value;
        next_accordion.querySelector(".doc_order").value = order;
        // Move the accordion
        next_accordion.after(accordion);
    }
}

function deleteEl(el) {
    el.parentElement.parentElement.parentElement.remove();
}