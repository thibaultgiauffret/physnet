let doc_modify_count = 0;
let removed_documents = [];
let modClassroomOrder = 0;

$(document).ready(function () {

    // Instanciate the classroomModifyModal
    var classroomModifyModal = new bootstrap.Modal(document.getElementById("classroomModifyModal"), {});

    $("#modifyClassroomSubmit").prop("disabled", true);
    $("#deleteClassroom").prop("disabled", true);

    socket.on("select_classroom", (data) => {

        // Init the form
        $("#mod_id").val(data.id);
        $("#see_classroom").attr("href", "/classroom?classe=" + data.id);
        $("#mod_title").val(data.title);
        $("#mod_level").val(data.level);
        $("#mod_date").val(data.date);
        $("#mod_time").val(data.time);
        $("#mod_password").val(data.pass);
        $("#mod_instructions").summernote("code", data.instructions);

        if (data.active == 1) {
            $("#mod_active").prop("checked", true);
        } else {
            $("#mod_active").prop("checked", false);
        }
        if (data.visible == 1) {
            $("#mod_visible").prop("checked", true);
        } else {
            $("#mod_visible").prop("checked", false);
        }

        // For each document
        for (let i = 0; i < data.documents.length; i++) {
            loadActivities("modifyDocument", data.documents[i]);
        }

        addActivitiesButtons("modifyDocument", "modClassroomActivities");

        // Find all tooltips in modifyDocument div and initialize them
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('#modify_classroom [data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
    });

    function loadActivities(location, activity) {
        if (activity.type == "doc") {
            loadDocuments(activity);
        } else if (activity.type == "section") {
            loadSection(location, activity);
        } else if (activity.type == "note") {
            loadNote(location, activity);
        } else {
            for (var i = 0; i < modules.length; i++) {
                if (modules[i].id == activity.type) {
                    // Get the function name from loadActivity key
                    var functionName = modules[i].defs.loadActivity;
                    // Call the function
                    data = window[functionName](location, activity);
                    addArrows(location);
                }
            }
        }
    }

    socket.on("modify_classroom", (data) => {
        if (data == "success") {
            // Refresh all the classrooms
            socket.emit('refresh_classroom', { id: $("#mod_id").val() });
            // Reload the classroom list
            socket.emit('load_classroom_list');
            // Clear the modifyDocument div
            clearModifyClassroom();

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Classe modifiée avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
            classroomModifyModal.hide();
        } else {
            $("#message_modify_classroom").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la modification de la salle de classe : ${data}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)

        }
    });

    socket.on("delete_classroom", (data) => {
        if (data == "success") {
            // Refresh all the classrooms
            socket.emit('refresh_classroom', { id: $("#mod_id").val() });
            // Reload the classroom list
            socket.emit('load_classroom_list');
            // Clear the modifyDocument div
            $("#modifyClassroomSubmit").prop("disabled", true);
            $("#deleteClassroom").prop("disabled", true);
            removed_documents = [];

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Classe supprimée avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)

            classroomModifyModal.hide();
        } else {
            $("#message_modify_classromm").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la suppression de la salle de classe !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        }

    });
});


function selectClassroom(id) {
    // If option zero is selected, do nothing
    if (id == "") {
        return;
    }
    // Clear the modifyDocument div and show the modify_classroom div
    $("#modifyDocument").html("");
    $("#modify_classroom").show();

    // Enable the buttons
    $("#modifyClassroomSubmit").prop("disabled", false);
    $("#deleteClassroom").prop("disabled", false);

    // Send the request to the server
    socket.emit("select_classroom", id);
}

function loadDocuments(data, location = "modifyDocument") {

    doc_modify_count++;

    var external = "";
    if (data.external == 1) {
        external = "checked";
    }

    // Create the document
    var doc = document.createElement("div");
    doc.classList.add("accordion-item");
    doc.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Document (id : ${data.id})
    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">
    <input type="hidden" class="form-control doc_id" placeholder="ID" aria-label="ID" value="`+ data.id + `">

    <input type="hidden" class="form-control doc_type" value="doc">

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Titre"><i class="fa-solid fa-heading fa-fw"></i></span>
        <input type="text" class="form-control doc_title old" placeholder="Titre du document" aria-label="Titre" value="`+ data.title + `">
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Icône"><i class="fa-solid fa-star fa-fw"></i></span>
        <input type="text" class="form-control doc_icon" placeholder="Icône" aria-label="Icône" style="border-right:none;" value="`+ data.icon + `">
        <span class="input-group-text" style="background-color:#fff;border-left:none;"><i class="doc_icon_display"></i></span>
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Description"><i class="fa-solid fa-paragraph fa-fw"></i></span>
        <input type="text" class="form-control doc_text" placeholder="Description" aria-label="Description"  value="`+ data.text + `">
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text"  data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lien"><i class="fa-solid fa-link fa-fw"></i></span>
        <input type="text" class="form-control doc_url" placeholder="Lien" aria-label="Lien" value="`+ data.url + `">
    </div>

    <div class="form-check form-switch ml-4">
    <input class="form-check-input doc_external" type="checkbox" role="switch" `+ external + `>
    <label class="form-check-label">Ouvrir dans un nouvel onglet ?</label>
    </div>
    </div>
    
    `;
    document.getElementById(location).appendChild(doc);

    let icon_el = $(".doc_icon").last()[0]
    loadIcons(icon_el, data.icon);

    addArrows(location);
}

function loadSection(location, data) {
    doc_modify_count++;

    // Create the section
    var section = document.createElement("div");
    section.classList.add("accordion-item");
    section.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Section (id : ${data.id})
    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <input type="hidden" class="form-control doc_id" placeholder="ID" aria-label="ID" value="`+ data.id + `">
            <input type="hidden" class="form-control doc_type" value="section">
            <div class="input-group mb-3">
                <span class="input-group-text"><i class="fa-solid fa-heading fa-fw"></i></span>
            <input type="text" class="form-control doc_title old" placeholder="Titre de la section" aria-label="Titre" value="`+ data.title + `">
            </div>
        </div>
    </div>
    `;

    document.getElementById(location).appendChild(section);

    addArrows(location);
}

function loadNote(location, data) {
    doc_modify_count++;

    // Create the note
    var note = document.createElement("div");
    note.classList.add("accordion-item");
    note.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Note (id : ${data.id})
    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <input type="hidden" class="form-control doc_id" placeholder="ID" aria-label="ID" value="`+ data.id + `">
            <input type="hidden" class="form-control doc_type" value="note">
            <div class="doc_text old"></div>
        </div>
    </div>
    `;
    document.getElementById(location).appendChild(note);

    var textNote = $(".doc_text").last();
    textNote.summernote();
    textNote.summernote(
        'code', data.text
    );

    addArrows(location);
}

function removeDocument(id) {
    // Store the ids of removed documents
    removed_documents.push(id);
}

function modifyClassroom() {

    let form_errors = []

    // Remove all is-invalid classes
    $("#title").removeClass("is-invalid");
    $("#level").removeClass("is-invalid");
    $("#addDocument input").removeClass("is-invalid");
    $("#addDocument select").removeClass("is-invalid");

    let id = parseInt($("#mod_id").val());
    let titre = $("#mod_title").val();
    let niveau = $("#mod_level").val();

    // Check if titre and niveau are not empty
    if (titre == "") {
        form_errors.push("le titre ne peut pas être vide");
        $("#mod_title").addClass("is-invalid");
    }
    if (niveau == "") {
        form_errors.push("le niveau ne peut pas être vide");
        $("#mod_level").addClass("is-invalid");
    }

    let date = $("#mod_date").val();
    let time = $("#mod_time").val();
    let password = $("#mod_password").val();
    let active = $("#mod_active").prop("checked");
    if (active == true) {
        active = 1;
    }
    let visible = $("#mod_visible").prop("checked");
    if (visible == true) {
        visible = 1;
    }
    let instructions = $("#mod_instructions").summernote('code');
    let documents = [];
    // Get all addDocument h2
    $("#modifyDocument .accordion-header").each(function () {

        let doc_order = $(this).parent().find(".doc_order").val();
        console.log("doc_order : " + doc_order);

        // Test if the document is a doc
        if ($(this).parent().find(".doc_type").val() == "doc") {
            // Determine if the document is an update or a new document
            let update;

            let title = $(this).parent().find(".doc_title");
            if (title.hasClass("old")) {
                update = true;
            } else {
                update = false;
            }
            let external;
            if ($(this).parent().find(".doc_external").prop("checked") == true) {
                external = 1;
            } else {
                external = 0;
            }

            if (title.val() == "") {
                form_errors.push("le titre du document ne peut pas être vide");
                title.addClass("is-invalid");
            }
            let url = $(this).parent().find(".doc_url");
            if (url.val() == "") {
                form_errors.push("le lien du document ne peut pas être vide");
                url.addClass("is-invalid");
            }

            // Create a document object
            let doc = {
                update: update,
                doc_order: doc_order,
                id: parseInt($(this).parent().find(".doc_id").val()),
                type: $(this).parent().find(".doc_type").val(),
                title: title.val(),
                icon: $(this).parent().find(".doc_icon").val(),
                text: $(this).parent().find(".doc_text").val(),
                url: url.val(),
                external: external
            }
            // Add the document object to the documents array
            documents.push(doc);
        } else if ($(this).parent().find(".doc_type").val() == "section") {
            // Determine if the document is an update or a new document
            let update;

            let title = $(this).parent().find(".doc_title");
            if (title.hasClass("old")) {
                update = true;
            } else {
                update = false;
            }

            if (title.val() == "") {
                form_errors.push("le titre de la section ne peut pas être vide");
                title.addClass("is-invalid");
            }

            // Create a document object
            let doc = {
                update: update,
                doc_order: doc_order,
                id: parseInt($(this).parent().find(".doc_id").val()),
                type: $(this).parent().find(".doc_type").val(),
                title: title.val()
            }
            // Add the document object to the documents array
            documents.push(doc);
        } else if ($(this).parent().find(".doc_type").val() == "note") {
            // Determine if the document is an update or a new document
            let update;

            let text = $(this).parent().find(".doc_text");
            if (text.hasClass("old")) {
                update = true;
            } else {
                update = false;
            }

            if (text.summernote("code") == "" || text.summernote("code") == "<p><br></p>") {
                form_errors.push("le texte de la note ne peut pas être vide");
            }

            // Create a document object
            let doc = {
                update: update,
                doc_order: doc_order,
                id: parseInt($(this).parent().find(".doc_id").val()),
                type: $(this).parent().find(".doc_type").val(),
                text: text.summernote("code")
            }
            // Add the document object to the documents array
            documents.push(doc);
        } else {
            // Get the id of the .accordion-collapse in the parent of the accordion-header
            let id = $(this).parent().find(".accordion-collapse").attr("id");
            // Find the doc order
            let doc_order = $(this).parent().find(".doc_order").val();
            console.log("doc_order : " + doc_order);
            // Add the module
            let type = $(this).parent().find(".doc_type").val();
            console.log("Adding module " + type + " to classroom")
            //Find the module type in modules array
            let module = modules.find(x => x.id == type);
            console.log("Module found : " + module.name);
            // Get the function name from addToClassroom key
            var functionName = module.defs.addToClassroom;
            console.log("Function name : " + functionName);
            // Call the function
            if (!functionName == "") {
                data = window[functionName]("modifyDocument", id);
                //  Add the doc order
                data.activity[0].doc_order = doc_order;
                documents = documents.concat(data.activity);
                form_errors = form_errors.concat(data.errors);
            }
        }
    });

    // Order the documents by doc_order
    documents.sort(function (a, b) {
        return a.doc_order - b.doc_order;
    });

    // Check if titre and niveau are not empty
    if (form_errors.length == 0) {
        // Send the classroom object to the server
        socket.emit('modify_classroom', { id: id, title: titre, level: niveau, date: date, instructions: instructions, time: time, password: password, active: active, visible: visible, documents: documents, removed_documents: removed_documents });
    } else {
        form_errors = form_errors.join(", ");
        $("#message_modify_classroom").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la modification de la salle de classe : ${form_errors}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        // Highlight the empty inputs
        if (titre == "") {
            $("#mod_title").addClass("is-invalid");
        }
        if (niveau == "") {
            $("#mod_level").addClass("is-invalid");
        }
    }

}

function clearModifyClassroom() {
    $("#mod_id").val("");
    $("#mod_title").val("");
    $("#mod_level").val("");
    $("#mod_title").removeClass("is-invalid");
    $("#mod_level").removeClass("is-invalid");
    $("#mod_date").val("");
    $("#mod_time").val("");
    $("#mod_password").val("");
    $("#mod_active").prop("checked", false);
    $("#mod_visible").prop("checked", false);
    $("#modifyDocument").html("");
    removed_documents = [];
    doc_modify_count = 0;
    $("#classroom_list_modify").val("").change();
    $("#modifyClassroomSubmit").prop("disabled", true);
    $("#deleteClassroom").prop("disabled", true);
    $("#modify_classroom").hide();
    $("#message_modify_classroom").html("");
    modClassroomOrder = 0;
}

function deleteClassroom() {
    // Ask for confirmation before deleting the classroom
    if (confirm("Êtes-vous sûr de vouloir supprimer cette salle de classe avec ses documents ?")) {
        let id = parseInt($("#mod_id").val());
        socket.emit('delete_classroom', id);
    }
}

// on each button click in the addClassroomActivities div
$(document).on("click", "#modClassroomActivities button", function () {
    // For each element h2 in the addDocument div
    addArrows("modifyDocument");
})

