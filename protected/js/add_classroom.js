let doc_count = 0;
// load icon list from ../protected/json/fa6_icons.json
let icon_list = [];
$.getJSON("../json/fa6_icons.json", function (data) {
    icon_list = data;
});
let addClassroomOrder = 0;

$(document).ready(function () {

    // Instanciate the classroomModal
    var classroomModal = new bootstrap.Modal(document.getElementById("classroomModal"), {});

    // Message after adding a classroom
    socket.on("add_classroom", (data) => {
        if (data == "success") {
            // Refresh all the classrooms
            socket.emit('refresh_classroom', { id: $("#mod_id").val() });
            // Reload the classroom list
            socket.emit('load_classroom_list');
            // Clear the form
            clearAddClassroom();

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Classe ajoutée avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
            classroomModal.hide();
        } else {
            $("#message_add_classroom").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de l'ajout de la salle de classe : ${data}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        }
    });


});

function addDocument(location = "addDocument") {

    doc_count++;

    // Creating the new document
    var doc = document.createElement("div");
    doc.classList.add("accordion-item");
    doc.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_count}">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_count}" aria-expanded="true" aria-controls="panelsStayOpen">
      Nouveau document ${doc_count}

      </button>
    </h2>

    <div id="panel_collapse_${doc_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

    <input type="hidden" class="form-control doc_type" value="doc">

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Titre"><i class="fa-solid fa-heading fa-fw"></i></span>
        <input type="text" class="form-control doc_title" placeholder="Titre du document" aria-label="Titre du document">
        
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Icône"><i class="fas fa-star fa-fw"></i></span>
        <input type="text" class="form-control doc_icon" placeholder="Icône" aria-label="Icône" style="border-right:none;">
        <span class="input-group-text" style="background-color:#fff;border-left:none;"><i class="doc_icon_display"></i></span>
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Description"><i class="fa-solid fa-paragraph fa-fw"></i></span>
        <input type="text" class="form-control doc_text" placeholder="Description" aria-label="Description">
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Lien"><i class="fa-solid fa-link fa-fw"></i></span>
        <input type="text" class="form-control doc_url" placeholder="Lien" aria-label="Lien">
    </div>

    <div class="form-check form-switch ml-4">
    <input class="form-check-input doc_external" type="checkbox" role="switch" checked>
    <label class="form-check-label">Ouvrir dans un nouvel onglet ?</label>
    </div>
    </div>
    
    `;
    document.getElementById(location).appendChild(doc);

    let icon_el = $(".doc_icon").last()[0]
    loadIcons(icon_el);

    // Find all tooltips in modifyDocument div and initialize them
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('#addDocument [data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
}

function addSection(location = "addDocument") {
    doc_count++;

    // Creating the new section
    var doc = document.createElement("div");
    doc.classList.add("accordion-item");
    doc.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_count}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_count}" aria-expanded="true" aria-controls="panelsStayOpen">
        Nouvelle section ${doc_count}
    </button>
    </h2>

    <div id="panel_collapse_${doc_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">

        <input type="hidden" class="form-control doc_type" value="section">

        <div class="input-group mb-3">
            <span class="input-group-text"><i class="fa-solid fa-heading fa-fw"></i></span>
            <input type="text" class="form-control doc_title" placeholder="Titre de la section" aria-label="Titre de la section">
        </div>

    </div>
    `;
    document.getElementById(location).appendChild(doc);
}

function addNote(location = "addDocument") {
    doc_count++;

    // Creating the new note
    var doc = document.createElement("div");
    doc.classList.add("accordion-item");
    doc.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_count}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_count}" aria-expanded="true" aria-controls="panelsStayOpen">
        Nouvelle note ${doc_count}
    </button>
    </h2>

    <div id="panel_collapse_${doc_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">

        <input type="hidden" class="form-control doc_type" value="note">

        <div class="doc_text"></div>

    </div>
    `;
    document.getElementById(location).appendChild(doc);
    var doc_text = $(".doc_text").last()[0];
    $(doc_text).summernote();

}

function loadIcons(icon_el, default_icon = 'fas fa-file') {

    display_el = icon_el.parentElement.querySelector(".doc_icon_display")

    new Iconpicker(icon_el, {
        icons: icon_list,
        showSelectedIn: display_el,
        searchable: true,
        hideOnSelect: true,
        fade: true,
        defaultValue: default_icon,
        valueFormat: val => `${val}`

    })


}

function addClassroom() {

    let form_errors = []

    // Remove all is-invalid classes
    $("#title").removeClass("is-invalid");
    $("#level").removeClass("is-invalid");
    $("#addDocument input").removeClass("is-invalid");
    $("#addDocument select").removeClass("is-invalid");

    let titre = $("#title").val();
    let niveau = $("#level").val();

    // Check if titre and niveau are not empty
    if (titre == "") {
        form_errors.push("le titre ne peut pas être vide");
        $("#title").addClass("is-invalid");
    }
    if (niveau == "") {
        form_errors.push("le niveau ne peut pas être vide");
        $("#level").addClass("is-invalid");
    }

    let date = $("#date").val();
    let time = $("#time").val();
    let password = $("#password").val();
    let active = $("#active").prop("checked");
    if (active == true) {
        active = 1;
    }
    let visible = $("#visible").prop("checked");
    if (visible == true) {
        visible = 1;
    }
    let instructions = $("#instructions").summernote('code');
    // Test if the instructions are empty
    if (instructions == "<p><br></p>") {
        instructions = "";
    }
    let documents = [];

    // Get all addDocument h2
    $("#addDocument .accordion-header").each(function () {

        // Get the document order
        let doc_order = $(this).parent().find(".doc_order").val();

        // Test if the document is a doc
        if ($(this).parent().find(".doc_type").val() == "doc") {

            let external;
            if ($(this).parent().find(".doc_external").is(":checked") == true) {
                external = 1;
            } else {
                external = 0;
            }

            let title = $(this).parent().find(".doc_title");
            if (title.val() == "") {
                form_errors.push("le titre du document ne peut pas être vide");
                title.addClass("is-invalid");
            }
            let url = $(this).parent().find(".doc_url");
            if (url.val() == "") {
                form_errors.push("le lien du document ne peut pas être vide");
                url.addClass("is-invalid");
            }

            // Create a document object
            let doc = {
                doc_order: doc_order,
                type: $(this).parent().find(".doc_type").val(),
                title: title.val(),
                icon: $(this).parent().find(".doc_icon").val(),
                text: $(this).parent().find(".doc_text").val(),
                url: url.val(),
                external: external
            }
            // Add the document object to the documents array
            documents.push(doc);
        } else if ($(this).parent().find(".doc_type").val() == "section") {
            let thesection = $(this).parent().find(".doc_title")
            if (thesection.val() == "") {
                form_errors.push("le titre de la section ne peut pas être vide");
                thesection.addClass("is-invalid");
            }
            // Create a section object
            let section = {
                doc_order: doc_order,
                type: $(this).parent().find(".doc_type").val(),
                title: thesection.val()
            }
            // Add the section object to the documents array
            documents.push(section);
        } else if ($(this).parent().find(".doc_type").val() == "note") {
            let thenote = $(this).parent().find(".doc_text")
            if (thenote.summernote("code") == "" || thenote.summernote("code") == "<p><br></p>") {
                form_errors.push("le texte de la note ne peut pas être vide");
                thenote.addClass("is-invalid");
            }
            // Create a note object
            let note = {
                doc_order: doc_order,
                type: $(this).parent().find(".doc_type").val(),
                text: thenote.summernote('code')
            }
            // Add the note object to the documents array
            documents.push(note);
        } else {
            // Get the id of the .accordion-collapse in the parent of the accordion-header
            let id = $(this).parent().find(".accordion-collapse").attr("id");
            // Find the doc order
            let doc_order = $(this).parent().find(".doc_order").val();
            // Add the module
            let type = $(this).parent().find(".doc_type").val();
            console.log("Adding module " + type + " to classroom")
            //Find the module type in modules array
            let module = modules.find(x => x.id == type);
            console.log("Module found : " + module.name);
            // Get the function name from addToClassroom key
            var functionName = module.defs.addToClassroom;
            console.log("Function name : " + functionName);
            // Call the function
            if (!functionName == "") {
                data = window[functionName]("addDocument", id);
                //  Add the doc order
                data.activity[0].doc_order = doc_order;
                documents = documents.concat(data.activity);
                form_errors = form_errors.concat(data.errors);
            }
        }
    });

    // Order the documents by doc_order
    documents.sort(function (a, b) {
        return a.doc_order - b.doc_order;
    });

    if (form_errors.length == 0) {
        // Send the classroom to the server
        socket.emit('add_classroom', { title: titre, level: niveau, date: date, time: time, instructions: instructions, password: password, active: active, visible: visible, documents: documents });
    } else {
        $("#message_add_classroom").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Erreur lors de l'ajout de la salle de classe :</strong> ${form_errors.join(", ")}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`);

    }


}

function clearAddClassroom() {
    $("#title").val("");
    $("#level").val("");
    $("#title").removeClass("is-invalid");
    $("#level").removeClass("is-invalid");
    $("#date").val("");
    $("#time").val("");
    $("#password").val("");
    $("#active").prop("checked", true);
    $("#visible").prop("checked", true);
    $("#addDocument").html("");
    doc_count = 0;
    $("#message_add_classroom").html("");
    addClassroomOrder = 0;
}


// on each button click in the addClassroomActivities div
$(document).on("click", "#addClassroomActivities button", function () {
    // For each element h2 in the addDocument div
    $("#addDocument h2").last().each(function () {
        addArrows("addDocument");
    });
})
