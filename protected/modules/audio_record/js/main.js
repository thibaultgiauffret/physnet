// This file contains all the functions related to the audio and its behaviour in the add/modify classroom modals in the admin panel. It is the connector between the audio activity and the classroom in the admin panel

$(document).ready(function () {

    // Get the audio list
    socket.emit('load_audio_list')

    socket.on("load_audio_list", (data) => {
        if (data == "error") {
            $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors du chargement des audios.</div>");
            return;
        }

        // For each audio list
        $(".audio_list").each(function () {

            // Get the selected value
            let selected = $(this).val();
            // Clean the select
            $(this).html("");
            // check if file_deposit_list has class old
            if (!$(this).hasClass("old")) {
                $(this).html("<option disabled selected value> Choisir une oral </option>");
                $("#modify_audio").hide();
                for (var j = 0; j < data.length; j++) {
                    var option = document.createElement("option");
                    option.value = data[j].id;
                    option.text = "Oral " + data[j].id + " : " + data[j].title;
                    $(this).append(option);
                }
                // Set back the selected value
                $(this).val(selected);
            } else {
                for (let i = 0; i < data.length; i++) {
                    // Add and set the selected quiz
                    if ($(this).attr("data-select") == data[i].id) {
                        $(this).append(`<option value="${data[i].id}" selected>Oral ` + data[i].id + ` : ` + data[i].title + `</option>`);
                        continue;
                    } else {
                        $(this).append(`<option value="${data[i].id}">Oral ` + data[i].id + ` : ` + data[i].title + `</option>`);
                    }
                }
            }
        })
    })


})

function addAudioActivityButton(location) {
    let counter;
    if (location == "modifyDocument") {
        counter = doc_modify_count + 1;
        doc_modify_count++;
    } else {
        counter = doc_count + 1;
        doc_count++;
    }

    // add a title input to the addDocument div
    var audio = document.createElement("div");
    audio.classList.add("accordion-item");
    audio.innerHTML = `
        <h2 class="accordion-header" id="panel_${counter}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${counter}" aria-expanded="true" aria-controls="panelsStayOpen">
        Oral ${counter}

        </button>
        </h2>

        <div id="panel_collapse_${counter}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

      <input type="hidden" class="form-control doc_type" value="audio_record">

        <div class="input-group mb-3">

        <div class="input-group-prepend">
            <label class="input-group-text" for="classroom_list_modify"><i
                    class="fa-solid fa-microphone fa-fw"></i></label>
        </div>
        <select class="custom-select audio_list"></select>
        
         </div>
         </div>
         </div>
         </div>
            
        `;
    $("#" + location).append(audio);

    // Load the audio list to the select
    socket.emit("load_audio_list");

}

function addAudioToClassroom(loc, id) {
    console.log("Adding audio to classroom from " + loc);
    let activity = [];
    let errors = [];

    let select = $('#' + loc + ' #' + id + ' .audio_list')

    if (select.val() == null) {
        errors.push("un oral doit être sélectionné");
        select.addClass("is-invalid");
    }

    // Determine if the audio is an update or a new audio
    let update;
    if (select.hasClass("old")) {
        update = true;
    } else {
        update = false;
    }

    // Create a document object
    let doc = {
        update: update,
        type: select.parent().parent().find(".doc_type").val(),
        doc_id: parseInt(select.parent().parent().find(".audio_id").val()),
        id: select.val(),
        text: select.parent().parent().find(".audio_list option:selected").text().split(": ")[1],
    }
    activity.push(doc);

    // Return the audio_list and errors
    return {
        activity: activity,
        errors: errors,
    }
}

function loadAudioActivity(location = "modifyDocument", data) {

    doc_modify_count++;

    // Create the audio
    audio_id = data.url.split("id=")[1];
    var audio = document.createElement("div");
    audio.classList.add("accordion-item");
    audio.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Oral ${doc_modify_count}

    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
  <div class="accordion-body">

  <input type="hidden" class="audio_id" value="${data.id}">

  <input type="hidden" class="form-control doc_type" value="audio_record">

    <div class="input-group mb-3">

    <div class="input-group-prepend">
        <label class="input-group-text" for="classroom_list_modify"><i
                class="fa-solid fa-circle-question fa-fw"></i></label>
    </div>
    <select class="custom-select audio_list old" data-select="${audio_id}">
    </select>
     </div>
     </div>
     </div>
     </div>
        
    `;
    $("#" + location).append(audio);

    socket.emit("load_audio_list");
}
