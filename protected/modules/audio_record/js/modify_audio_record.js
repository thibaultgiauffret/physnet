let audioModifyModal

$(document).ready(function () {

    // Instanciate the classroomModifyModal
    audioModifyModal = new bootstrap.Modal(document.getElementById("audioModifyModal"), {});

})

function selectAudio(id) {
    if (id == "") {
        return;
    }
    console.log(id);
    $("#modify_audio").show();

    $("#modifyAudioSubmit").prop("disabled", false);
    $("#deleteAudio").prop("disabled", false);

    socket.emit('select_audio', id, data => {
        $("#audio_mod_id").val(data.id);
        $("#audio_mod_title").val(data.title);
        $("#audio_mod_instructions").summernote('code', data.instructions);
        time = data.time;
        // Convert from seconds to mm:ss
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        $("#audio_mod_time").val(minutes + ":" + seconds);
        $("#audio_mod_pass").val(data.pass);
    })
}


function modifyAudio() {
    var id = $("#audio_mod_id").val();
    var title = $("#audio_mod_title").val();
    var instructions = $("#audio_mod_instructions").summernote('code');
    var time = $("#audio_mod_time").val();
    var pass = $("#audio_mod_pass").val();

    var errors = []
    if (title == "") {
        $("#audio_title").addClass("is-invalid");
        errors.push("le titre ne peut pas être vide");
    } else if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>") {
        $("#audio_instructions").addClass("is-invalid");
        errors.push("les consignes ne peuvent pas être vides");
    } else if (pass == "") {
        $("#audio_pass").addClass("is-invalid");
        errors.push("le mot de passe ne peut pas être vide");
    }

    if (errors.length > 0) {
        $("#message_mod_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification de l'oral : " + errors.join(", ") + ".</div>");
        return;
    }
    socket.emit('modify_audio', { id: id, title: title, instructions: instructions, time: time, pass: pass }, callback => {
        if (callback == "success") {
            $("#message").html("<div class='alert alert-success' role='alert'>Oral modifié avec succès.</div>");
            clearModifyAudio();
            // Load the audio list to the select
            socket.emit("load_audio_list");
        } else {
            $("#message_add_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification de l'oral.</div>");
        }
    })
}

function clearModifyAudio() {
    $("#audio_mod_title").val("");
    $("#audio_mod_instructions").summernote('code', '');
    $("#audio_mod_time").val("");
    $("#audio_mod_pass").val("");
    $("#message_mod_audio").html("");
    $("#audio_mod_title").removeClass("is-invalid");
    $("#audio_mod_instructions").removeClass("is-invalid");
    $("audio_mod_pass").removeClass("is-invalid");
    $("#modify_audio").hide();
    $("#audio_list_modify").val("").change();
    $("#modifyAudioSubmit").prop("disabled", true);
    $("#deleteAudio").prop("disabled", true);
    audioModifyModal.hide();
}

function deleteAudio() {
    if (confirm("Êtes-vous sûr de vouloir supprimer cet oral ?")) {
        var id = $("#audio_mod_id").val();
        socket.emit('delete_audio', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Oral supprimé avec succès.</div>");
                clearModifyAudio();
                // Load the audio list to the select
                socket.emit("load_audio_list");
            } else {
                $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression de l'oral.</div>");
            }
        })
    }
}