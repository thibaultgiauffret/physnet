let audioFilesModal;

$(document).ready(function () {

    // Instanciate the audioFilesModal
    audioFilesModal = new bootstrap.Modal(document.getElementById("audioFilesModal"), {});

    socket.on("audio_files_admin", (data) => {
        for (var i = 0; i < data.length; i++) {
            files_table.row.add([
                data[i].name,
                data[i].date,
                data[i].path,
                data[i].size,
                "<a class='btn btn-success mr-1' href='/assignments/audio_record/" + data[i].path + "' download><i class='fas fa-download'></i></a><button class='btn btn-danger' onclick='deleteFile(\"" + data[i].path + "\");'><i class='fas fa-trash'></i></button>"
            ]).draw(false);
        }
    })
});

function loadAudioFiles(id) {
    files_table.rows().remove().draw();
    $("#deleteAudioFiles").prop("disabled", false);
    $("#downloadAudioFiles").prop("disabled", false);
    if (id == "") {
        return;
    }
    socket.emit("get_audio_files", id);
}

function deleteFile(path) {
    console.log(path);
    if (confirm("Êtes-vous sûr de vouloir supprimer ce fichier ?")) {
        socket.emit("delete_audio_file", path, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichier supprimé avec succès.</div>");
                clearAudioFiles();
            } else {
                $("#message_files_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du fichier : " + callback + ".</div>");
            }
        })
    }
}

function clearAudioFiles() {
    files_table.rows().remove().draw();
    $("#deleteAudioFiles").prop("disabled", true);
    $("#downloadAudioFiles").prop("disabled", true);
    $("#audio_list_files").val("").change();
    $("#message_files_audio").html("");
    socket.emit("load_audio_list");
    audioFilesModal.hide();
}

function deleteAudioFiles() {
    if (confirm("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?")) {
        // Get id of selected rows
        var rows = files_table.rows('.selected').data();
        var id = [];
        for (i = 0; i < rows.length; i++) {
            // Get html path in the 3rd column
            id.push(rows[i][2]);
        }
        socket.emit('delete_audio_files', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichiers supprimés avec succès.</div>");
                clearAudioFiles();
            } else {
                $("#message_files_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression des fichiers : " + callback + ".</div>");
            }
        })
    }
}

function downloadAudioFiles() {
    // Get id of selected rows
    var rows = files_table.rows('.selected').data();
    for (i = 0; i < rows.length; i++) {
        // Create a link and click on it
        var link = document.createElement('a');
        link.href = '/assignments/audio_record/'+rows[i][2];
        link.download = rows[i][2];
        link.click();
    }    
}