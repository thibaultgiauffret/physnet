let audioModal;

$(document).ready(function () {

    // Instanciate the quizModal
    audioModal = new bootstrap.Modal(document.getElementById("audioModal"), {});



})

function createAudio(){
    var title = $("#audio_title").val();
    var instructions = $("#audio_instructions").summernote('code');
    var time = $("#audio_time").val();
    var pass = $("#audio_pass").val();

    var errors = []
    if (title == "" ){
        $("#audio_title").addClass("is-invalid");
        errors.push("le titre ne peut pas être vide");
    } else if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>"){
        $("#audio_instructions").addClass("is-invalid");
        errors.push("les consignes ne peuvent pas être vides");
    }else if (pass == ""){
        $("#audio_pass").addClass("is-invalid");
        errors.push("le mot de passe ne peut pas être vide");
    }

    if (errors.length > 0){
        $("#message_add_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création de l'oral : " + errors.join(", ") + ".</div>");
        return;
    }
    socket.emit('create_audio', {title: title, instructions: instructions, time: time, pass: pass}, callback => {
        if (callback == "success"){
            $("#message").html("<div class='alert alert-success' role='alert'>Oral créé avec succès.</div>");
            clearCreateAudio();
            // Load the audio list to the select
            socket.emit("load_audio_list");
        } else {
            $("#message_add_audio").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création de l'oral.</div>");
        }
    })
}

function clearCreateAudio(){
    $("#audio_title").val("");
    $("#audio_instructions").summernote('code', '');
    $("#audio_time").val("");
    $("#audio_pass").val("");
    $("#message_add_audio").html("");
    $("#audio_title").removeClass("is-invalid");
    $("#audio_instructions").removeClass("is-invalid");
    $("audio_pass").removeClass("is-invalid");
    audioModal.hide();
}