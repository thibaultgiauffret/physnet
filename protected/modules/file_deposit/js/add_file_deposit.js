let fileDepositModal;

$(document).ready(function () {

    // Instanciate the quizModal
    fileDepositModal = new bootstrap.Modal(document.getElementById("fileDepositModal"), {});

})

function createFileDeposit(){
    var title = $("#file_deposit_title").val();
    var instructions = $("#file_deposit_instructions").summernote('code');
    var max_size = $("#file_deposit_size").val();
    var extensions = $("#file_deposit_extensions").val();
    var pass = $("#file_deposit_pass").val();

    var errors = []
    if (title == "" ){
        $("#file_deposit_title").addClass("is-invalid");
        errors.push("le titre ne peut pas être vide");
    } else if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>"){
        $("#file_deposit_instructions").addClass("is-invalid");
        errors.push("les consignes ne peuvent pas être vides");
    }else if (pass == ""){
        $("#file_deposit_pass").addClass("is-invalid");
        errors.push("le mot de passe ne peut pas être vide");
    }

    if (errors.length > 0){
        $("#message_add_file_deposit").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création de la boîte de dépôt : " + errors.join(", ") + ".</div>");
        return;
    }
    socket.emit('create_file_deposit', {title: title, instructions: instructions, max_size:max_size, extensions:extensions, pass: pass}, callback => {
        if (callback == "success"){
            $("#message").html("<div class='alert alert-success' role='alert'>Boîte de dépôt créée avec succès.</div>");
            clearCreateFileDeposit();
            // Load the file_deposit list to the select
            socket.emit("load_file_deposit_list");
        } else {
            $("#message_add_file_deposit").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la création de la boîte de dépôt.</div>");
        }
    })
}

function clearCreateFileDeposit(){
    $("#file_deposit_title").val("");
    $("#file_deposit_instructions").summernote('code', '');
    $("#file_deposit_size").val("");
    $("#file_deposit_extensions").val("");
    $("#file_deposit_pass").val("");
    $("#message_add_file_deposit").html("");
    $("#file_deposit_title").removeClass("is-invalid");
    $("#file_deposit_instructions").removeClass("is-invalid");
    $("file_deposit_pass").removeClass("is-invalid");
    fileDepositModal.hide();
}