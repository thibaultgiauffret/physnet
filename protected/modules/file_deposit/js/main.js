// This file contains all the functions related to the file_deposit and its behaviour in the add/modify classroom modals in the admin panel. It is the connector between the file_deposit activity and the classroom in the admin panel

$(document).ready(function () {

    // Get the file_deposit list
    socket.emit('load_file_deposit_list')

    socket.on("load_file_deposit_list", (data) => {
        if (data == "error") {
            $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors du chargement des boîtes de dépôt.</div>");
            return;
        }
        // For each file_deposit list
        $(".file_deposit_list").each(function () {

            // Get the selected value
            let selected = $(this).val();
            // Clean the select
            $(this).html("");
            // check if file_deposit_list has class old
            if (!$(this).hasClass("old")) {
                $(this).html("<option disabled selected value> Choisir une boîte de dépôt </option>");
                $("#modify_file_deposit").hide();
                for (var j = 0; j < data.length; j++) {
                    var option = document.createElement("option");
                    option.value = data[j].id;
                    option.text = "Boîte de dépôt " + data[j].id + " : " + data[j].title;
                    $(this).append(option);
                }
                // Set back the selected value
                $(this).val(selected);
            } else {
                for (let i = 0; i < data.length; i++) {
                    // Add and set the selected quiz
                    if ($(this).attr("data-select") == data[i].id) {
                        $(this).append(`<option value="${data[i].id}" selected>Boîte de dépôt ` + data[i].id + ` : ` + data[i].title + `</option>`);
                        continue;
                    } else {
                        $(this).append(`<option value="${data[i].id}">Boîte de dépôt ` + data[i].id + ` : ` + data[i].title + `</option>`);
                    }
                }
            }
        })
    })


})

function addFileDepositActivityButton(location) {
    let counter;
    if (location == "modifyDocument") {
        counter = doc_modify_count + 1;
        doc_modify_count++;
    } else {
        counter = doc_count + 1;
        doc_count++;
    }

    // add a title input to the addDocument div
    var file = document.createElement("div");
    file.classList.add("accordion-item");
    file.innerHTML = `
        <h2 class="accordion-header" id="panel_${counter}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${counter}" aria-expanded="true" aria-controls="panelsStayOpen">
        Boîte de dépôt ${counter}

        </button>
        </h2>

        <div id="panel_collapse_${counter}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

      <input type="hidden" class="form-control doc_type" value="file_deposit">

        <div class="input-group mb-3">

        <div class="input-group-prepend">
            <label class="input-group-text" for="classroom_list_modify"><i
                    class="fa-solid fa-box-open fa-fw"></i></label>
        </div>
        <select class="custom-select file_deposit_list"></select>
        
         </div>
         </div>
         </div>
         </div>
            
        `;
    $("#" + location).append(file);

    // Load the file_deposit list to the select
    socket.emit("load_file_deposit_list");

}

function addFileDepositToClassroom(loc, id) {
    console.log("Adding file_deposit to classroom from " + loc);
    let activity = [];
    let errors = [];

    let select = $('#' + loc + ' #' + id + ' .file_deposit_list')

    if (select.val() == null) {
        errors.push("une boîte de dépôt doit être sélectionné");
        select.addClass("is-invalid");
    }

    // Determine if the file_deposit is an update or a new file_deposit
    let update;
    if (select.hasClass("old")) {
        update = true;
    } else {
        update = false;
    }

    // Create a document object
    let doc = {
        update: update,
        type: select.parent().parent().find(".doc_type").val(),
        doc_id: parseInt(select.parent().parent().find(".file_deposit_id").val()),
        id: select.val(),
        text: select.parent().parent().find(".file_deposit_list option:selected").text().split(": ")[1],
    }
    activity.push(doc);

    // Return the file_deposit_list and errors
    return {
        activity: activity,
        errors: errors,
    }
}

function loadFileDepositActivity(location = "modifyDocument", data) {

    doc_modify_count++;

    // Create the file_deposit
    file_deposit_id = data.url.split("id=")[1];
    console.log("Creating file_deposit " + file_deposit_id + " in " + location)
    var file_deposit = document.createElement("div");
    file_deposit.classList.add("accordion-item");
    file_deposit.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Boîte de dépôt ${doc_modify_count}

    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
  <div class="accordion-body">

  <input type="hidden" class="file_deposit_id" value="${data.id}">

  <input type="hidden" class="form-control doc_type" value="file_deposit">

    <div class="input-group mb-3">

    <div class="input-group-prepend">
        <label class="input-group-text" for="classroom_list_modify"><i
                class="fa-solid fa-box-open fa-fw"></i></label>
    </div>
    <select class="custom-select file_deposit_list old" data-select="${file_deposit_id}">
    </select>
     </div>
     </div>
     </div>
     </div>
        
    `;
    $("#" + location).append(file_deposit);

    socket.emit("load_file_deposit_list");
}
