let fileDepositFilesModal;

$(document).ready(function () {

    // Instanciate the fileDepositFilesModal
    fileDepositFilesModal = new bootstrap.Modal(document.getElementById("fileDepositFilesModal"), {});

    socket.on("file_deposit_files_admin", (data) => {
        for (var i = 0; i < data.length; i++) {
            file_deposit_files_table.row.add([
                data[i].name,
                data[i].date,
                data[i].path,
                data[i].size,
                "<a class='btn btn-success mr-1' href='/assignments/file_deposit/" + data[i].path + "' download><i class='fas fa-download'></i></a><button class='btn btn-danger' onclick='deleteFile(\"" + data[i].path + "\");'><i class='fas fa-trash'></i></button>"
            ]).draw(false);
        }
    })
});

function loadFileDepositFiles(id) {
    file_deposit_files_table.rows().remove().draw();
    $("#deleteFileDepositFiles").prop("disabled", false);
    $("#downloadFileDepositFiles").prop("disabled", false);
    if (id == "") {
        return;
    }
    socket.emit("get_file_deposit_files", id);
}

function deleteFile(path) {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce fichier ?")) {
        socket.emit("delete_file_deposit_file", path, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichier supprimé avec succès.</div>");
                clearFileDepositFiles();
            } else {
                $("#message_file_deposit_files").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du fichier : " + callback + ".</div>");
            }
        })
    }
}

function clearFileDepositFiles() {
    file_deposit_files_table.rows().remove().draw();
    $("#deleteFileDepositFiles").prop("disabled", true);
    $("#downloadFileDepositFiles").prop("disabled", true);
    $("#list_file_deposit_files").val("").change();
    $("#message_files_file_deposit").html("");
    socket.emit("load_file_deposit_list");
    fileDepositFilesModal.hide();
}

function deleteFileDepositFiles() {
    if (confirm("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?")) {
        // Get id of selected rows
        var rows = file_deposit_files_table.rows('.selected').data();
        var id = [];
        for (i = 0; i < rows.length; i++) {
            // Get html path in the 3rd column
            id.push(rows[i][2]);
        }
        socket.emit('delete_file_deposit_files', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichiers supprimés avec succès.</div>");
                clearFileDepositFiles();
            } else {
                $("#message_files_file_deposit").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression des fichiers : " + callback + ".</div>");
            }
        })
    }
}

function downloadFileDepositFiles() {
    // Get id of selected rows
    var rows = file_deposit_files_table.rows('.selected').data();
    for (i = 0; i < rows.length; i++) {
        // Create a link and click on it
        var link = document.createElement('a');
        link.href = '/assignments/file_deposit/'+rows[i][2];
        link.download = rows[i][2];
        link.click();
    }    
}