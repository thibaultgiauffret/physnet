// This file contains all the functions related to the results of the quizzes in the admin panel

$(document).ready(function () {

    socket.on('results_admin', function (data) {

        console.log("results_admin");
        console.log(data);

        username = data.username;
        quiz = data.quiz;
        id = data.id;

        allQuestions = data.allQuestions
        allAnswers = data.allAnswers
        userResults = data.userResults

        let questionTitles = [];
        let questionPoints = [];
        let userPoints = [];
        let correctAnswersTexts = [];
        let correctAnswersLetters = [];
        let totalPoints = 0;

        for (i = 0; i < userResults.questions.length; i++) {

            // Store question titles
            for (j = 0; j < allQuestions.length; j++) {
                if (userResults.questions[i] == allQuestions[j].question_id) {
                    questionTitles.push(allQuestions[j].question_text);
                }
            }

            // Get correct answers and points
            let sum = 0;
            let correctAnswersTextsTemp = [];
            let correctAnswersLettersTemp = [];
            let sumPoints = 0;
            for (j = 0; j < allAnswers.length; j++) {
                if (userResults.questions[i] == allAnswers[j].question_id) {
                    sum += allAnswers[j].answer_points;
                    if (allAnswers[j].answer_points > 0) {
                        correctAnswersTextsTemp.push(allAnswers[j].answer_text);
                        correctAnswersLettersTemp.push(allAnswers[j].answer_letter);
                    }
                    // Get user points
                    for (k = 0; k < userResults.answers[i].length; k++) {
                        if (userResults.answers[i][k] == allAnswers[j].answer_id) {
                            sumPoints += allAnswers[j].answer_points;
                        }
                    }
                }
            }

            totalPoints += sum;
            questionPoints.push(sum);
            userPoints.push(sumPoints);
            correctAnswersTexts.push(correctAnswersTextsTemp);
            correctAnswersLetters.push(correctAnswersLettersTemp);
        }

        let note = 0;
        let note_max = 0;

        // Display results
        for (k = 0; k < userResults.questions.length; k++) {

            question_title = questionTitles[k];
            points = questionPoints[k];
            question_id = userResults.questions[k];
            correct_answer_id = correctAnswersLetters[k].toString().split(',').join(', ');
            correct_answer_text = correctAnswersTexts[k].toString().split(',').join(', ');
            answer_id = userResults.answers[k];
            answer_text = [];
            answer_letter = [];

            for (i = 0; i < allAnswers.length; i++) {
                for (j = 0; j < answer_id.length; j++) {
                    if (answer_id[j] == allAnswers[i].answer_id) {
                        answer_text.push(allAnswers[i].answer_text);
                        answer_letter.push(allAnswers[i].answer_letter);
                    }
                }
            }
            answer_text = answer_text.toString().split(',').join(', ');
            answer_letter = answer_letter.toString().split(',').join(', ');

            points_attrib = userPoints[k];

            note += points_attrib;
            note_max = parseFloat(totalPoints);
        }


        var date = new Date(userResults.date);
        var new_date = date.toLocaleString('fr-FR');

        results_table.row.add([id, username, new_date, note + '/' + note_max, quiz]).draw();
    });

});

function resultsQuiz(quiz) {
    // Clear $("#results") table tbody
    results_table.rows().remove().draw();
    // Enable delete button
    $('#deleteResults').prop('disabled', false);
    if (quiz != "") {
        socket.emit('get_results_list', quiz);
    }
}

function deleteResults() {
    if (confirm("Êtes-vous sûr de vouloir supprimer les résultats sélectionnés ?")) {
        // Get id of selected rows
        var table = $('#results').DataTable();
        var rows = table.rows('.selected').data();
        var id = [];
        for (i = 0; i < rows.length; i++) {
            id.push(rows[i][0]);
        }
        console.log(id);
        socket.emit('delete_results', id);
    }

}

socket.on('delete_results', function (data) {
    if (data == "success") {
        // Clear the form
        clearResultsQuiz();
        $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Résultats supprimés avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
    } else {
        $("#message_results_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la suppression des résultats !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
    }
});

function clearResultsQuiz() {
    // Clear $("#results") table tbody
    results_table.rows().remove().draw();
    // Disable delete button
    $('#deleteResults').prop('disabled', true);
    $("#quiz_list_results").val("").change();
}