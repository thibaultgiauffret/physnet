// This file contains all the functions related to the quiz creation in the admin panel (questions, answers, ...)

let question_count = 0;
let last_quiz_id = 0;
let last_question_id = 0;
let quiz_list = [];

$(document).ready(function () {

    // Instanciate the quizModal
    var quizModal = new bootstrap.Modal(document.getElementById("quizModal"), {});

    // Get last quiz and question ids
    socket.emit('get_last_quiz_ids')

    socket.on('last_ids', (data) => {
        console.log("Last IDs are : " + JSON.stringify(data));
        last_quiz_id = parseInt(data.quiz);
        last_question_id = data.question;
    });

    // Message when quiz is added
    socket.on('add_quiz', (data) => {
        if (data == "success") {
            // Reload the quiz list
            socket.emit('load_quiz_list');
            // Clear the form
            clearCreateQuiz();

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Quiz ajouté avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
            quizModal.hide();
            socket.emit('get_last_quiz_ids')
        } else {
            $("#message_add_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de l'ajout du quiz : ${data}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        }
    });
});

function createQuiz() {
    let form_errors = []
    // Remove all invalid classes
    $("#quiz_title").removeClass("is-invalid");
    $("#addQuestion input").removeClass("is-invalid");
    $("#addQuestion select").removeClass("is-invalid");

    let quiz_title = $("#quiz_title").val();
    if (quiz_title == "") {
        form_errors.push("le titre du quiz est vide");
        $("#quiz_title").addClass("is-invalid");
    }

    let display_results_check = $("#display_results").prop("checked");
    if (display_results_check) {
        display_results = 1;
    } else {
        display_results = 0;
    }

    let order_check = $("#order").prop("checked");
    if (order_check) {
        order = 1;
    } else {
        order = 0;
    }

    // For each question
    let questions = [];
    $("#addQuestion .question_text").each(function () {
        let question_text = $(this).summernote('code');
        if (question_text == "" || question_text == "<p><br></p>" || question_text == "<br>") {
            form_errors.push("le titre d'une question est vide");
            $(this).parent().find(".note-editor").addClass("is-invalid");
        }
        let question_type_el = $(this).parent().parent().find(".question_type");
        let question_type = question_type_el.val();
        if (question_type == null) {
            form_errors.push("le type d'une question est vide");
            question_type_el.addClass("is-invalid");
        }
        let question_time_el = $(this).parent().parent().find(".question_time");
        let question_time_switch_el = $(this).parent().parent().find(".question_time_switch");
        let question_time = question_time_el.val();
        if (question_time_switch_el.is(":checked")) {
            if (question_time == "") {
                form_errors.push("la durée d'une question est vide");
                question_time_el.addClass("is-invalid");
            } else {
                // Convert question_time to minutes
                question_time = parseInt(question_time.split(":")[0]) + parseInt(question_time.split(":")[1]) / 60;
            }
        } else {
            question_time = 0;
        }
        let question_id = parseInt($(this).parent().parent().find(".question_id").val().split("_")[1]);
        let question_answers = [];
        let counter = 0;
        let points_total = 0;
        $(this).parent().parent().parent().find(".question_answers").each(function () {
            // For each answer
            let answer = {};
            $(this).find(".answer").each(function () {
                let text = $(this).find(".answer_text");
                let points = $(this).find(".answer_points");
                let letter = $(this).find(".answer_letter");
                if (text.val() == "" || points.val() == "" || letter.val() == "") {
                    form_errors.push("une réponse possède un ou plusieurs champs vides");
                    if (text.val() == "") {
                        text.addClass("is-invalid");
                    }
                    if (points.val() == "") {
                        points.addClass("is-invalid");
                    }
                    if (letter.val() == "") {
                        letter.addClass("is-invalid");
                    }
                } else {
                    points_total += parseInt(points.val());
                    if (points.val() > 0) {
                        counter++;
                    }
                    answer = {
                        question_id: question_id,
                        letter: letter.val(),
                        text: text.val(),
                        points: parseInt(points.val()),
                    }
                }

                question_answers.push(answer);

            });

            if (counter > 1 && parseInt(question_type) == 0) {
                form_errors.push("une question à choix unique possède plusieurs réponses correctes");
            } else {
                if (counter == 0) {
                    form_errors.push("une question ne possède aucune réponse correcte");
                }
            }


        });
        if (question_answers.length == 0) {
            form_errors.push("une question ne possède aucune réponse");
        }
        let question = {
            quiz_id: last_quiz_id + 1,
            text: question_text,
            time: question_time,
            type: question_type,
            answers: question_answers,
        }
        questions.push(question);
    });
    let quiz = {
        title: quiz_title,
        display_results: display_results,
        question_order: order,
        questions: questions,
    }
    if (form_errors.length > 0) {
        $("#message_add_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Erreur lors de l'ajout du quiz :</strong> ${form_errors.join(", ")}.<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        return;
    } else {
        // Send the new quiz to the server
        socket.emit('create_quiz', quiz);
    }
}

function clearCreateQuiz() {
    $("#quiz_title").val("");
    $('#quiz_title').removeClass("is-invalid");
    $("#addQuestion").html("");
    question_count = 0;
    $("#message_add_quiz").html("");
}

function addQuestion(location = 'addQuestion') {
    if (location == 'addQuestion') {
        counter = parseInt(question_count) + 1;
        question_count++;
    } else {
        counter = parseInt(last_question_id) + 1;
        last_question_id++;
    }
    // Create the question
    var question = document.createElement("div");
    question.classList.add("accordion-item");
    question.innerHTML = `

    <h2 class="accordion-header" id="panel_${counter}">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${counter}" aria-expanded="true" aria-controls="panelsStayOpen">
      Nouvelle question

      </button>
    </h2>

    <div id="panel_collapse_${counter}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

    <input type="hidden" class="question_id" value="new_${counter}">

    
    <button type="button" class="btn btn-sm btn-danger mb-2" onclick="if(confirm('Supprimer la question ?')){this.parentElement.parentElement.parentElement.remove();}"><i class="fa-solid fa-trash"></i>&nbsp;Supprimer la question</button>

        <div class="question_text"></div>

    <div class="input-group mt-3 mb-3">
        <span class="input-group-text"><i class="fa-solid fa-list fa-fw"></i></span>
        <select class="custom-select question_type">
            <option value="" disabled selected>Type</option>
            <option value="0">Question simple</option>
            <option value="1">Question à choix multiples</option>
        </select>
    </div>

    <div class="form-check form-switch mb-3 ml-4">
        <input class="form-check-input question_time_switch" type="checkbox"onchange="toggleTime(this)" checked>
        <label class="form-check-label"">Temps limité</label>
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text"><i class="fa-solid fa-clock fa-fw"></i></span>
        <input type="time" class="form-control question_time">
        <span class="input-group-text">(min:sec)</span>
    </div>

    Réponses : <br>

    <div class="question_answers" id="question_answers_${counter}">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Lettre</th>
                <th scope="col">Texte</th>
                <th scope="col">Points</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>        
    </div>
    <button type="button" class="btn btn-secondary" onclick="addAnswer(${counter})">Ajouter une réponse</button>
  </div>
    `;
    document.getElementById(location).appendChild(question);

    if (location == 'addQuestion') {
        $('#addQuestion .question_text').last().summernote();
    } else {
        $('#modifyQuestions .question_text').last().summernote();
    }

    // Fix summernote
    fixSummernote();
}

function toggleTime(element) {
    // Toggle the .question_time input in the parent element
    if (element.checked) {
        element.parentElement.parentElement.getElementsByClassName("question_time")[0].disabled = false;
    } else {
        element.parentElement.parentElement.getElementsByClassName("question_time")[0].disabled = true;
    }
}

function addAnswer(question_id) {
    // Add an answer to the question in a table
    var answer = document.createElement("tr");
    answer.classList.add("answer");
    answer.innerHTML = `
        <td><input type="hidden" class="form-control answer_id new" disabled></td>
       <td><input type="text" class="form-control answer_letter" placeholder="Lettre"></td>
        <td><input type="text" class="form-control answer_text" placeholder="Texte"></td>
        <td><input type="text" class="form-control answer_points" placeholder="Points"></td>
        <td><button type="button" class="btn btn-sm btn-danger" onclick="this.parentElement.parentElement.remove()"><i class="fa-solid fa-trash"></i></button></td>
    `;

    // Add a letter to the answer_letter
    answer.getElementsByClassName("answer_letter")[0].value = String.fromCharCode(65 + document.getElementById("question_answers_" + question_id).getElementsByTagName("tbody")[0].getElementsByTagName("tr").length);

    document.getElementById("question_answers_" + question_id).getElementsByTagName("tbody")[0].appendChild(answer);
}