// This file contains the functions for the quiz modification in the admin panel

let question_modify_count = 0;
let removed_questions = [];
let removed_answers = [];

$(document).ready(function () {

    // Instanciate the classroomModifyModal
    var quizModifyModal = new bootstrap.Modal(document.getElementById("quizModifyModal"), {});


    socket.on("select_quiz", function (data) {
        // Init the form
        $("#modifyQuestions").html("");
        $("#mod_quiz_id").val(data.id);
        $("#mod_quiz_title").val(data.title);
        if (data.display_results == 1) {
            $("#mod_display_results").prop("checked", true);
        } else {
            $("#mod_display_results").prop("checked", false);
        }
        if (data.question_order == 1) {
            $("#mod_order").prop("checked", true);
        } else {
            $("#mod_order").prop("checked", false);
        }
        // For each question
        for (var i = 0; i < data.questions.length; i++) {
            // Add the question to the form
            loadQuestions(data.questions[i]);
            // For each answer in question 
            for (var j = 0; j < data.questions[i].answers.length; j++) {
                // Add the answer to the form
                loadAnswers(data.questions[i].answers[j], "question_answers_" + data.questions[i].id);
            }
        }
    });

    // Message when quiz is modified
    socket.on("modify_quiz", (data) => {
        if (data == "success") {
            // Reload the quiz list
            socket.emit('load_quiz_list');
            // Clear the form
            clearModifyQuiz();

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Quiz modifié avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
            quizModifyModal.hide();
        } else {
            $("#message_modify_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la modification du quiz de classe : ${data}<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        }
    });

    socket.on('delete_quiz', function (data) {
        if (data == "success") {
            // Reload the quiz list
            socket.emit('load_quiz_list');
            // Clear the form
            $("#modifyQuizSubmit").prop("disabled", true);
            $("#deleteQuiz").prop("disabled", true);
            removed_questions = [];
            removed_answers = [];

            $("#message").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">Quiz supprimé avec succès !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)

            quizModifyModal.hide();
        } else {
            $("#message_modify_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert">Erreur lors de la suppression du quiz !<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        }
    });

});

function selectQuiz(id) {
    // If option zero is selected, do nothing
    if (id == "") {
        return;
    }
    $("#modify_quiz").show();

    // Enable the buttons
    $("#modifyQuizSubmit").prop("disabled", false);
    $("#deleteQuiz").prop("disabled", false);

    // Send the request to the server
    socket.emit('select_quiz', id);
}

function loadQuestions(data, location = "modifyQuestions") {
    question_modify_count++;

    let select_simple = "";
    let select_multiple = "";
    if (data.type == 0 || data.type == null || data.type == "") {
        select_simple = "selected";
    } else if (data.type == 1) {
        select_multiple = "selected";
    }

    // Convert time to minutes and seconds. Careful if minutes < 10, format is 0X:XX !
    let question_time = 0;
    let switch_time = "";
    let input_time = "";
    if (data.time == 0) {
        switch_time = "";
        input_time = "disabled";
    } else {
        let minutes = Math.floor(data.time);
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        let seconds = (data.time - minutes) * 60;
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        question_time = minutes + ":" + seconds;
        switch_time = "checked"
        input_time = "";
    }

    // Create the question div
    var question = document.createElement("div");
    question.classList.add("accordion-item");
    question.innerHTML = `
    <h2 class="accordion-header" id="heading${data.id}">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${data.id}" aria-expanded="false" aria-controls="collapse${data.id}">
            Question (id : ${data.id})
        </button>
    </h2>
    <div id="collapse${data.id}" class="accordion-collapse collapse" aria-labelledby="heading${data.id}" data-bs-parent="#modifyQuestions">
    <div class="accordion-body">

    <input type="hidden" class="question_id old" value="${data.id}">

    <button type="button" class="btn btn-sm btn-danger mb-2" onclick="if(confirm('Supprimer la question ?')){this.parentElement.parentElement.parentElement.remove();last_question_id--;removeQuestion(${data.id})}"><i class="fa-solid fa-trash"></i>&nbsp;Supprimer la question</button>

    <div class="question_text"></div>        

    <div class="input-group mt-3 mb-3">
        <span class="input-group-text"><i class="fa-solid fa-list fa-fw"></i></span>
        <select class="custom-select question_type">
            <option value="0" ${select_simple}>Question simple</option>
            <option value="1" ${select_multiple}>Question à choix multiples</option>
        </select>
    </div>

    <div class="form-check form-switch mb-3 ml-4">
        <input class="form-check-input question_time_switch" type="checkbox"onchange="toggleTime(this)" `+ switch_time + `>
        <label class="form-check-label"">Temps limité</label>
    </div>

    <div class="input-group mb-3">
        <span class="input-group-text"><i class="fa-solid fa-clock fa-fw"></i></span>
        <input type="time" class="form-control question_time"  value="`+ question_time + `" ` + input_time + `>
        <span class="input-group-text">(min:sec)</span>
    </div>

    Réponses : <br>

    <div class="question_answers" id="question_answers_${data.id}">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Lettre</th>
                <th scope="col">Texte</th>
                <th scope="col">Points</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>        
    </div>
    <button type="button" class="btn btn-secondary" onclick="addAnswer(${data.id})">Ajouter une réponse</button>`;
    document.getElementById(location).appendChild(question);

    $('#modifyQuestions .question_text').last().summernote("code", data.text);

    // Fix summernote
    fixSummernote();
}

function loadAnswers(data, location) {
    // Add the answer to the question table
    var answer = document.createElement("tr");
    answer.innerHTML = `
    <td><input type="text" class="form-control answer_id old" value="${data.id}" disabled></td>
    <td><input type="text" class="form-control answer_letter" value="${data.letter}"></td>
    <td><input type="text" class="form-control answer_text" value="${data.text}"></td>
    <td><input type="number" class="form-control answer_points" value="${data.points}"></td>
    <td><button type="button" class="btn btn-sm btn-danger" onclick="if(confirm('Supprimer la réponse ?')){this.parentElement.parentElement.remove();removeAnswer(${data.id})}"><i class="fa-solid fa-trash"></i></button></td>`;
    document.getElementById(location).getElementsByTagName("tbody")[0].appendChild(answer);
}

function removeQuestion(id) {
    // Store the id of the questions to remove
    removed_questions.push(id);
}

function removeAnswer(id) {
    // Store the id of the answers to remove
    removed_answers.push(id);
}

function modifyQuiz() {
    let form_errors = []
    // Remove all invalid classes
    $("#mod_quiz_title").removeClass("is-invalid");
    $("#modify_quiz input").removeClass("is-invalid");
    $("#modify_quiz select").removeClass("is-invalid");
    $("#modify_quiz div").removeClass("is-invalid");

    let id = parseInt($("#mod_quiz_id").val());
    let title = $("#mod_quiz_title").val();
    if (title == "") {
        form_errors.push("le titre du quiz est vide");
        $("#mod_quiz_title").addClass("is-invalid");
    }

    let display_results_check = $("#mod_display_results").prop("checked");
    if (display_results_check) {
        display_results = 1;
    } else {
        display_results = 0;
    }

    let order_check = $("#mod_order").prop("checked");
    if (order_check) {
        order = 1;
    } else {
        order = 0;
    }

    let questions = [];
    // For each question
    $("#modifyQuestions .question_id").each(function () {
        // Check if the question is new or old
        let update;
        if ($(this).hasClass("old")) {
            update = true;
        } else {
            update = false;
        }

        let question_id = parseInt($(this).val());
        let question_text_el = $(this).parent().find(".question_text");
        let question_text = question_text_el.summernote("code");
        if (question_text == "" || question_text == "<p><br></p>" || question_text == "<br>") {
            form_errors.push("une question est vide");
            $(this).parent().find(".note-editor").addClass("is-invalid");
        }
        let question_type_el = $(this).parent().find(".question_type");
        let question_type = question_type_el.val();
        if (question_type == null || question_type == "") {
            form_errors.push("le type d'une question est vide");
            question_type_el.addClass("is-invalid");
        }
        let question_time_el = $(this).parent().find(".question_time");
        let question_time_switch_el = $(this).parent().parent().find(".question_time_switch");
        let question_time = question_time_el.val();
        if (question_time_switch_el.is(":checked")) {
            if (question_time == "") {
                form_errors.push("la durée d'une question est vide");
                question_time_el.addClass("is-invalid");
            } else {
                // Convert question_time to float minutes
                question_time = parseInt(question_time.split(":")[0]) + parseInt(question_time.split(":")[1]) / 60;
            }
        } else {
            question_time = 0;
        }
        let answers = [];
        let counter = 0;
        let points_total = 0;

        // For each answer
        $(this).parent().find(".answer_id").each(function () {
            let answer = {};
            // Check if the answer is new or old
            let update_answer;
            let answer_id = $(this).val();
            if ($(this).hasClass("old")) {
                update_answer = true;
            } else {
                update_answer = false;
            }
            let answer_letter = $(this).parent().parent().find(".answer_letter");
            let answer_text = $(this).parent().parent().find(".answer_text");
            let answer_points = $(this).parent().parent().find(".answer_points");
            if (answer_text.val() == "" || answer_letter.val() == "" || answer_points.val() == "") {
                form_errors.push("une réponse possède un ou plusieurs champs vides");
                if (answer_text.val() == "") {
                    answer_text.addClass("is-invalid");
                }
                if (answer_letter.val() == "") {
                    answer_letter.addClass("is-invalid");
                }
                if (answer_points.val() == "") {
                    answer_points.addClass("is-invalid");
                }
            } else {
                points_total += parseInt(answer_points.val());
                if (answer_points.val() > 0) {
                    counter++;
                }

                answer = {
                    update: update_answer,
                    id: answer_id,
                    question_id: question_id,
                    letter: answer_letter.val(),
                    text: answer_text.val(),
                    points: parseInt(answer_points.val())
                }
            }
            answers.push(answer);

        });

        if (counter > 1 && parseInt(question_type) == 0) {
            form_errors.push("une question à choix unique possède plusieurs réponses correctes");
        } else {
            if (counter == 0) {
                form_errors.push("une question ne possède aucune réponse correcte");
            }
        }

        if (answers.length == 0) {
            form_errors.push("une question ne possède aucune réponse");
        } else {

            questions.push({
                update: update,
                id: question_id,
                text: question_text,
                type: parseInt(question_type),
                time: question_time,
                answers: answers
            });
        }
    });

    if (form_errors.length > 0) {
        $("#message_modify_quiz").html(`<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Erreur lors de la modification du quiz :</strong> ${form_errors.join(", ")}.<button type="button" class="close" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`)
        return;
    } else {
        // Send the modified quiz to the server
        socket.emit('modify_quiz', {
            id: id,
            title: title,
            display_results: display_results,
            question_order: order,
            questions: questions,
            removed_questions: removed_questions,
            removed_answers: removed_answers
        });
    }
}

function clearModifyQuiz() {
    // Clear the form
    $("#quiz_list_modify").val("").change();
    $("#modify_quiz").hide();
    $("#mod_quiz_title").removeClass("is-invalid");
    $("#modify_quiz input").removeClass("is-invalid");
    removed_questions = [];
    removed_answers = [];
    $("#modifyQuizSubmit").prop("disabled", true);
    $("#deleteQuiz").prop("disabled", true);
    question_modify_count = 0;
    $("#message_modify_quiz").html("");
}


function deleteQuiz() {
    // Ask for confirmation before deleting the quiz
    if (confirm("Êtes-vous sûr de vouloir supprimer ce quiz (cela inclus les questions, réponses et résultats d'utilisateurs) ?")) {
        let id = parseInt($("#mod_quiz_id").val());
        socket.emit('delete_quiz', id);
    }
}

