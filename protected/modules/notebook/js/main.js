// This file contains all the functions related to the notebook and its behaviour in the add/modify classroom modals in the admin panel. It is the connector between the notebook activity and the classroom in the admin panel

$(document).ready(function () {

    // Get the notebook list
    socket.emit('load_notebook_list')

    socket.on("load_notebook_list", (data) => {
        if (data == "error") {
            $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors du chargement des boîtes de dépôt.</div>");
            return;
        }
        // For each notebook list
        $(".notebook_list").each(function () {

            // Get the selected value
            let selected = $(this).val();
            // Clean the select
            $(this).html("");
            // Check if notebook_list has class old
            if (!$(this).hasClass("old")) {
                $(this).html("<option disabled selected value> Choisir un cahier Python </option>");
                $("#modify_notebook").hide();
                for (var j = 0; j < data.length; j++) {
                    var option = document.createElement("option");
                    option.value = data[j].id;
                    option.text = "Cahier Python " + data[j].id + " : " + data[j].title;
                    $(this).append(option);
                }
                // Set back the selected value
                $(this).val(selected);
            } else {
                for (let i = 0; i < data.length; i++) {
                    // Add and set the selected quiz
                    if ($(this).attr("data-select") == data[i].id) {
                        $(this).append(`<option value="${data[i].id}" selected>Cahier Python ` + data[i].id + ` : ` + data[i].title + `</option>`);
                        continue;
                    } else {
                        $(this).append(`<option value="${data[i].id}">Cahier Python ` + data[i].id + ` : ` + data[i].title + `</option>`);
                    }
                }
            }
        })
    })
})

function addNotebookActivityButton(location) {
    let counter;
    if (location == "modifyDocument") {
        counter = doc_modify_count + 1;
        doc_modify_count++;
    } else {
        counter = doc_count + 1;
        doc_count++;
    }

    // add a title input to the addDocument div
    var file = document.createElement("div");
    file.classList.add("accordion-item");
    file.innerHTML = `
        <h2 class="accordion-header" id="panel_${counter}">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${counter}" aria-expanded="true" aria-controls="panelsStayOpen">
        Cahier Python ${counter}

        </button>
        </h2>

        <div id="panel_collapse_${counter}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">

      <input type="hidden" class="form-control doc_type" value="notebook">

        <div class="input-group mb-3">

        <div class="input-group-prepend">
            <label class="input-group-text" for="classroom_list_modify"><i
                    class="fa-brands fa-python fa-fw"></i></label>
        </div>
        <select class="custom-select notebook_list"></select>
        
         </div>
         </div>
         </div>
         </div>
            
        `;
    $("#" + location).append(file);

    // Load the notebook list to the select
    socket.emit("load_notebook_list");

}

function addNotebookToClassroom(loc, id) {
    console.log("Adding notebook to classroom from " + loc);
    let activity = [];
    let errors = [];

    let select = $('#' + loc + ' #' + id + ' .notebook_list')

    if (select.val() == null) {
        errors.push("un cahier Python doit être sélectionné");
        select.addClass("is-invalid");
    }

    // Determine if the notebook is an update or a new notebook
    let update;
    if (select.hasClass("old")) {
        update = true;
    } else {
        update = false;
    }

    // Create a document object
    let doc = {
        update: update,
        type: select.parent().parent().find(".doc_type").val(),
        doc_id: parseInt(select.parent().parent().find(".notebook_id").val()),
        id: select.val(),
        text: select.parent().parent().find(".notebook_list option:selected").text().split(": ")[1],
    }
    activity.push(doc);

    // Return the notebook_list and errors
    return {
        activity: activity,
        errors: errors,
    }
}

function loadNotebookActivity(location = "modifyDocument", data) {

    doc_modify_count++;

    // Create the notebook
    notebook_id = data.url.split("id=")[1];
    console.log("Creating notebook " + notebook_id + " in " + location)
    var notebook = document.createElement("div");
    notebook.classList.add("accordion-item");
    notebook.innerHTML = `
    <h2 class="accordion-header" id="panel_${doc_modify_count}">
    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panel_collapse_${doc_modify_count}" aria-expanded="true" aria-controls="panelsStayOpen">
    Cahier Python ${doc_modify_count}

    </button>
    </h2>

    <div id="panel_collapse_${doc_modify_count}" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
  <div class="accordion-body">

  <input type="hidden" class="notebook_id" value="${data.id}">

  <input type="hidden" class="form-control doc_type" value="notebook">

    <div class="input-group mb-3">

    <div class="input-group-prepend">
        <label class="input-group-text" for="classroom_list_modify"><i
                class="fa-brands fa-python fa-fw"></i></label>
    </div>
    <select class="custom-select notebook_list old" data-select="${notebook_id}">
    </select>
     </div>
     </div>
     </div>
     </div>
        
    `;
    $("#" + location).append(notebook);

    socket.emit("load_notebook_list");
}
