let notebookFilesModal;

$(document).ready(function () {

    // Instanciate the notebookFilesModal
    notebookFilesModal = new bootstrap.Modal(document.getElementById("notebookFilesModal"), {});

    socket.on("notebook_files_admin", (data) => {
        for (var i = 0; i < data.length; i++) {
            notebook_files_table.row.add([
                data[i].name,
                data[i].date,
                data[i].path,
                "<a class='btn btn-success mr-1' href='/assignments/notebook/" + data[i].path + "' download><i class='fas fa-download'></i></a><button class='btn btn-danger' onclick='deleteNotebookFile(\"" + data[i].path + "\");'><i class='fas fa-trash'></i></button>"
            ]).draw(false);
        }
    })
});

function loadNotebookFiles(id) {
    notebook_files_table.rows().remove().draw();
    $("#deleteNotebookFiles").prop("disabled", false);
    $("#downloadNotebookFiles").prop("disabled", false);
    if (id == "") {
        return;
    }
    socket.emit("get_notebook_files", id);
}

function deleteNotebookFile(path) {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce fichier ?")) {
        socket.emit("delete_notebook_file", path, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichier supprimé avec succès.</div>");
                clearNotebookFiles();
            } else {
                $("#message_notebook_files").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du fichier : " + callback + ".</div>");
            }
        })
    }
}

function clearNotebookFiles() {
    notebook_files_table.rows().remove().draw();
    $("#deleteNotebookFiles").prop("disabled", true);
    $("#downloadNotebookFiles").prop("disabled", true);
    $("#list_notebook_files").val("").change();
    $("#message_files_notebook").html("");
    socket.emit("load_notebook_list");
    notebookFilesModal.hide();
}

function deleteNotebookFiles() {
    if (confirm("Êtes-vous sûr de vouloir supprimer les fichiers sélectionnés ?")) {
        // Get id of selected rows
        var rows = notebook_files_table.rows('.selected').data();
        var id = [];
        for (i = 0; i < rows.length; i++) {
            // Get html path in the 3rd column
            id.push(rows[i][2]);
        }
        socket.emit('delete_notebook_files', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Fichiers supprimés avec succès.</div>");
                clearNotebookFiles();
            } else {
                $("#message_files_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression des fichiers : " + callback + ".</div>");
            }
        })
    }
}

function downloadNotebookFiles() {
    // Get id of selected rows
    var rows = notebook_files_table.rows('.selected').data();
    for (i = 0; i < rows.length; i++) {
        // Create a link and click on it
        var link = document.createElement('a');
        link.href = '/assignments/notebook/'+rows[i][2];
        link.download = rows[i][2];
        link.click();
    }    
}