let notebookModifyModal;

$(document).ready(function () {

    // Instanciate the classroomModifyModal
    notebookModifyModal = new bootstrap.Modal(document.getElementById("notebookModifyModal"), {});

})

function selectNotebook(id) {
    if (id == "") {
        return;
    }
    $("#modify_notebook").show();

    $("#modifyNotebookSubmit").prop("disabled", false);
    $("#deleteNotebook").prop("disabled", false);

    socket.emit('select_notebook', id, data => {
        $("#notebook_mod_id").val(data.id);
        $("#notebook_mod_title").val(data.title);
        $("#notebook_mod_instructions").summernote('code', data.instructions);
        $("#notebook_mod_file_link").attr("href", data.url);
        // If data.split begins with http, it's a url
        if (data.split == "" || data.split == null) {
            $("#doc_mod_url").val(data.split);
        } else {
            if (data.split.startsWith("http")) {
                $("#doc_mod_url").val(data.split);
                $("#doc_mod_prev").addClass("d-none");
            } else {
                $("#doc_mod_file_link").attr("href", data.split);
                $("#doc_mod_file_link").html(data.split.split("/").pop());
                $("#doc_mod_prev").removeClass("d-none");
                $("#doc_mod_file_prev").html(data.split.split("/").pop());
            }
        }
        $("#notebook_mod_pass").val(data.pass);

        if (data.aux == "" || data.aux == null) {
            $("#aux_mod_prev").addClass("d-none");
        } else {
            $("#aux_mod_prev").removeClass("d-none");
            $("#aux_mod_file_link").attr("href", data.aux);
            $("#aux_mod_file_link").html(data.aux.split("/").pop());
            $("#aux_mod_file_link").removeClass("d-none");
        }

        if (data.blocalgo == 1) {
            $("#mod_blocalgo_type").prop("checked", true);
        } else {
            $("#mod_blocalgo_type").prop("checked", false);
        }
    })
}


function modifyNotebook() {
    console.log("Modifying notebook");
    var id = $("#notebook_mod_id").val();
    var title = $("#notebook_mod_title").val();
    var instructions = $("#notebook_mod_instructions").summernote('code');
    if ($("#notebook_mod_file")[0].files.length != 0) {
        var notebook_file = $("#notebook_mod_file")[0].files[0];
    } else {
        var notebook_file = ""
    }
    if ($("#notebook_mod_aux")[0].files.length != 0) {
        var notebook_aux = $("#notebook_mod_aux")[0].files[0];
        var notebook_aux_name = notebook_aux.name;
    } else {
        var notebook_aux = ""
        var notebook_aux_name = ""
    }
    var old_notebook_file = $("#notebook_mod_file_link").attr("href");
    var doc_url = $("#doc_mod_url").val();
    var doc_file_ext = "";
    if ($("#doc_mod_file")[0].files.length != 0) {
        var doc_file = $("#doc_mod_file")[0].files[0];
        var doc_file_ext = doc_file.name.split('.').pop();
    } else {
        var doc_file = ""
        var doc_file_ext = ""
    }
    var old_doc_file = $("#doc_mod_file_link").attr("href");
    var old_aux_file = $("#aux_mod_file_link").attr("href");
    var pass = $("#notebook_mod_pass").val();

    var errors = []

    if (title == "") {
        errors.push("le titre ne peut pas être vide");
        $("#notebook_mod_title").addClass("is-invalid");
    }
    if (instructions == "" || instructions == "<p><br></p>" || instructions == "<br>") {
        errors.push("les consignes ne peuvent pas être vides");
        $("#notebook_mod_instructions").addClass("is-invalid");
    }
    if (doc_file != "" && doc_url != "") {
        errors.push("seulement un lien ou un fichier peut être ajouté");
        $("#doc_mod_url").addClass("is-invalid");
        $("#doc_mod_file").addClass("is-invalid");
    }
    if (pass == "") {
        errors.push("le mot de passe ne peut pas être vide");
        $("#notebook_mod_pass").addClass("is-invalid");
    }

    let blocalgo = 0;
    let blocalgo_check = $("#mod_blocalgo_type").prop("checked");
    if (blocalgo_check) {
        blocalgo = 1;
    } else {
        blocalgo = 0;
    }

    if (errors.length > 0) {
        $("#message_mod_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification du cahier Python : " + errors.join(", ") + ".</div>");
    } else {
        console.log("Sending modify_notebook");
        socket.emit('modify_notebook', { id: id, title: title, instructions: instructions, notebook_file: notebook_file, notebook_aux: notebook_aux, notebook_aux_name: notebook_aux_name, old_aux_file: old_aux_file, doc_url: doc_url, doc_file: doc_file, doc_file_ext: doc_file_ext, old_notebook_file: old_notebook_file, old_doc_file: old_doc_file, blocalgo: blocalgo, pass: pass }, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Cahier Python modifié avec succès.</div>");
                clearModifyNotebook();
                // Load the notebook list to the select
                socket.emit("load_notebook_list");
            } else {
                $("#message_add_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la modification du cahier Python.</div>");
            }
        })
    }
}

function clearModifyNotebook() {
    console.log("Clearing modify notebook");
    $("#notebook_mod_title").val("");
    $("#notebook_mod_instructions").summernote('code', '');
    $("#notebook_mod_file").val("");
    $("#doc_mod_url").val("");
    $("#doc_mod_file").val("");
    $("#notebook_mod_pass").val("");
    $("#message_mod_notebook").html("");
    $("#notebook_mod_title").removeClass("is-invalid");
    $("#notebook_mod_instructions").removeClass("is-invalid");
    $("#notebook_mod_pass").removeClass("is-invalid");
    $("#modify_notebook").hide();
    $("#notebook_list_modify").val("").change();
    $("#modifyNotebookSubmit").prop("disabled", true);
    $("#deleteNotebook").prop("disabled", true);
    notebookModifyModal.hide();
}

function deleteNotebook() {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce cahier Python ?")) {
        var id = $("#notebook_mod_id").val();
        socket.emit('delete_notebook', id, callback => {
            if (callback == "success") {
                $("#message").html("<div class='alert alert-success' role='alert'>Cahier Python supprimé avec succès.</div>");
                clearModifyNotebook();
                // Load the notebook list to the select
                socket.emit("load_notebook_list");
            } else {
                console.log(callback);
                $("#message").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du cahier Python.</div>");
            }
        })
    }
}

function deleteAuxFile() {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce fichier auxiliaire ?")) {
        socket.emit('delete_notebook_aux', $("#notebook_mod_id").val(), callback => {
            if (callback == "success") {
                $("#message_mod_notebook").html("<div class='alert alert-success' role='alert'>Fichier auxiliaire supprimé avec succès.</div>");
                $('#aux_mod_file_link').attr('href', '');
                $('#aux_mod_prev').addClass('d-none');
            } else {
                console.log(callback);
                $("#message_mod_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du fichier auxiliaire.</div>");
            }
        })
    }
}

function deleteDocFile() {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce document ?")) {
        socket.emit('delete_notebook_doc', $("#notebook_mod_id").val(), callback => {
            if (callback == "success") {
                $("#message_mod_notebook").html("<div class='alert alert-success' role='alert'>Document supprimé avec succès.</div>");
                $('#doc_mod_file_link').attr('href', '');
                $('#doc_mod_prev').addClass('d-none');
            } else {
                console.log(callback);
                $("#message_mod_notebook").html("<div class='alert alert-danger' role='alert'>Une erreur est survenue lors de la suppression du document.</div>");
            }
        })
    }
}
